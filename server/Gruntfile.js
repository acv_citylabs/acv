module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        nggettext_extract: {
            pot: {
                files: {
                    'languages/template.pot': [
                        'languages/extraDictionary.html',
                        'public/templates/*.html',
                        'public/templates/animators/*.html',
                        'public/templates/cafes/*.html',
                        'public/templates/conditions/*.html',
                        'public/templates/home/*.html',
                        'public/templates/home/homes/*.html',
                        'public/templates/home/homes/directives/*.html',
                        'public/templates/modals/*.html',
                        'public/templates/modals/guidelines/*.html',
                        'public/templates/navbar/*.html',
                        'public/templates/participants/*.html',
                        'public/templates/profile/*.html',
                        'public/templates/recovery/*.html',
                        'public/templates/sessions/*.html',
                        'public/templates/settings/*.html',
                        'public/js/controllers/*.js',
                        'public/js/controllers/animators/*.js',
                        'public/js/controllers/cafes/*.js',
                        'public/js/controllers/participants/*.js',
                        'public/js/controllers/recovery/*.js',
                        'public/js/controllers/sessions/*.js',
                        'public/js/directives/*.js',
                        'public/js/*.js'
                    ]
                }
            }
        },
        nggettext_compile: {
            all: {
                files: {
                    'public/js/appTranslations.js': ['languages/*.po']
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-angular-gettext');

    // Default task(s).
    grunt.registerTask('default', ['nggettext_extract', 'nggettext_compile']);
};