var jsonwebtoken    = require('jsonwebtoken');
var fs              = require('fs');
var keyGenerator    = require("generate-key");
var nconf           = require('nconf');
var db              = require('../models/user');
var redisClient     = require('../databases/redis').redisClient;
var tokenManager    = require('../token_manager');
var audit           = require('../audit-log');
var https           = require('https');
var crypto          = require('crypto');
var formidable      = require("formidable");
var nodemailer      = require('nodemailer');

//Config
nconf.file("config/server.json");
var tokenSecret     = nconf.get('token').secret;
var recaptchaSecret = nconf.get('reCAPTCHA').secret;
var mailerConfig = nconf.get('mailer');

exports.lostPassword = function(req, res) {
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {   
        var email = fields.email || '';
        var captcha = fields.captcha || '';

        if (email === '' || captcha === '') {
            audit.logEvent('[anonymous]', 'Account ctrl', 'Lost password', '', '', 'failed',
                           'The user tried to send an email to reset password in but one or more params of the request was not defined');
            return res.sendStatus(400); 
        } else {
            verifyRecaptcha(captcha, function(success) {
                if (success) {
                    db.userModel.findOne({email: email}, function (err, user) {
                        if (err) {
                            console.log(err);
                            audit.logEvent('[mongodb]', 'Account ctrl', 'Lost password', 'used email', email, 'failed', 'Mongodb attempted to find the email');
                            return res.status(500).send(err);
                        } else {
                            if (user === null) {
                                audit.logEvent('[anonymous]', 'Account ctrl', 'Lost password', 'used email', email, 'failed', 
                                               'The user tried to send an email to reset password but the email does not exist');
                                return res.json({
                                    exists: false,
                                    captcha: true
                                });
                            } else {
                                //Create a token
                                crypto.randomBytes(20, function(err, buf) {
                                    if (err) {
                                        console.log(err);
                                        return res.status(500).send(err);
                                    } else {
                                        var token = buf.toString('hex');
                                        user.resetPasswordToken = token;
                                        user.resetPasswordExpires = Date.now() + 3600000;
                                        user.save(function(err) {
                                            if (err) {
                                                console.log(err);
                                                audit.logEvent('[mongodb]', 'Account ctrl', 'Lost password', "username", user.username, 'failed',
                                                               "Mongodb attempted to save the modified user");
                                                return res.status(500).send(err);
                                            } else {
                                                var rep = [['[name]', user.firstname + " " + user.lastname], ['[link]', "https://" + req.headers.host + "/recovery/reset/" + token]];
                                                replaceInFile('./emails/' + user.language + '_reset.html', rep, function (err, result) {
                                                    if (err) {
                                                        console.log(err);
                                                        return res.status(500).send(err);
                                                    } else {
                                                        //Send an email with a link containing the key
                                                        sendMail({to: user.email, subject: 'Password Reset', html: result}, function(err){
                                                            if (err) {
                                                                console.log(err);
                                                                return res.status(500).send(err);
                                                            }
                                                            return res.json({
                                                                exists: true,
                                                                captcha: true
                                                            });
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    });
                } else {
                    //Captcha failed
                    return res.json({
                        captcha: false
                    });
                }
            });
        }
    });
};

exports.resetPassword = function(req, res) {
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {  
        var token = fields.token || '';
        var password = fields.newPassword || '';
        var passwordConfirmation = fields.newPasswordConfirmation || '';
        if (password === '' || passwordConfirmation === '' || token === '') {
            audit.logEvent('[anonymous]', 'Account ctrl', 'Reset password', '', '', 'failed',
                           'The user tried to reset password but one or more params of the request was not defined');
            return res.sendStatus(400); 
        } else {
            if (password !== passwordConfirmation) {
                audit.logEvent('[anonymous]', 'Account ctrl', 'Reset password', '', '', 'failed',
                               'The user tried to change his password but the password was not equal to its confirmation');
                return res.json({
                    token: false,
                    changed: false
                });
            } else {
                console.log(token);
                db.userModel.findOne({
                    $or:[{
                        resetPasswordToken: token,  activationToken: { $ne: '0' }
                    }, {
                        resetPasswordToken: token, resetPasswordExpires: {$gt: Date.now()}
                    }]}, function(err, user) {
                    if (err) {
                        console.log(err);
                        audit.logEvent('[mongodb]', 'Account ctrl', 'Reset password', '', '', 'failed', 'Mongodb attempted to find a user by token');
                        return res.status(500).send(err);
                    } else {
                        if (!user) {
                            return res.json({
                                token: false
                            });
                        } else {
                            user.password = password;
                            user.resetPasswordToken = undefined;
                            user.activationToken = "0";
                            user.resetPasswordExpires = undefined;
                            user.save(function(err) {
                                if (err) {
                                    console.log(err);
                                    audit.logEvent('[mongodb]', 'Account ctrl', 'Reset password', "username", user.username, 'failed',
                                                   "Mongodb attempted to save the modified user");
                                    return res.status(500).send(err);
                                } else {
                                    audit.logEvent(user.username, 'Account ctrl', 'Reset password', "username", user.username, 'succeed',
                                                   'The user has successfully changed the password of his account');

                                    return res.json({
                                        email : user.email,
                                        token: true,
                                        changed: true
                                    });
                                }
                            });
                        }
                    }
                });
            }
        }
    });
};

exports.signin = function(req, res) {
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
        var email = fields.email || '';
        var password = fields.password || '';
        var expiration = tokenManager.TOKEN_EXPIRATION;

        if (email === '' || password === '') {
            audit.logEvent('[anonymous]', 'Account ctrl', 'Sign in', '', '', 'failed',
                           'The user tried to sign in but one or more params of the request was not defined');
            return res.sendStatus(400); 
        } else {
            if(fields.rememberme){
                expiration = 1000 * 60 * 24 * 7;
            }

            db.userModel.findOne({email: email}, function (err, user) {
                if (err) {
                    console.log(err);
                    audit.logEvent('[mongodb]', 'Account ctrl', 'Sign in', 'used email', email, 'failed', 'Mongodb attempted to find the email');
                    return res.status(500).send(err);
                } else {
                    if (user) {
                        if(user.activationToken !== '0'){
                            audit.logEvent('[anonymous]', 'Account ctrl', 'Sign in', 'used email', email, 'failed', 
                                           'The user tried to sign in but the account is not activated');
                            return res.send({
                                activated:false
                            });
                        } else {
                            user.comparePassword(password, function(isMatch) {
                                if (!isMatch) {
                                    audit.logEvent(user.username, 'Account ctrl', 'Sign in', 'used email', email, 'failed',
                                                   'The user tried to sign in but the password was incorrect');
                                    return res.sendStatus(401);
                                } else {
                                    var token = jsonwebtoken.sign({id: user._id}, tokenSecret, { expiresInMinutes: expiration });
                                    audit.logEvent(user.username, 'Account ctrl', 'Sign in', 'used email', email, 'succeed',
                                                   'The user has successfully signed in to his account');
                                    user.save(function(err) {
                                        if (err) {
                                            console.log(err);
                                            return res.status(500).send(err);
                                        } else {
                                            return res.status(200).json({
                                                activated:true,
                                                token:token,
                                                language: user.language
                                            });
                                        }
                                    });
                                }
                            });
                        } 
                    } else {
                        audit.logEvent('[anonymous]', 'Account ctrl', 'Sign in', 'used email', email, 'failed', 
                                       'The user tried to sign in but the used email does not exist');
                        return res.sendStatus(401);
                    }
                }
            });
        }
    });
};

exports.activation = function(req, res) {
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
        var token = fields.token || '';
        if (token === '') {
            audit.logEvent('[anonymous]', 'Account ctrl', 'Activation', '', '', 'failed',
                           'The user tried to reset password but one or more params of the request was not defined');
            return res.send(400); 
        } else {
            db.userModel.findOne({ activationToken: token }, function(err, user) {
                if (err) {
                    console.log(err);
                    audit.logEvent('[mongodb]', 'Account ctrl', 'Activation', '', '', 'failed', 'Mongodb attempted to find a user by token');
                    return res.status(500).send(err);
                } else {
                    if (!user) {
                        return res.json({
                            activated: false
                        });
                    } else {
                        user.activationToken = "0";
                        user.save(function(err) {
                            if (err) {
                                console.log(err);
                                audit.logEvent('[mongodb]', 'Account ctrl', 'Activation', "username", user.username, 'failed',
                                               "Mongodb attempted to save the modified user");
                                return res.status(500).send(err);
                            } else {
                                audit.logEvent(user.username, 'Account ctrl', 'Activation', "username", user.username, 'succeed',
                                               'The user has successfully activated his account');
                                return res.json({
                                    activated: true,
                                    email: user.email
                                });
                            }
                        });
                    }
                }
            });
        }
    });
};


exports.signout = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if(token !== null){
        tokenManager.expireToken(token, true);
        audit.logEvent(jsonwebtoken.decode(token).id, 'Account ctrl', 'Sign out', '', '', 'succeed', 'A user has successfully logged out');
        delete req.user;
        return res.sendStatus(200);
    } else {
        audit.logEvent('[anonymous]', 'Account ctrl', 'Sign out', '', '', 'failed', 'The user was not authenticated');
        return res.send(401);
    }
};

exports.changePassword = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if(token != null){
        var userID = jsonwebtoken.decode(token).id;
        var form = new formidable.IncomingForm();
        form.parse(req, function(err, fields, files) {
            var oldPassword = fields.oldPassword || '';
            var newPassword = fields.newPassword || '';
            var newPasswordConfirmation = fields.newPasswordConfirmation || '';

            if (oldPassword === '' || newPassword === '' || newPasswordConfirmation === '') {
                audit.logEvent(userID, 'Account ctrl', 'Change password', '', '', 'failed',
                               'The user tried to change his password but one or more params of the request was not defined');
                return res.sendStatus(400); 
            } else {
                if (newPassword !== newPasswordConfirmation) {
                    audit.logEvent(userID, 'Account ctrl', 'Change password', '', '', 'failed',
                                   'The user tried to change his password but the password was not equal to its confirmation');
                    return res.json({
                        new: false
                    });
                } else {
                    db.userModel.findOne({_id: userID}, function (err, user) {
                        if (err) {
                            console.log(err);
                            audit.logEvent('[mongodb]', 'Account ctrl', 'Change password', "user id", userID, 'failed', "Mongodb attempted to find the user");
                            return res.send(500).send(err);
                        } else {
                            if (user == undefined) {
                                audit.logEvent('[mongodb]', 'Account ctrl', 'Change password', '', '', 'failed',
                                       'Mongodb attempted to find the user but it revealed not defined');
                                return res.sendStatus(401);
                            } else {
                                user.comparePassword(oldPassword, function(isMatch) {
                                    if (!isMatch) {
                                        audit.logEvent(user.username, 'Account ctrl', 'Change password', '', '', 'failed',
                                                       'The user tried to change the password but the old one was incorrect');
                                        return res.json({
                                            old: false,
                                            new: true
                                        });
                                    }
                                    else{
                                        user.password = newPassword;
                                        user.save(function(err) {
                                            if (err) {
                                                console.log(err);
                                                audit.logEvent('[mongodb]', 'Account ctrl', 'Change password', "username", user.username, 'failed',
                                                               "Mongodb attempted to save the modified user");
                                                return res.status(500).send(err);
                                            } else {
                                                audit.logEvent(user.username, 'Account ctrl', 'Change password', '', '', 'succeed',
                                                               'The user has successfully changed the password of his account');
                                                return res.json({
                                                    old: true,
                                                    new: true
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    } else {
        audit.logEvent('[anonymous]', 'Account ctrl', '', '', '', 'failed','The user was not authenticated');
        return res.send(401);
    }
};

exports.profile = function(req, res){
    var token = tokenManager.getToken(req.headers);
    if(token !== null){
        var actorID = jsonwebtoken.decode(token).id;
        db.userModel.findOne({
            _id: actorID
        }, {
            _id: 0
        }, function (err, user) {
            if (err) {
                console.log(err);
                audit.logEvent('[mongodb]', 'Account ctrl', 'Profile', "user id", actorID, 'failed', "Mongodb attempted to find the user");
                return res.send(500).send(err);
            } else {
                if (user === null) {
                    audit.logEvent('[mongodb]', 'Account ctrl', 'Read profile', '', '', 'failed', 'Mongodb attempted to find the user but it revealed not defined');
                    return res.sendStatus(401);
                } else {
                    return res.json(user);
                }
            }
        });
    } else {
        audit.logEvent('[anonymous]', 'Account ctrl', 'Read profile', '', '', 'failed','The user was not authenticated');
        return res.sendStatus(401); 
    }
};

exports.update = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if(token != null){
        var userID = jsonwebtoken.decode(token).id;
        var form = new formidable.IncomingForm();
        form.parse(req, function(err, fields, files) {
            var avatar = fields.avatar;
            var firstname = fields.firstname;
            var lastname = fields.lastname;
            var homeAddress = fields.homeAddress;
            var phone = fields.phone;
            var email = fields.email;
            var language = fields.language;
            var who = fields.who;

            var toUpdate = {};
            var toRemove = {};

            // -- Required if exist --
            //firstname
            if (firstname !== undefined && firstname != ''){
                toUpdate['firstname'] = firstname;
            }

            //lastname
            if (lastname !== undefined && lastname != ''){
                toUpdate['lastname'] = lastname;
            }
            
            //email
            if (email !== undefined && email != ''){
                toUpdate['email'] = email;
            }

            //language
            if (language !== undefined && language != ''){
                toUpdate['language'] = language;
            };

            //who
            if (who !== undefined && who != ''){
                toUpdate['who'] = who
            };

            // -- Not Required --
            //avatar
            if (avatar !== undefined && avatar != '') {
                toUpdate['preferences.avatar'] = avatar;
            } else if (avatar === '') {
                toRemove['preferences.avatar'] = 1;
            };

            //homeAddress
            if (homeAddress !== undefined && homeAddress !== ''){
                toUpdate['homeAddress'] = homeAddress;
            } else if (homeAddress === '') {
                toRemove['homeAddress'] = 1;
            };

            //phone
            if (phone !== undefined && phone !== ''){
                toUpdate['phone'] = phone;
            } else if (phone === ''){
                toRemove['phone'] = 1;
            };

            db.userModel.findOne({_id: userID}, function (err, user) {
                if (err) {
                    console.log(err);
                    return res.send(500).send(err);
                } else {
                    if (user === null) {
                        return res.sendStatus(401);
                    } else {
                        if(Object.keys(toRemove).length > 0 && Object.keys(toUpdate).length > 0){
                            //remove AND update
                            db.userModel.update({_id:userID}, { $unset: toRemove }, function(err, nbRow) {
                                if (err) {
                                    console.log(err);
                                    return res.status(500).send(err);
                                } else {
                                    user.save(function(err) {
                                        if (err) {
                                            console.log(err);
                                            audit.logEvent('[mongodb]', 'Account ctrl', 'Update', "username", user.username, 'failed',
                                                           "Mongodb attempted to save the new user");
                                            return res.status(500).send(err);
                                        } else {
                                            db.userModel.update({_id:userID}, toUpdate, function(err, nbRow) {
                                                if (err) {
                                                    console.log(err);
                                                    return res.status(500).send(err);
                                                } else {
                                                    user.save(function(err) {
                                                        if (err) {
                                                            console.log(err);
                                                            audit.logEvent('[mongodb]', 'Account ctrl', 'Update', "username", user.username, 'failed',
                                                                           "Mongodb attempted to save the new user");
                                                            return res.status(500).send(err);
                                                        } else {
                                                            return res.sendStatus(200);
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        } else if(Object.keys(toRemove).length == 0 && Object.keys(toUpdate).length > 0){
                            //just update
                            db.userModel.update({_id:userID}, toUpdate, function(err, nbRow) {
                                if (err) {
                                    console.log(err);
                                    return res.status(500).send(err);
                                } else {
                                    user.save(function(err) {
                                        if (err) {
                                            console.log(err);
                                            audit.logEvent('[mongodb]', 'Account ctrl', 'Update', "username", user.username, 'failed',
                                                           "Mongodb attempted to save the new user");
                                            return res.status(500).send(err);
                                        } else {
                                            return res.sendStatus(200);
                                        }
                                    });
                                }
                            });
                        } else if(Object.keys(toRemove).length > 0 && Object.keys(toUpdate).length == 0){
                            //just remove
                            db.userModel.update({_id:userID}, { $unset: toRemove }, function(err, nbRow) {
                                if (err) {
                                    console.log(err);
                                    return res.status(500).send(err);
                                } else {
                                    user.save(function(err) {
                                        if (err) {
                                            console.log(err);
                                            audit.logEvent('[mongodb]', 'Account ctrl', 'Update', "username", user.username, 'failed',
                                                           "Mongodb attempted to save the new user");
                                            return res.status(500).send(err);
                                        } else {
                                            return res.sendStatus(200);
                                        }
                                    });
                                }
                            })
                        }
                    }
                }
            });
        });
    } else {
        return res.send(401);
    }
};




exports.getUser = function getUser(userID, callback){
    db.userModel.findOne({ _id : userID }, {}, function(err, user) {
        if (err)
            callback(err, null);
        else{
            callback(null, user);
        }
    });
};

exports.getUserID = function getUserID(username, callback){
    db.userModel.findOne({ username : username }, {_id: 1}, function(err, userID) {
        if (err)
            callback(err, null);
        else if ((!userID) || (typeof userID._id === 'undefined'))
            callback("User " +username+ " not found.", null);
        else
            callback(null, userID._id);
    });
};

exports.getUsername = function getUsername(userID, callback){
    db.userModel.findOne({ _id : userID }, {username: 1, _id: 0}, function(err, username) {
        if (err)
            callback(err, null);
        else if (typeof username.username === 'undefined')
            callback("User " +userID+ " not found.", null);
        else
            callback(null, username.username);
    });
};




function sendMail(mail, callback) {
    var smtpTransport = nodemailer.createTransport({
        host: mailerConfig.host,
        port: mailerConfig.port,
        secure: true,
        auth: {
            user: mailerConfig.auth.user,
            pass: mailerConfig.auth.pass
        }
    });

    var mailOptions = {
        to: mail.to,
        from: mailerConfig.sender.name + ' <' + mailerConfig.sender.address + '>',
        subject: mail.subject,
        html: mail.html
    };
    
    smtpTransport.sendMail(mailOptions, function(err) {
        if(err){
            console.log(err);
            callback(err);
        }
        callback(null);
    });
}

function verifyRecaptcha(key, callback) {
    https.get("https://www.google.com/recaptcha/api/siteverify?secret=" + recaptchaSecret + "&response=" + key, function(res) {
        var data = "";
        res.on('data', function (chunk) {
            data += chunk.toString();
        });
        res.on('end', function() {
            try {
                var parsedData = JSON.parse(data);
                console.log("captcha : " + data);
                callback(parsedData.success);
            } catch (e) {
                callback(false);
            }
        });
    });
};

function escapeRegExp(string) {
    return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function replaceAll(string, find, replace) {
    return string.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function replaceInFile(file, toReplace, callback){
    fs.readFile(file, 'utf8', function (err, data) {
        if (err) {
            callback(err);
        }
        for(var i=0;i<toReplace.length;i++){
            for(var j=0;j<toReplace[i].length-1;j++){
                data = replaceAll(data, toReplace[i][j], toReplace[i][j+1]);
            }
        }
        callback(null, data);
    });
};




/*
exports.modifyProfilePhoto = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if(token != null){
        var userID = jsonwebtoken.decode(token).id;
        //Verify if a file is received
        if(req.files.file && req.files.file.path){
            if(req.files.file.size < 5242880){
                var originalFilename = req.files.file.originalFilename;
                var fileExtension = originalFilename.substr(originalFilename.lastIndexOf("."), originalFilename.length - 1)
                var tmpFile = "./tmp/" + keyGenerator.generateKey() + fileExtension;
                
                if (!fs.existsSync("./tmp")) {
                    fs.mkdirSync("./tmp");
                }
                
                var gm = require('gm');
                var gmFile = gm(req.files.file.path);
                gmFile.size(function(err, value){
                    if(value){
                        var min = value.width >= value.height ? value.height : value.width;
                        gmFile.gravity('Center')
                        .crop(min, min, 0, 0)
                        .resize(200, 200)
                        .write(tmpFile , function (err) {
                            if (!err){
                                //Find the user
                                db.userModel.findOne({_id: userID}, function (err, user) {
                                    if (err) {
                                        console.log(err);
                                        return res.send(401);
                                    }
                                    if (user == undefined) {
                                        return res.send(401);
                                    }

                                    //Read the file
                                    fs.readFile(tmpFile, function (err, data) {
                                        var key = keyGenerator.generateKey();
                                        var newPath = "./public/files/profilePhotos/" + key + "/photo" + fileExtension;

                                        //Delete the old photo and the old folder
                                        if(user.photo){
                                            deleteFolderRecursive("./public/files/profilePhotos/" + user.photo.substr(user.photo, user.photo.indexOf("/")));
                                        }

                                        //Create the new folder
                                        fs.mkdir("./public/files/profilePhotos/" + key, function(err) {
                                            if (err) {
                                                console.error(err);
                                                audit.logEvent('[fs]', 'Users ctrl', 'Modify profile photo', "Folder path",
                                                               './public/files/profilePhotos/' + key, 'failed','FS attempted to create a folder');
                                                return res.status(500).send(err);
                                            } 
                                            else {
                                                //Create the new file
                                                fs.writeFile(newPath, data, function (err) {
                                                    if (err) {
                                                        console.error(err);
                                                        audit.logEvent('[fs]', 'Users ctrl', 'Modify profile photo', "File path", newPath, 'failed',
                                                                       'FS attempted to create a file');
                                                        return res.status(500).send(err);
                                                    } 
                                                    else {
                                                        fs.unlink(tmpFile, function (err) {
                                                            if (err) {
                                                                console.error(err);
                                                                audit.logEvent('[fs]', 'Users ctrl', 'Modify profile photo', "Temp file", tmpFile, 'failed',
                                                                               'FS attempted to delete this temp file');
                                                                return res.status(500).send(err);
                                                            }
                                                            else{
                                                                db.userModel.update({
                                                                    _id:userID
                                                                }, {
                                                                    "photo": key + "/photo" + fileExtension
                                                                }, function(err, nbRow){
                                                                    if (err) {
                                                                        console.error(err);
                                                                        audit.logEvent('[mongodb]', 'Users ctrl', 'Modify profile photo',
                                                                                       "User id", userID, 'failed',
                                                                                       "Mongodb attempted to update the value of the user's field 'photo'");
                                                                        return res.status(500).send(err);
                                                                    }
                                                                    user.save(function(err) {
                                                                        if (err) {
                                                                            console.error(err);
                                                                            audit.logEvent('[mongodb]', 'Users ctrl', 'Modify profile photo', "User id", userID,
                                                                                           'failed',"Mongodb attempted to save the modified user");
                                                                            return res.status(500).send(err);
                                                                        }
                                                                        audit.logEvent(user.username, 'Users ctrl', 'Modify profile photo', 
                                                                                       "Photo key", key, 'succeed',
                                                                                       "The user has successfully uploaded a new profile photo for his account");
                                                                        return res.json({
                                                                            photo: key + "/photo" + fileExtension
                                                                        });
                                                                    });
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    });
                                });
                            }
                        });
                    }
                    else{
                        console.log(err)
                        audit.logEvent(userID, 'Users ctrl', 'Modify profile photo', '', '', 'failed',
                           'The user tried to upload a photo but the file was not a photo');
                        return res.status(500).send(err);
                    }
                })
            }
            else{
                audit.logEvent(userID, 'Users ctrl', 'Modify profile photo', '', '', 'failed',
                           'The user tried to upload a photo but the file was too large');
                return res.send(413);
            }
        }
        else{
            audit.logEvent(userID, 'Users ctrl', 'Modify profile photo', '', '', 'failed',
                           'The user tried to upload a photo but the file of the request was not defined');
            return res.send(400);
        }
    }
    else{
        audit.logEvent('[anonymous]', 'Users ctrl', 'Modify profile photo', '', '', 'failed','The user was not authenticated');
        return res.send(401); 
    }
};
*/


/*
exports.delete = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if(token != null && req.user){
        var actorID = jsonwebtoken.decode(token).id;

        //delete user's entries
        dbEntry.entryModel.remove({userID : actorID}, function(err){
            if (err) {
                console.log('Error: ' + err);
                return res.status(500).send(err);
            }
            else{
                //delete notifications
                dbNotification.notificationModel.remove({userID : actorID}, function(err){
                    if (err) {
                        console.log('Error: ' + err);
                        return res.status(500).send(err);
                    }
                    else{
                        //delete events
                        dbEvent.eventModel.remove({userID : actorID}, function(err){
                            if (err) {
                                console.log('Error: ' + err);
                                return res.status(500).send(err);
                            }
                            else{
                                //delete tips
                                dbTip.tipModel.remove({userID : actorID}, function(err){
                                    if (err) {
                                        console.log('Error: ' + err);
                                        return res.status(500).send(err);
                                    }
                                    else{
                                        //delete photo
                                        db.userModel.findOne({_id : actorID}, function(err, user){
                                            if (err) {
                                                console.log('Error: ' + err);
                                                return res.status(500).send(err);
                                            }
                                            else{
                                                if(user && user.photo && fs.existsSync('./public/files/profilePhotos/' + user.photo)){
                                                    fs.unlinkSync('./public/files/profilePhotos/' + user.photo);    
                                                    fs.rmdirSync('./public/files/profilePhotos/' + user.photo.substring(0,user.photo.indexOf("/")));
                                                }
                                                
                                                //delete account
                                                db.userModel.remove({_id : actorID}, function(err){
                                                    if (err) {
                                                        console.log('Error: ' + err);
                                                        return res.status(500).send(err);
                                                    }
                                                    else{
                                                        tokenManager.expireToken(req.headers);  
                                                        delete req.user;
                                                        return res.send(200);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
    else {
        return res.send(401);
    }
};
*/

/*
function deleteFolderRecursive(path) {
    var files = [];
    if( fs.existsSync(path) ) {
        files = fs.readdirSync(path);
        files.forEach(function(file,index){
            var curPath = path + "/" + file;
            if(fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};
*/