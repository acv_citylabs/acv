var jsonwebtoken    = require('jsonwebtoken');
var dbCafe          = require('../models/cafe');
var dbUser          = require('../models/user');
var audit           = require('../audit-log');
var tokenManager    = require('../token_manager');
var formidable      = require("formidable");

// Create a cafe
exports.create = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if(token != null){
        var actorID = jsonwebtoken.decode(token).id;
        var form = new formidable.IncomingForm();
        form.parse(req, function(err, fields, files) {
            var name = fields.name || '';
            var description = fields.description || '';
            var place = fields.place || '';

            if (name === '') {
                audit.logEvent(actorID, 'Cafes ctrl', 'Create', '', '', 'failed',
                               'The actor could not create a cafe because one or more params of the request was not defined');
                return res.sendStatus(400);
            } else {
                var cafe = new dbCafe.cafeModel();
                cafe.name = name;
                if(description !== ''){
                    cafe.description = description;
                }
                if(place !== ''){
                    cafe.place = place;
                }

                cafe.save(function(err) {
                    if (err) {
                        console.log(err);
                        audit.logEvent('[mongodb]', 'Cafes ctrl', 'Create', "name", name, 'failed', 
                                       "Mongodb attempted to save the new cafe");
                        return res.status(500).send(err);
                    } else {
                        return res.sendStatus(200);
                    }
                });
            }
        });
    } else {
        audit.logEvent('[anonymous]', 'Cafes ctrl', 'search', '', '', 'failed','The actor was not authenticated');
        return res.sendStatus(401); 
    }   
};

exports.update = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if(token != null){
        var actorID = jsonwebtoken.decode(token).id;
        if(req.params.id == undefined){
            audit.logEvent(actorID, 'Cafes ctrl', 'Update', '', '', 'failed',
                           'The actor could not update a cafe because one or more params of the request was not defined');
            return res.sendStatus(400);
        } else {
            var form = new formidable.IncomingForm();
            form.parse(req, function(err, fields, files) {
                var name = fields.name;
                var description = fields.description;
                var place = fields.place;
                var toUpdate = {};
                var toRemove = {};

                // -- Required if exist --
                if (name !== undefined && name != ''){
                    toUpdate['name'] = name;
                }

                // -- Not Required --
                if (description !== undefined && description !== ''){
                    toUpdate['description'] = description;
                } else if (description === ''){
                    toRemove['description'] = 1;
                };

                if (place !== undefined && place !== ''){
                    toUpdate['place'] = place;
                } else if (place === ''){
                    toRemove['place'] = 1;
                };

                dbCafe.cafeModel.findOne({_id: req.params.id}, function (err, cafe) {
                    if (err) {
                        console.log(err);
                        return res.status(500).send(err);
                    } else {
                        if (cafe === null) {
                            return res.sendStatus(401);
                        } else {
                            if(Object.keys(toRemove).length > 0 && Object.keys(toUpdate).length > 0){
                                //remove AND update
                                dbCafe.cafeModel.update({_id: req.params.id}, { $unset: toRemove }, function(err, nbRow) {
                                    if (err) {
                                        console.log(err);
                                        return res.status(500).send(err);
                                    } else {
                                        cafe.save(function(err) {
                                            if (err) {
                                                console.log(err);
                                                audit.logEvent('[mongodb]', 'Cafes ctrl', 'Update', "cafeID", req.params.id, 'failed',
                                                               "Mongodb attempted to save the new cafe");
                                                return res.status(500).send(err);
                                            } else {
                                                dbCafe.cafeModel.update({_id: req.params.id}, toUpdate, function(err, nbRow) {
                                                    if (err) {
                                                        console.log(err);
                                                        return res.status(500).send(err);
                                                    } else {
                                                        cafe.save(function(err) {
                                                            if (err) {
                                                                console.log(err);
                                                                audit.logEvent('[mongodb]', 'Cafes ctrl', 'Update', "cafeID", req.params.id, 'failed',
                                                                               "Mongodb attempted to save the new cafe");
                                                                return res.status(500).send(err);
                                                            } else {
                                                                return res.sendStatus(200);
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            } else if(Object.keys(toRemove).length == 0 && Object.keys(toUpdate).length > 0){
                                //just update
                                dbCafe.cafeModel.update({_id: req.params.id}, toUpdate, function(err, nbRow) {
                                    if (err) {
                                        console.log(err);
                                        return res.status(500).send(err);
                                    } else {
                                        cafe.save(function(err) {
                                            if (err) {
                                                console.log(err);
                                                audit.logEvent('[mongodb]', 'Cafes ctrl', 'Update', "cafeID", req.params.id, 'failed',
                                                               "Mongodb attempted to save the new cafe");
                                                return res.status(500).send(err);
                                            } else {
                                                return res.sendStatus(200);
                                            }
                                        });
                                    }
                                });
                            } else if(Object.keys(toRemove).length > 0 && Object.keys(toUpdate).length == 0){
                                //just remove
                                dbCafe.cafeModel.update({_id: req.params.id}, { $unset: toRemove }, function(err, nbRow) {
                                    if (err) {
                                        console.log(err);
                                        return res.status(500).send(err);
                                    } else {
                                        cafe.save(function(err) {
                                            if (err) {
                                                console.log(err);
                                                audit.logEvent('[mongodb]', 'Cafes ctrl', 'Update', "cafeID", req.params.id, 'failed',
                                                               "Mongodb attempted to save the new cafe");
                                                return res.status(500).send(err);
                                            } else {
                                                return res.sendStatus(200);
                                            }
                                        });
                                    }
                                })
                            }
                        }
                    }
                });
            });
        }
    } else {
        return res.send(401);
    }
};

// Get all cafes
exports.list = function (req, res) {
    var token = tokenManager.getToken(req.headers);
    if (token !== null) {
        var actorID = jsonwebtoken.decode(token).id;
        dbCafe.cafeModel.find({}, function (err, cafes) {
            if (err) {
                audit.logEvent('[mongodb]', 'Cafes ctrl', 'List', '', '', 'failed', 'Mongodb attempted to retrieve cafes');
                return res.status(500).send(err);
            } else {
                return res.json(cafes);
            }
        });
        
    } else {
        audit.logEvent('[anonymous]', 'Cafes ctrl', 'List', '', '', 'failed', 'The actor was not authenticated');
        return res.sendStatus(401);
    }
};


// Get a specified cafe
exports.read = function (req, res) {
    var token = tokenManager.getToken(req.headers);
    if (token !== null) {
        var actorID = jsonwebtoken.decode(token).id;
        if (req.params.id !== undefined) {
            dbCafe.cafeModel.findOne({
                _id: req.params.id
            }).lean().exec(function (err, cafe) {
                if (err) {
                    audit.logEvent('[mongodb]', 'Cafes ctrl', 'Read', 'CafeID', req.params.id, 'failed', 'Mongodb attempted to retrieve a cafe');
                    return res.status(500).send(err);
                } else {
                    if(cafe !== null){
                        return res.json(cafe);
                    } else {
                        return res.sendStatus(404);
                    }
                }
            });
        } else {
            audit.logEvent(actorID, 'Cafes ctrl', 'Read', '', '', 'failed',
                'The actor could not retrieve cafe because one or more params of the request was not defined');
            return res.sendStatus(400);
        }

    } else {
        audit.logEvent('[anonymous]', 'Cafes ctrl', 'Read', '', '', 'failed', 'The actor was not authenticated');
        return res.sendStatus(401);
    }
};

// Read its own cafe
exports.readOwn = function (req, res) {
    var token = tokenManager.getToken(req.headers);
    if (token !== null) {
        var actorID = jsonwebtoken.decode(token).id;
        dbUser.userModel.findOne({
            _id:actorID
        }, {
            _id: 0, cafeID: 1
        }).exec(function (err, user) {
            if (err) {
                audit.logEvent('[mongodb]', 'Cafes ctrl', 'Read own', 'CafeID', req.params.id, 'failed', 'Mongodb attempted to retrieve a cafe');
                return res.status(500).send(err);
            } else {
                if(user.cafeID){
                    dbCafe.cafeModel.findOne({
                        _id: user.cafeID
                    }).lean().exec(function (err, cafe) {
                        if (err) {
                            audit.logEvent('[mongodb]', 'Cafes ctrl', 'Read  own', 'CafeID', req.params.id, 'failed', 'Mongodb attempted to retrieve a cafe');
                            return res.status(500).send(err);
                        } else {
                            if(cafe !== null){
                                return res.json(cafe);
                            } else {
                                return res.sendStatus(404);
                            }
                        }
                    });
                } else {
                    return res.json({noCafe: true});
                }
            }
        });
    } else {
        audit.logEvent('[anonymous]', 'Cafes ctrl', 'Read  own', '', '', 'failed', 'The actor was not authenticated');
        return res.sendStatus(401);
    }
};


// Delete a cafe
exports.delete = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if(token != null){
        var actorID = jsonwebtoken.decode(token).id;
        if(req.params.id == undefined){
            audit.logEvent(actorID, 'Cafes ctrl', 'Delete', '', '', 'failed',
                           'The actor could not delete a cafe because one or more params of the request was not defined');
            return res.sendStatus(400);
        } else {
            dbCafe.cafeModel.remove({
                _id: req.params.id
            }, function (err) {
                if (err) {
                    console.log(err);
                    audit.logEvent('[mongodb]', 'Cafes ctrl', 'Delete', 'cafeID', req.params.id, 'failed', 'Mongodb attempted to delete a cafe');
                    return res.status(500).send(err);
                } else {
                    audit.logEvent(actorID, 'Cafes ctrl', 'Delete', 'cafeID', req.params.id, 'succeed', 'The actor successfully deleted a cafe');
                    return res.sendStatus(200);
                }
            });
        }
    } else {
        audit.logEvent('[anonymous]', 'Cafes ctrl', 'Delete', '', '', 'failed', 'The actor was not authenticated');
        return res.send(401);
    }
}