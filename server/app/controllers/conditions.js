var jsonwebtoken    = require('jsonwebtoken');
var dbConditions    = require('../models/conditions');
var dbUser          = require('../models/user');
var audit           = require('../audit-log');
var tokenManager    = require('../token_manager');
var formidable      = require("formidable");


exports.update = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if(token != null){
        var actorID = jsonwebtoken.decode(token).id;
        var form = new formidable.IncomingForm();
        form.parse(req, function(err, fields, files) {
            var content = fields.content;
            dbConditions.conditionsModel.findOne({}, function (err, conditions) {
                if (err) {
                    console.log(err);
                    return res.status(500).send(err);
                } else {
                    if (conditions === null) {
                        return res.sendStatus(401);
                    } else {
                        dbConditions.conditionsModel.update({_id: conditions._id}, {content: content}, function(err, nbRow) {
                            if (err) {
                                console.log(err);
                                return res.status(500).send(err);
                            } else {
                                conditions.save(function(err) {
                                    if (err) {
                                        console.log(err);
                                        audit.logEvent('[mongodb]', 'Condtions ctrl', 'Update', "", "", 'failed',
                                                       "Mongodb attempted to save the condtions");
                                        return res.status(500).send(err);
                                    } else {
                                        return res.sendStatus(200);
                                    }
                                });
                            }
                        });
                    }
                }
            });
        });
    } else {
        return res.send(401);
    }
};


exports.read = function (req, res) {
    dbConditions.conditionsModel.findOne({}).exec(function (err, conditions) {
        if (err) {
            audit.logEvent('[mongodb]', 'Conditions ctrl', 'Read', '', '', 'failed', 'Mongodb attempted to retrieve conditions');
            return res.status(500).send(err);
        } else {
            if(conditions !== null){
                return res.json(conditions);
            } else {
                return res.sendStatus(404);
            }
        }
    });
};