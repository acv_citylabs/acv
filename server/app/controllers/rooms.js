var mongoose        = require('mongoose');
var ObjectId        = require('mongodb').ObjectID;
var dbSession       = require('../models/session');
var dbUser          = require('../models/user');
var audit           = require('../audit-log');
var tokenManager    = require('../token_manager');
var users           = require('./account.js');
var jsonwebtoken    = require('jsonwebtoken');
var formidable      = require('formidable');
var _               = require('underscore');
var keyGenerator    = require("generate-key");
var crypto          = require('crypto');
var nconf           = require('nconf');nconf.file("config/server.json");
var sharedKey       = nconf.get('webrtc').secret;
var expiration      = nconf.get('webrtc').expiration * 1000;
var rotationPeriod  = nconf.get('webrtc').rotationPeriod;

exports.getSignalingToken = function(req, res){
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * token structure : timestamp:roomID:userID:socketID:digest                 *
     * where the digest is :                                                     *
     *            base64(hmac(sharedKey,"timestamp:roomID:userID:socketID"))     *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    var token = tokenManager.token(req.headers);
    if (token !== null) {
        var userID = jsonwebtoken.decode(token).id;
        users.getUsername(userID, function (err, username) {
            if (err) {
                console.log('Error : ' + err);
                return res.status(500).send(err);
            }
            else{
                var room = req.params.roomID;
                var users = room.split("_");
                if ((username !== users[0]) && (username !== users[1])) {
                    console.log('Error : the user '+username+' tried to get a token for the room "' + room+'"');
                    return res.sendStatus(403);
                }
                else{
                    var result = {};
                    var timestamp = Math.floor(new Date().getTime()) + (expiration || 10000) + "";
                    result = timestamp + ":" + room + ":" + username;

                    var hmac = crypto.createHmac('sha1', sharedKey);
                    hmac.update(result);
                    result += ":" + hmac.digest('base64');
//                    console.log('token in getSignalingToken :' , result)
                    return res.status(200).send(result);
                }
            }
        });
    }
}


var token = function (user1, user2, expiration) {
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * token structure : timestamp:roomID:userID:socketID:digest              *
     * where the digest is :                                                     *
     *            base64(hmac(sharedKey,"timestamp:roomID:userID:socketID"))  *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    var room = (user1.localeCompare(user2) < 0) ? user1+'_'+user2 : user2+'_'+user1;
    var result = {};
    var timestamp = Math.floor(new Date().getTime()) + (expiration || 10000) + "";
    
    result = timestamp + ":" + room + ":" + user1;

    var hmac = crypto.createHmac('sha1', sharedKey);
    hmac.update(result);
    
    result += ":" + hmac.digest('base64');
//    console.log('token : '+result);
    return result;
}


exports.join = function (req, res) {
    var token = tokenManager.getToken(req.headers);
    if(token != null){
        var actorID = jsonwebtoken.decode(token).id;
        var form = new formidable.IncomingForm();
        form.parse(req, function(err, fields, files) {
            var sessionID = req.params.id || '';
            if (sessionID === '') {
                audit.logEvent(actorID, 'Rooms ctrl', 'Join', '', '', 'failed',
                               'The actor could not join a room because one or more params of the request was not defined');
                return res.sendStatus(400);
            } else {
                poll({userID: actorID, token: fields.token, sessionID: sessionID}, function(err, data){
                    if (err) {
                        console.log ('Error in join(), returned by poll() :', err);
                        return res.status(500).send(err);
                    } else {
                        return res.json(data);
                    }
                });
            }
        });
    } else {
        audit.logEvent('[anonymous]', 'Rooms ctrl', 'Join', '', '', 'failed','The actor was not authenticated');
        return res.sendStatus(401);
    }
}


exports.read = function (req, res) {
    var token = tokenManager.getToken(req.headers);
    if(token != null){
        var actorID = jsonwebtoken.decode(token).id;
        var form = new formidable.IncomingForm();
        form.parse(req, function(err, fields, files) {
            var sessionID = req.params.id || '';
            if (sessionID === '') {
                audit.logEvent(actorID, 'Rooms ctrl', 'Read', '', '', 'failed',
                               'The actor could not read a room because one or more params of the request was not defined');
                return res.sendStatus(400);
            } else {
                poll({userID: actorID, token: fields.token, sessionID: sessionID}, function(err, data){
                    if (err) {
                        console.log ('Error in read(), returned by poll() : err = ', err, ' params: ', {userID: actorID, token: fields.token, sessionID: sessionID});
                        return res.status(500).send(err);
                    } else {
                        return res.json(data);
                    }
                });
            }
        });
    } else {
        audit.logEvent('[anonymous]', 'Rooms ctrl', 'Read', '', '', 'failed','The actor was not authenticated');
        return res.sendStatus(401); 
    }
};



exports.getToken = function (req, res) {
    var token = tokenManager.getToken(req.headers);
    if(token != null){
        var actorID = jsonwebtoken.decode(token).id;
        var form = new formidable.IncomingForm();
        form.parse(req, function(err, fields, files) {
            var sessionID = req.params.id || '';
            if ((sessionID === '') || (typeof fields.socketID !== 'string') || (fields.socketID.length <10)) {
                audit.logEvent(actorID, 'Rooms ctrl', 'getToken', '', '', 'failed',
                               'getToken() failed because one or more params of the request was not defined');
                return res.sendStatus(400);
            } else {
                poll({userID: actorID, token: fields.token, sessionID: sessionID, socketID: fields.socketID}, function(err, data){
                    if (err) {
                        console.log ('Error in getToken(), returned by poll() :', err);
                        return res.status(500).send(err);
                    } else {
                        return res.json(data);
                    }
                });
            }
        });
    } else {
        audit.logEvent('[anonymous]', 'Rooms ctrl', 'getToken', '', '', 'failed','The actor was not authenticated');
        return res.sendStatus(401); 
    }
};



var updateSessionVideoMappings = function (session, callback) {
    // compute a new video-sending schema for the next cycle
    // 1 - make a list of all eligible peers (those which polled recently)
    // 2 - sort the list by socket IDs (randomises more than user IDs and avoids non-eligible peers, those without socket ID)
    // 3 - write it into the session, with a time-dependant, determinist, modulo, and the time remaining in this cycle
    var connectedPeers = [];
    session.peers.forEach(function(peer) {
        if (peer.socketID && (new Date().getTime() - new Date(peer.lastPoll).getTime() < 5000)) {
            connectedPeers.push(peer.socketID);
        }
    });

    var modulus = Math.floor(new Date().getTime() / (1000*rotationPeriod));
    var remainingRotationTime = ((modulus+1) * 1000 * rotationPeriod) - new Date().getTime();


    if (session.currentRotation !== modulus) {
        dbSession.sessionModel.findOneAndUpdate(
            { _id : session._id },
            { $set : { currentRotation: modulus, activePeers : connectedPeers } },
            { new : true },
            function(err, updatedSession) {
            if (err) {
                console.log('Error in updateSessionVideoMappings(): could not save session :', err);
                callback(err);
            } else {
                callback(null, updatedSession, remainingRotationTime);
            }
        });
    } else {
        callback(null, session, remainingRotationTime);
    }
}

function poll(config, callback){
    dbSession.sessionModel.findOne({ _id : config.sessionID}).exec(function(err, session) {
        if (err) {
            console.log(err);
            callback(err);
        } else {
            if (session !== null) {
                dbUser.userModel.findOne({ _id : config.userID}).exec(function(err, user) {
                    if (err) {
                        console.log(err);
                        callback(err);
                    } else {
                        if (user !== null) {
                            //if the user is an animator and he is the creator of the session OR if the user is a participant
                            if ((user.role === '2' && session.userID === config.userID) || (user.role === '3')) {
                                var token = config.token || null;
                                var $set = {};
                                var $push = {};
                                var found = _.findWhere(session.peers, {userID: config.userID})
                                var foundWithToken = found && config.token && found.token === config.token;
                                var answer = null;
                                
                                //if the user is already in peers' array 
                                if (!found) {
                                    //if session begins in less than 15 minutes OR if session has begun since 15 minutes
                                    if((user.role === '2') ||
                                       ((new Date().getTime() <= new Date(session.start).getTime() + 15*60000)
                                     && (new Date().getTime() >= new Date(session.start).getTime() - 15*60000))){
                                        //if the user is a participant and the animator has already joined OR if the user is an animator and the animator is not in the room
                                        if((!session.animatorJoined && user.role === '2') || (session.animatorJoined && user.role === '3')){
                                            //if there is less than 10 peers in the room
                                            if(session.peers.length < 10){
                                                $set.animatorJoined = true;
                                                token = keyGenerator.generateKey();
                                                var peer = {
                                                    userID: config.userID,
                                                    token: token,
                                                    isAnimator: user.role === '2' ? true : false
                                                };
                                                $push.peers = peer;
                                            } else answer = {ok: false, full: true};
                                        } else answer = {ok: false, animatorFirst: true};
                                    } else if(new Date().getTime() < new Date(session.start).getTime() - 15*60000)
                                         answer = {ok: false, tooSoon: true};
                                    else answer = {ok: false, tooLate: true};
                                } else {
                                    //if the user has a room token AND is the same as the token present in peers' array OR check the datetime of the last poll
                                    if (foundWithToken || (new Date().getTime() -  new Date(found.lastPoll).getTime() > 5000)) {
                                        $set['peers.$.lastPoll'] = new Date();
                                        
                                        if(!foundWithToken){
                                            token = keyGenerator.generateKey();
                                            $set['peers.$.token'] = token;
                                        }
                                        
                                        // create a signaling token if a socketID is provided
                                        var signalingToken = null;
                                        if (config.socketID) {
                                            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
                                             * token structure : timestamp:roomID:userID:socketID:digest                 *
                                             * where the digest is :                                                     *
                                             *            base64(hmac(sharedKey,"timestamp:roomID:userID:socketID"))     *
                                             * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
                                            var timestamp = '' + Math.floor(new Date().getTime() + (expiration || 10000));
                                            signalingToken = [ timestamp, config.sessionID, config.userID, config.socketID ].join(':');

                                            var hmac = crypto.createHmac('sha1', sharedKey);
                                            hmac.update(signalingToken);
                                            signalingToken += ":" + hmac.digest('base64');
                                            $set['peers.$.socketID'] = config.socketID;
                                        }
                                    } else {
                                        answer = {ok: false, duplicate: true};
                                    }
                                }
                                
                                // if there is no answer, we can proceed with the update
                                if (!answer) {
                                    var query = { _id: config.sessionID };
                                    var updates = {};
                                    var options = { new: true };
                                    
                                    if (Object.keys($set).length > 0)  updates.$set = $set;
                                    if (Object.keys($push).length > 0) updates.$push = $push;
                                    else query['peers.userID'] = config.userID; // mandatory, as we use the positional operator $
                                    
                                    dbSession.sessionModel.findOneAndUpdate(query, updates, options, function(err, updatedSession) {
                                        if (err) {
                                            console.log(err);
                                            callback(err);
                                        } else {
                                            // update the session's video mappings (which peer sends to which other peer)
                                            updateSessionVideoMappings(updatedSession, function(err, session, remainingRotationTime) {
                                                if(err){
                                                    console.log('Error in poll(), returned by updateSessionVideoMappings() :', err);
                                                    callback(err);
                                                } else {
                                                    //OK send session details
                                                    beautifySession(session, function (err, session) {
                                                        if(err){
                                                            callback(err);
                                                        } else {
                                                            var result = {
                                                                ok: true,
                                                                session: session,
                                                                token: token,
                                                                remainingRotationTime: remainingRotationTime
                                                            };
                                                            if (signalingToken) result.signalingToken = signalingToken;
                                                            callback(null, result);
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                } else {
                                    callback(null, answer);
                                }
                            } else {
                                callback(null, {ok: false, unauthorized: true});
                            }
                        } else {
                            callback(null, {ok: false, unauthorized: true});
                        }
                    }
                });
            } else {
                callback(null, {ok: false, unauthorized: true});
            }
        }
    });
}

function beautifySession(session, callback){
    function myLoop(i) {
        if (i < session.peers.length) {
            dbUser.userModel.findOne({
                _id: session.peers[i].userID
            }).exec(function (err, user) {
                if (err){
                    console.log(err);
                    callback(err);
                } else {
                    if(user !== null){
                        session.peers[i].firstname = user.firstname;
                        session.peers[i].avatar = user.preferences.avatar;
                    } else {
                        session.peers[i].firstname = 'unknown';
                    }
                    myLoop(i+1);
                }
            });
        } else {
            callback(null, session);
        }
    }
    myLoop(0);
}



















