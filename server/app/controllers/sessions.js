var jsonwebtoken    = require('jsonwebtoken');
var dbSession       = require('../models/session');
var dbUser          = require('../models/user');
var audit           = require('../audit-log');
var tokenManager    = require('../token_manager');
var fs              = require('fs');
var nconf           = require('nconf');
var nodemailer      = require('nodemailer');
var _               = require('underscore');
var keyGenerator    = require("generate-key");
var formidable      = require("formidable");

//Config
nconf.file("config/server.json");
var mailerConfig = nconf.get('mailer');

// Create a session
exports.create = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if(token != null){
        var actorID = jsonwebtoken.decode(token).id;
        var form = new formidable.IncomingForm();
        form.parse(req, function(err, fields, files) {
            if(err){
                console.log(err);
                audit.logEvent('[formidable]', 'Sessions ctrl', 'Create', "title", title, 'failed', 
                               "Formidable attempted to read fields & files");
                return res.status(500).send(err);
            } else {   
                var title = fields.title || '';
                var start = fields.start || '';
                var description = fields.description || '';

                if (title === '' || start === '') {
                    audit.logEvent(actorID, 'Sessions ctrl', 'Create', '', '', 'failed',
                                   'The actor could not create a session because one or more params of the request was not defined');
                    return res.sendStatus(400);
                } else {
                    dbUser.userModel.findOne({
                        _id: actorID
                    }).lean().exec(function (err, user) {
                        if (err){
                            console.log(err);
                            return res.status(500).send(err);
                        } else {
                            if(user.cafeID){
                                var session = new dbSession.sessionModel();
                                session.userID = actorID;
                                session.cafeID = user.cafeID;
                                session.title = title;
                                session.start = start;
                                if(description !== ''){
                                    session.description = description;
                                }

                                var file = files.file;
                                if(file && file.path){
                                    uploadImage(file, undefined, function(err, data) {
                                        if(err){
                                            return res.status(500).send(err);
                                        } else {
                                            if(!data.image){
                                                return res.json({errImage: data});
                                            } else {
                                                session.image = data.image
                                                session.save(function(err) {
                                                    if (err) {
                                                        console.log(err);
                                                        audit.logEvent('[mongodb]', 'Sessions ctrl', 'Create', "title", title, 'failed', 
                                                                       "Mongodb attempted to save the new session");
                                                        return res.status(500).send(err);
                                                    } else {
                                                        return res.sendStatus(200);
                                                    }
                                                });
                                            }
                                        }
                                    });
                                } else {
                                    session.save(function(err) {
                                        if (err) {
                                            console.log(err);
                                            audit.logEvent('[mongodb]', 'Sessions ctrl', 'Create', "title", title, 'failed', 
                                                           "Mongodb attempted to save the new session");
                                            return res.status(500).send(err);
                                        } else {
                                            return res.sendStatus(200);
                                        }
                                    });
                                }

                            } else {
                                return res.json({noCafe: true});
                            }
                        }
                    });
                }
            }
        });
    } else {
        audit.logEvent('[anonymous]', 'Sessions ctrl', 'search', '', '', 'failed','The actor was not authenticated');
        return res.sendStatus(401); 
    }   
};

exports.reminders = function (req, res) {
    var token = tokenManager.getToken(req.headers);
    if (token !== null) {
        var actorID = jsonwebtoken.decode(token).id;
        dbSession.sessionModel.find({
            followers:{$elemMatch: {userID: actorID}}
        }).lean().exec(function (err, sessions) {
            if (err) {
                console.log(err);
                audit.logEvent('[mongodb]', 'Sessions ctrl', 'Reminders', '', '', 'failed', 'Mongodb attempted to find a session');
                return res.status(500).send(err);
            } else {
                for(i=0;i<sessions.length;i++){
                    for(j=0;j<sessions[i].followers.length;j++){
                        if((sessions[i].followers[j].userID != actorID) || (sessions[i].followers[j].reminders.length == 0)){
                            sessions[i].followers.splice(j, 1);
                        }
                    }
                }
                sessions = _.filter(sessions, function (session){
                    return session.followers.length > 0;
                });
                
                function myLoop(i) {
                    if (i < sessions.length) {
                        sessions[i].reminder =  _.findWhere(sessions[i].followers, {userID: actorID});
                        delete sessions[i].followers;
                        dbUser.userModel.findOne({
                            _id: sessions[i].userID
                        }).lean().exec(function (err, user) {
                            if (err){
                                return res.status(500).send(err);
                            } else {
                                if(user !== null){
                                    sessions[i].userID = user.firstname + " " + user.lastname;
                                } else {
                                    sessions[i].userID = 'unknown';
                                }
                                myLoop(i+1);
                            }
                        });
                    } else {
                        return res.json(sessions);
                    }
                }
                myLoop(0);
            }
        });
    } else {
        audit.logEvent('[anonymous]', 'Sessions ctrl', 'Reminders', '', '', 'failed', 'The actor was not authenticated');
        return res.sendStatus(401);
    }
};

exports.cron = function () {
	var beforeYesterday = new Date();
	beforeYesterday.setDate(beforeYesterday.getDate() - 2);
    dbSession.sessionModel.find({
        //$or:[{followers:{$size: 1}}, {followers:{$size: 2}}]
    	followers : {$exists: true, $not: {$size: 0}},
    	start : {$gt : beforeYesterday}
    }).exec(function (err, sessions) {
        if (err) {
            console.log(err);
            audit.logEvent('[mongodb]', 'Sessions ctrl', 'Cron', '', '', 'failed', 'Mongodb attempted to find reminders');
        } else {
            function myLoop(i) {
                if (i < sessions.length) {
                    function mySLoop(j) {
                    	
                        if (j < sessions[i].followers.length) {
                            function myTLoop(k) {
                            	
                                if (k < sessions[i].followers[j].reminders.length) {
                                	var ONE_HOUR = 60 * 60 * 1000;
                                	var ONE_DAY = 24 * 60 * 60 * 1000;
                                	var start = new Date(sessions[i].start);
                                    // There is a shift between UCL server hour and realtime hour.
                                	var oneHourBeforeStart = new Date(start.getTime() - 2*ONE_HOUR);
                                	var oneDayBeforeStart = new Date(start.getTime() - ONE_DAY - ONE_HOUR);
                                	
                                    if(sessions[i].followers[j].reminders[k].sent === false 
                                       && new Date() < new Date(sessions[i].start)
                                       && ((sessions[i].followers[j].reminders[k].when === "0" 
                                       && new Date() >= oneHourBeforeStart)
                                       || (sessions[i].followers[j].reminders[k].when === "1"
                                       && new Date() >= oneDayBeforeStart))){
                                        dbUser.userModel.findOne({
                                            _id: sessions[i].followers[j].userID
                                        }).exec(function (err, user) {
                                            if (err){
                                                console.log(err);
                                            } else {
                                                if(user !== null){
                                                    var when1 = "tomorrow", when2 = "today", subject = "Reminder";
                                                    if(user.language === 'FR'){
                                                        when1 = "demain";
                                                        when2 = "aujourd'hui";
                                                        subject = "Rappel";
                                                    } else if(user.language === 'NL'){
                                                        when1 = "vandaag";
                                                        when2 = "morgen";
                                                        subject = "Herinnering";
                                                    }
                                                    
                                                    var htmlFile = './emails/' + user.language + '_reminder.html';
                                                    var subject = subject;
                                                    var when = when1;
                                                    if(sessions[i].followers[j].reminders[k].when === "0"){
                                                        when = when2;
                                                    }
                                                    var rep = [['[name]', user.firstname + " " + user.lastname], ['[title]', sessions[i].title], ['[when]', when], ['[time]', timeFormat(sessions[i].start)]];
                                                    replaceInFile(htmlFile, rep, function (err, result) {
                                                        if (err) {
                                                            console.log(err);
                                                        } else {
                                                            sendMail({to: user.email, subject: subject, html: result}, function(err){
                                                                if (err) {
                                                                    console.log(err);
                                                                } else {
                                                                    console.log("Reminder email sent to " + user.email);
                                                                    sessions[i].followers[j].reminders[k].sent = true;
                                                                    sessions[i].save(function (err) {
                                                                        if (err) {
                                                                            console.log(err);
                                                                            audit.logEvent('[mongodb]', 'Sessions ctrl', 'Cron', "sessionID", sessions[i]._id, 'failed',
                                                                                           "Mongodb attempted to save the modified session");
                                                                        }
                                                                        // reminder sent!
                                                                        // go to next follower
                                                                        mySLoop(j+1);
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }else{
                                                	console.log("[debug-cron] Follower that needs reminder not found! D= We have a problem Charlie!");                                                	
                                                }
                                            }
                                        });
                                    }else{
                                    	// if reminder doesn't need to be sent yet
                                    	// go to next reminder
                                      myTLoop(k+1);                                    	
                                    }
                                }else{
                                	// no more reminders
                                	// go to next follower
                                  mySLoop(j+1);
                                }
                            }// <- end definition reminder loop
                            // starting reminder loop : 
                            myTLoop(0);
                        }else{
                        	// no more followers
                        	// go to next session
                          myLoop(i+1);
                        }
                    }// <- end definition follower loop
                    //starting follower loop : 
                    mySLoop(0);
                }else{
                	// no more sessions
                	// nothing to do!
                }
            }// <- end definition session loop
            //starting session loop : 
            myLoop(0);
        }
    });
};

exports.follow = function (req, res) {
    var token = tokenManager.getToken(req.headers);
    if (token !== null) {
        var actorID = jsonwebtoken.decode(token).id;
        var form = new formidable.IncomingForm();
        form.parse(req, function(err, fields, files) {
            var reminders = fields.reminders || [];
            var unfollow = fields.unfollow || false;

            if (req.params.id == undefined) {
                audit.logEvent(actorID, 'Sessions ctrl', 'Follow', '', '', 'failed',
                               'The actor could not update the session because one or more params of the request was not defined');
                return res.sendStatus(400);
            } else {
                dbSession.sessionModel.findOne({
                    _id: req.params.id
                }).exec(function (err, session) {
                    if (err) {
                        audit.logEvent('[mongodb]', 'Sessions ctrl', 'Follow', '', '', 'failed', 'Mongodb attempted to find a session');
                        return res.status(500).send(err);
                    } else {
                        if(session !== null){
                            var remindersToSave = [];
                            for(i=0;i<reminders.length;i++){
                                remindersToSave.push({
                                    when:reminders[i],
                                    sent: false
                                });
                            };
                            var found = _.findWhere(session.followers, {userID: actorID});                    
                            if (found) {
                                if(unfollow){
                                    for(i=0;i<session.followers.length;i++){
                                        if(session.followers[i].userID === actorID){
                                            session.followers.splice(i, 1);
                                        }
                                    }
                                } else {
                                    found.reminders = remindersToSave;
                                }
                            } else {
                                session.followers.push({
                                    userID: actorID,
                                    reminders: remindersToSave
                                });
                            }
                            session.save(function (err) {
                                if (err) {
                                    console.log(err);
                                    audit.logEvent('[mongodb]', 'Sessions ctrl', 'Follow', "sessionID", req.params.id, 'failed',
                                                   "Mongodb attempted to save the modified session");
                                    return res.status(500).send(err);
                                } else {
                                    return res.sendStatus(200);
                                }
                            });
                        } else {
                            return res.sendStatus(500);
                        }
                    }
                });
            }
        });
    } else {
        audit.logEvent('[anonymous]', 'Sessions ctrl', 'Enable reminder', '', '', 'failed', 'The actor was not authenticated');
        return res.sendStatus(401);
    }
};

exports.toggle = function (req, res) {
    var token = tokenManager.getToken(req.headers);
    if (token !== null) {
        var actorID = jsonwebtoken.decode(token).id;
        var form = new formidable.IncomingForm();
        form.parse(req, function(err, fields, files) {
            if (fields.id == undefined) {
                audit.logEvent(actorID, 'Sessions ctrl', 'Toggle', '', '', 'failed',
                               'The actor could not update the session because one or more params of the request was not defined');
                return res.sendStatus(400);
            } else {
                dbSession.sessionModel.findOne({
                    _id: fields.id
                }).exec(function (err, session) {
                    if (err) {
                        audit.logEvent('[mongodb]', 'Sessions ctrl', 'Toggle', '', '', 'failed', 'Mongodb attempted to update a session');
                        return res.status(500).send(err);
                    } else {
                        dbUser.userModel.findOne({
                            _id: actorID
                        }).lean().exec(function (err, user) {
                            if (err){
                                return res.status(500).send(err);
                            } else {
                                if(user.role === '1' && session.disabled){
                                    session.disabled = false;
                                } else {
                                    session.disabled = true;
                                }
                                session.save(function (err) {
                                    if (err) {
                                        console.log(err);
                                        audit.logEvent('[mongodb]', 'Sessions ctrl', 'Toggle', "sessionID", fields.id, 'failed',
                                                       "Mongodb attempted to save the modified session");
                                        return res.status(500).send(err);
                                    } else {
                                        function myLoop(i) {
                                            if (i < session.followers.length) {
                                                dbUser.userModel.findOne({
                                                    _id: session.followers[i].userID
                                                }).lean().exec(function (err, follower) {
                                                    if (err){
                                                        return res.status(500).send(err);
                                                    } else {
                                                        if(follower !== null){
                                                            var subject1 = "The session has been cancelled", subject2 = "The session has been reenabled";
                                                            if(user.language === 'FR'){
                                                                subject1 = "La séance a été annulée";
                                                                subject2 = "La séance a été réactivée";
                                                            }
                                                            
                                                            var rep = [
                                                                ['[name]', follower.firstname + " " + follower.lastname],
                                                                ['[title]', session.title],
                                                                ['[date]', dateFormatFromISO(session.start)],
                                                                ['[link]', "https://" + req.headers.host]
                                                            ];
                                                            var htmlFile = './emails/' + follower.language + '_disabledSession.html';
                                                            var subject = subject1;
                                                            if(session.disabled === false){
                                                                htmlFile = './emails/' + follower.language + '_reenableSession.html';
                                                                subject = subject2;
                                                            }
                                                            replaceInFile(htmlFile, rep, function (err, result) {
                                                                if (err) {
                                                                    console.log(err);
                                                                    return res.status(500).send(err);
                                                                } else {
                                                                    sendMail({to: follower.email, subject: subject, html: result}, function(err){
                                                                        if (err) {
                                                                            console.log(err);
                                                                            return res.status(500).send(err);
                                                                        }
                                                                        myLoop(i+1);
                                                                    });
                                                                }
                                                            });
                                                        } else {
                                                            myLoop(i+1);
                                                        }
                                                    }
                                                });
                                            } else {
                                                return res.sendStatus(200);
                                            }
                                        }
                                        myLoop(0);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    } else {
        audit.logEvent('[anonymous]', 'Sessions ctrl', 'Toggle', '', '', 'failed', 'The actor was not authenticated');
        return res.sendStatus(401);
    }
};

exports.update = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if(token != null){
        var actorID = jsonwebtoken.decode(token).id;
        if(req.params.id == undefined){
            audit.logEvent(actorID, 'Sessions ctrl', 'Update', '', '', 'failed',
                           'The actor could not update a session because one or more params of the request was not defined');
            return res.sendStatus(400);
        } else {
            var form = new formidable.IncomingForm();
            form.parse(req, function(err, fields, files) {
                var title = fields.title;
                var start = fields.start;
                var description = fields.description;
                var toUpdate = {};
                var toRemove = {};

                // -- Required if exist --
                if (title !== undefined && title != ''){
                    toUpdate['title'] = title;
                }
                if (start !== undefined && start != ''){
                    toUpdate['start'] = start;
                }

                // -- Not Required --
                if (description !== undefined && description !== ''){
                    toUpdate['description'] = description
                } else if (description === ''){
                    toRemove['description'] = 1
                };
                
                dbSession.sessionModel.findOne({_id: req.params.id}, function (err, session) {
                    if (err) {
                        console.log(err);
                        return res.status(500).send(err);
                    } else {
                        if (session === null) {
                            return res.sendStatus(401);
                        } else {
                            if(Object.keys(toRemove).length > 0 && Object.keys(toUpdate).length > 0){
                                //remove AND update
                                dbSession.sessionModel.update({_id: req.params.id}, { $unset: toRemove }, function(err, nbRow) {
                                    if (err) {
                                        console.log(err);
                                        return res.status(500).send(err);
                                    } else {
                                        session.save(function(err) {
                                            if (err) {
                                                console.log(err);
                                                audit.logEvent('[mongodb]', 'Sessions ctrl', 'Update', "sessionID", req.params.id, 'failed',
                                                               "Mongodb attempted to save the new session");
                                                return res.status(500).send(err);
                                            } else {
                                                dbSession.sessionModel.update({_id: req.params.id}, toUpdate, function(err, nbRow) {
                                                    if (err) {
                                                        console.log(err);
                                                        return res.status(500).send(err);
                                                    } else {
                                                        session.save(function(err) {
                                                            if (err) {
                                                                console.log(err);
                                                                audit.logEvent('[mongodb]', 'Sessions ctrl', 'Update', "sessionID", req.params.id, 'failed',
                                                                               "Mongodb attempted to save the new session");
                                                                return res.status(500).send(err);
                                                            } else {
                                                                return res.sendStatus(200);
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            } else if(Object.keys(toRemove).length == 0 && Object.keys(toUpdate).length > 0){
                                //just update
                                dbSession.sessionModel.update({_id: req.params.id}, toUpdate, function(err, nbRow) {
                                    if (err) {
                                        console.log(err);
                                        return res.status(500).send(err);
                                    } else {
                                        var file = files.file;
                                        if(file && file.path){
                                            uploadImage(file, session.image, function(err, data) {
                                                if(err){
                                                    console.log(err);
                                                    return res.status(500).send(err);
                                                } else {
                                                    if(!data.image){
                                                        return res.json({errImage: data});
                                                    } else {
                                                        session.image = data.image
                                                        session.save(function(err) {
                                                            if (err) {
                                                                console.log(err);
                                                                audit.logEvent('[mongodb]', 'Sessions ctrl', 'Update', "sessionID", req.params.id, 'failed',
                                                                               "Mongodb attempted to save the new session");
                                                                return res.status(500).send(err);
                                                            } else {
                                                                return res.sendStatus(200);
                                                            }
                                                        });
                                                    }
                                                }
                                            });
                                        } else {
                                            session.save(function(err) {
                                                if (err) {
                                                    console.log(err);
                                                    audit.logEvent('[mongodb]', 'Sessions ctrl', 'Update', "sessionID", req.params.id, 'failed',
                                                                   "Mongodb attempted to save the new session");
                                                    return res.status(500).send(err);
                                                } else {
                                                    return res.sendStatus(200);
                                                }
                                            });
                                        }
                                    }
                                });
                            } else if(Object.keys(toRemove).length > 0 && Object.keys(toUpdate).length == 0){
                                //just remove
                                dbSession.sessionModel.update({_id: req.params.id}, { $unset: toRemove }, function(err, nbRow) {
                                    if (err) {
                                        console.log(err);
                                        return res.status(500).send(err);
                                    } else {
                                        var file = files.file;
                                        if(file && file.path){
                                            uploadImage(file, session.image, function(err, data) {
                                                if(err){
                                                    console.log(err);
                                                    return res.status(500).send(err);
                                                } else {
                                                    if(!data.image){
                                                        return res.json({errImage: data});
                                                    } else {
                                                        session.image = data.image
                                                        session.save(function(err) {
                                                            if (err) {
                                                                console.log(err);
                                                                audit.logEvent('[mongodb]', 'Sessions ctrl', 'Update', "sessionID", req.params.id, 'failed',
                                                                               "Mongodb attempted to save the new session");
                                                                return res.status(500).send(err);
                                                            } else {
                                                                return res.sendStatus(200);
                                                            }
                                                        });
                                                    }
                                                }
                                            });
                                        } else {
                                            session.save(function(err) {
                                                if (err) {
                                                    console.log(err);
                                                    audit.logEvent('[mongodb]', 'Sessions ctrl', 'Update', "sessionID", req.params.id, 'failed',
                                                                   "Mongodb attempted to save the new session");
                                                    return res.status(500).send(err);
                                                } else {
                                                    return res.sendStatus(200);
                                                }
                                            });
                                        }
                                    }
                                })
                            }
                        }
                    }
                });
            });
        }
    } else {
        return res.send(401);
    }
};

// Get all sessions
exports.list = function (req, res) {
    var token = tokenManager.getToken(req.headers);
    if (token !== null) {
        var actorID = jsonwebtoken.decode(token).id;
        var checkFollowed = false;
        var checkFollowers = false;
        var checkPeers = false;
        var query = {
            start: {
                $gt: new Date(new Date().setDate(new Date().getDate() - 365))
            }
        };

        if(req.params.from != 'undefined'){
            query.start.$gt = req.params.from;
        }
        
        if(req.params.to != 'undefined'){
            query.start.$lt = req.params.to;
        }

        dbUser.userModel.findOne({
            _id: actorID
        }).exec(function (err, user) {
            if (err) {
                console.log(err);
                audit.logEvent('[mongodb]', 'Sessions ctrl', 'List', '', '', 'failed', 'Mongodb attempted to retrieve a user');
                return res.status(500).send(err);
            } else {
                if (user.role == "3") {
                    checkFollowed = true;
                } else if (user.role == "2") {
                    checkPeers = true;
                    checkFollowers = true;
                    query.userID = actorID;
                } else if (user.role == "1") {
                    checkPeers = true;
                    checkFollowers = true;
                }
                
                dbSession.sessionModel.find(query).lean().exec(function (err, sessions) {
                    if (err) {
                        console.log(err);
                        audit.logEvent('[mongodb]', 'Sessions ctrl', 'List', '', '', 'failed', 'Mongodb attempted to retrieve sessions');
                        return res.status(500).send(err);
                    } else {
                        function myLoop(i) {
                            if (i < sessions.length) {
                                dbUser.userModel.findOne({
                                    _id: sessions[i].userID
                                }).lean().exec(function (err, user) {
                                    if (err){
                                        console.log(err);
                                        return res.status(500).send(err);
                                    } else {
                                        if(user !== null){
                                            sessions[i].userID = user.firstname + " " + user.lastname;
                                        } else {
                                            sessions[i].userID = 'unknown';
                                        }

                                        if(checkFollowers){
                                            sessions[i].followers = sessions[i].followers.length;
                                        }

                                        if(checkPeers){
                                            sessions[i].peers = sessions[i].peers.length;
                                        }

                                        if(checkFollowed && sessions[i].followers){
                                            for(j=0;j<sessions[i].followers.length; j++){
                                                if(sessions[i].followers[j].userID == actorID){
                                                    sessions[i].followed = true;
                                                }
                                            }
                                            myLoop(i+1);
                                        } else {
                                            myLoop(i+1);
                                        }
                                    }
                                });
                            } else {
                                return res.json(sessions);
                            }
                        }
                        myLoop(0);
                    }
                });
            }
        });
    } else {
        audit.logEvent('[anonymous]', 'Sessions ctrl', 'List', '', '', 'failed', 'The actor was not authenticated');
        return res.sendStatus(401);
    }
};


// Get a specified session
exports.read = function (req, res) {
    var token = tokenManager.getToken(req.headers);
    if (token !== null) {
        var actorID = jsonwebtoken.decode(token).id;
        if (req.params.id !== undefined) {
            dbSession.sessionModel.findOne({
                _id: req.params.id
            }).lean().exec(function (err, session) {
                if (err) {
                    console.log(err);
                    audit.logEvent('[mongodb]', 'Sessions ctrl', 'Read', 'sessionID', req.params.id, 'failed', 'Mongodb attempted to retrieve a session');
                    return res.status(500).send(err);
                } else {
                    if(session !== null){
                        return res.json(session);
                    } else {
                        return res.sendStatus(404);
                    }
                }
            });
        } else {
            audit.logEvent(actorID, 'Sessions ctrl', 'Read', '', '', 'failed',
                'The actor could not retrieve a session because one or more params of the request was not defined');
            return res.sendStatus(400);
        }

    } else {
        audit.logEvent('[anonymous]', 'Sessions ctrl', 'Read', '', '', 'failed', 'The actor was not authenticated');
        return res.sendStatus(401);
    }
};

// Get a specified session
exports.print = function (req, res) {
    var token = tokenManager.getToken(req.headers);
    if (token !== null) {
        var actorID = jsonwebtoken.decode(token).id;
        if (req.params.id !== undefined) {
            dbSession.sessionModel.findOne({
                _id: req.params.id
            }).lean().exec(function (err, session) {
                if (err) {
                    console.log(err);
                    return res.status(500).send(err);
                } else {
                    if(session !== null){
                        var animatorPosition = -1;
                        function myLoop(i) {
                            if (i < session.peers.length) {
                                if(!session.peers[i].isAnimator){
                                    dbUser.userModel.findOne({
                                        _id: session.peers[i].userID
                                    }).lean().exec(function (err, user) {
                                        if (err){
                                            console.log(err);
                                            return res.status(500).send(err);
                                        } else {
                                            if(user !== null){
                                                session.peers[i].name = user.firstname + " " + user.lastname;
                                                session.peers[i].who = user.who;
                                            } else {
                                                session.peers[i].name = 'unknown';
                                                session.peers[i].who = '';
                                            }
                                            myLoop(i+1);
                                        }
                                    });
                                } else {
                                    animatorPosition = i;
                                    myLoop(i+1);
                                }
                            } else {
                                dbUser.userModel.findOne({
                                    _id: session.userID
                                }).lean().exec(function (err, animator) {
                                    if (err){
                                        console.log(err);
                                        return res.status(500).send(err);
                                    } else {
                                        if(animator !== null){
                                            session.animator = animator.firstname + " " + animator.lastname;
                                        } else {
                                            session.animator = '';
                                        }
                                        if(animatorPosition != -1){
                                            session.peers.splice(animatorPosition, 1);
                                        }
                                        session.followers = session.followers.length;
                                        return res.json(session);
                                    }
                                });
                            }
                        }
                        myLoop(0);
                    } else {
                        return res.sendStatus(404);
                    }
                }
            });
        } else {
            audit.logEvent(actorID, 'Sessions ctrl', 'Print', '', '', 'failed',
                'The actor could not retrieve a session because one or more params of the request was not defined');
            return res.sendStatus(400);
        }

    } else {
        audit.logEvent('[anonymous]', 'Sessions ctrl', 'Print', '', '', 'failed', 'The actor was not authenticated');
        return res.sendStatus(401);
    }
};

// Delete a session
exports.delete = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if(token != null){
        var actorID = jsonwebtoken.decode(token).id;
        if(req.params.id == undefined){
            audit.logEvent(actorID, 'Sessions ctrl', 'Delete', '', '', 'failed',
                           'The actor could not delete a session because one or more params of the request was not defined');
            return res.sendStatus(400);
        } else {
            dbSession.sessionModel.remove({
                _id: req.params.id
            }, function (err) {
                if (err) {
                    console.log(err);
                    audit.logEvent('[mongodb]', 'Sessions ctrl', 'Delete', 'sessionID', req.params.id, 'failed', 'Mongodb attempted to delete a session');
                    return res.status(500).send(err);
                } else {
                    audit.logEvent(actorID, 'Sessions ctrl', 'Delete', 'sessionID', req.params.id, 'succeed', 'The actor successfully deleted a session');
                    return res.sendStatus(200);
                }
            });
        }
    } else {
        audit.logEvent('[anonymous]', 'Sessions ctrl', 'Delete', '', '', 'failed', 'The actor was not authenticated');
        return res.send(401);
    }
};


function uploadImage(file, old, callback) {
    if(file.size < 5242880){
        var name = file.name;
        var fileExtension = name.substr(name.lastIndexOf("."), name.length - 1)
        var tmpFile = "./tmp/" + keyGenerator.generateKey() + fileExtension;

        if (!fs.existsSync("./tmp")) {
            fs.mkdirSync("./tmp");
        }

        var gm = require('gm');
        var gmFile = gm(file.path);
        gmFile.size(function(err, value){
            if(!err){
                if(value){
                    var min = value.width >= value.height ? value.height : value.width;
                    gmFile.gravity('Center')
                    .crop(min, min, 0, 0)
                    .resize(200, 200)
                    .write(tmpFile , function (err) {
                        if (!err){
                                //Read the file
                                fs.readFile(tmpFile, function (err, data) {
                                    if(err){
                                        console.log(err);
                                        callback(err);
                                    } else {
                                        var key = keyGenerator.generateKey();
                                        var newPath = "./public/files/sessions/" + key + "/image" + fileExtension;

                                        //Delete the old image and the old folder
                                        if(old){
                                            deleteFolderRecursive("./public/files/sessions/" + old.substr(old, old.indexOf("/")));
                                        }

                                        //Create the new folder
                                        fs.mkdir("./public/files/sessions/" + key, function(err) {
                                            if (err) {
                                                console.log(err);
                                                audit.logEvent('[fs]', 'Sessions ctrl', 'Upload image', "Folder path",
                                                               './public/files/sessions/' + key, 'failed','FS attempted to create a folder');
                                                callback(err);
                                            } else {
                                                //Create the new file
                                                fs.writeFile(newPath, data, function (err) {
                                                    if (err) {
                                                        console.log(err);
                                                        audit.logEvent('[fs]', 'Sessions ctrl', 'Upload image', "File path", newPath, 'failed',
                                                                       'FS attempted to create a file');
                                                        callback(err);
                                                    } else {
                                                        fs.unlink(tmpFile, function (err) {
                                                            if (err) {
                                                                console.log(err);
                                                                audit.logEvent('[fs]', 'Sessions ctrl', 'Upload image', "Temp file", tmpFile, 'failed', 'FS attempted to delete this temp file');
                                                                callback(err);
                                                            } else {
                                                                callback(null, {image: key + "/image" + fileExtension});
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                        } else {
                            console.log(err);
                            callback(err);
                        }
                    });
                } else {
                    audit.logEvent(userID, 'Sessions ctrl', 'Upload image', '', '', 'failed',
                       'The user tried to upload a photo but the file was not an image');
                    callback(null, {notImage: true});
                }
            } else {
                console.log(err);
                callback(err);
            }
        });
    }
    else{
        audit.logEvent(userID, 'Sessions ctrl', 'Upload image', '', '', 'failed',
                   'The user tried to upload a photo but the file was too large');
        callback(null, {tooLarge: true});
    }
};

function addZero(str){
    str = str.toString();
    if(str.length == 1){
        str = "0" + str;
    }
    return str;
};

function timeFormat(date){
    var h = addZero(date.getHours());
    var m = addZero(date.getMinutes());
    return h + "H" + m;
};

function dateFormatFromISO(str){
    date = new Date(str);
    var d = addZero(date.getDate());
    var m = addZero(date.getMonth()+1);
    var y = date.getFullYear();
    return d + '/' + m + '/' + y;
};


function sendMail(mail, callback) {
    var smtpTransport = nodemailer.createTransport({
        host: mailerConfig.host,
        port: mailerConfig.port,
        secure: true,
        auth: {
            user: mailerConfig.auth.user,
            pass: mailerConfig.auth.pass
        }
    });

    var mailOptions = {
        to: mail.to,
        from: mailerConfig.sender.name + ' <' + mailerConfig.sender.address + '>',
        subject: mail.subject,
        html: mail.html
    };
    
    smtpTransport.sendMail(mailOptions, function(err) {
        if(err){
            console.log(err);
            callback(err);
        }
        callback(null);
    });
}
function escapeRegExp(string) {
    return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function replaceAll(string, find, replace) {
    return string.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function replaceInFile(file, toReplace, callback){
    fs.readFile(file, 'utf8', function (err, data) {
        if (err) {
            callback(err);
        }
        for(var i=0;i<toReplace.length;i++){
            for(var j=0;j<toReplace[i].length-1;j++){
                data = replaceAll(data, toReplace[i][j], toReplace[i][j+1]);
            }
        }
        callback(null, data);
    });
};

function deleteFolderRecursive(path) {
    var files = [];
    if( fs.existsSync(path) ) {
        files = fs.readdirSync(path);
        files.forEach(function(file,index){
            var curPath = path + "/" + file;
            if(fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};