var jsonwebtoken    = require('jsonwebtoken');
var dbUser          = require('../models/user');
var tokenManager    = require('../token_manager');
var audit           = require('../audit-log');
var _               = require('underscore');
var formidable      = require("formidable");


exports.nav = function(req,res){
    var token = tokenManager.getToken(req.headers);
    if(token !== null){
        var decodedToken = jsonwebtoken.decode(token);
        var userID = decodedToken.id;
        dbUser.userModel.findOne({_id: userID}, function (err, user) {
            if(err){
                console.log(err);
                return res.status(500).send(err);
            } else {
                if (user !== null) {
                    buildNav(user, function(navBuilt){
                        return res.json({
                            nav: navBuilt
                        });
                    });
                } else {
                    return res.sendStatus(401);
                }
            }
        });
    } else {
        return res.sendStatus(401);
    }
};


exports.home = function(req,res){
    var token = tokenManager.getToken(req.headers);
    if(token !== null){
        var decodedToken = jsonwebtoken.decode(token);
        var userID = decodedToken.id;
        dbUser.userModel.findOne({_id: userID}, function (err, user) {
            if(err){
                console.log(err);
                return res.status(500).send(err);
            } else {
                if (user !== null) {
                    buildHome(user, function(homes){
                        return res.json({
                            homes: homes
                        });
                    });
                } else {
                    return res.sendStatus(401);
                }
            }
        });
    } else {
        return res.sendStatus(401);
    }
};


exports.bookmark = function (req, res) {
    var token = tokenManager.getToken(req.headers);
    if (token !== null) {
        var actorID = jsonwebtoken.decode(token).id;
        if (req.body.id == undefined) {
            audit.logEvent(actorID, 'Tips ctrl', 'Bookmark', '', '', 'failed',
                           'The user could not bookmark or unbookmark the tip because one or more params of the request was not defined');
            return res.send(400);
        } else {
            dbUser.userModel.findOne({
                _id: actorID
            }).exec(function (err, user) {
                if (err) {
                    audit.logEvent('[mongodb]', 'Tips ctrl', 'Bookmark', '', '', 'failed', 'Mongodb attempted to modify a user');
                    return res.status(500).send(err);
                } else {
                    var bookmarked;
                    var found = user.preferences.tips.indexOf(req.body.id);
                    if (found > -1) {
                        user.preferences.tips.splice(found, 1);
                        bookmarked = false;
                    } else {
                        user.preferences.tips.push(req.body.id);
                        bookmarked = true;
                    }
                    user.save(function (err) {
                        if (err) {
                            console.log(err);
                            audit.logEvent('[mongodb]', 'Tips ctrl', 'Bookmark', "username", user.username, 'failed',
                                           "Mongodb attempted to save the modified user");
                            return res.status(500).send(err);
                        }
                        res.json({bookmarked: bookmarked});
                    });
                }
            });
        }
    } else {
        audit.logEvent('[anonymous]', 'Tips ctrl', 'Bookmark', '', '', 'failed', 'The user was not authenticated');
        return res.sendStatus(401);
    }
};



exports.appToggle = function(req,res){
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
        var name = fields.name || '';
        if (name == '') {
            audit.logEvent('[anonymous]', 'UI ctrl', 'Got it', '', '', 'failed','The actor tried to skip an app info but one or more params of the request was not defined');
            return res.sendStatus(400); 
        } else {
            var token = tokenManager.getToken(req.headers);
            if(token !== null){
                var decodedToken = jsonwebtoken.decode(token);
                var actoID = decodedToken.id;
                dbUser.userModel.findOne({_id: actoID}, {"preferences.app":1}, function (err, user) {
                    if(err){
                        console.log(err);
                        return res.status(500).send(err);
                    } else {
                        if (user !== null) {
                        var found = user.preferences.app.indexOf(name);
                        if (found > -1) {
                            user.preferences.app.splice(found, 1);
                        } else {
                            user.preferences.app.push(name);
                        }
                        user.save(function (err) {
                            if (err) {
                                console.log(err);
                                audit.logEvent('[mongodb]', 'UI ctrl', 'Got it', "username", user._id, 'failed', "Mongodb attempted to save the modified user");
                                return res.status(500).send(err);
                            } else {
                                res.sendStatus(200);
                            }
                        });
                        
                        } else {
                            return res.sendStatus(401);
                        }
                    }
                });
            } else {
                return res.sendStatus(401);
            }
        }
    });
};


exports.app = function (req, res) {
    var token = tokenManager.getToken(req.headers);
    if (token !== null) {
        var actorID = jsonwebtoken.decode(token).id;
        if(req.params.name == undefined){
            audit.logEvent(actorID, 'UI ctrl', 'App', '', '', 'failed', 
                           'The user could not read the gotit because one or more params of the request was not defined');
            return res.sendStatus(400);
        } else {
            dbUser.userModel.findOne({_id: actorID }, function (err, user) {
                if (err) {
                    console.log(err);
                    audit.logEvent('[mongodb]', 'UI ctrl', 'App', "user id", actorID, 'failed', "Mongodb attempted to find the user");
                    return res.sendStatus(401);
                } else {
                    if (user === null) {
                        audit.logEvent('[mongodb]', 'UI ctrl', 'App', '', '', 'failed', 'Mongodb attempted to find the user but it revealed not defined');
                        return res.sendStatus(401);
                    } else {
                        if ((user.preferences.app.indexOf(req.params.name) > -1) || (user.role !== '3')) {
                            return res.json({display: false});
                        } else {
                            return res.json({display: true});
                        }
                    }
                }
            });
        }
    } else {
        audit.logEvent('[anonymous]', 'UI ctrl', 'App', '', '', 'failed', 'The user was not authenticated');
        return res.sendStatus(401);
    }
};


function buildNav(user, callback){
    //Top-Left Nav
    var home = {
        href: 'home.main',
        title: 'Home'
    };
    var audit = {
        href: 'home.audit',
        title: 'Audit'
    }; 
    var reminders = {
        href: 'home.reminders',
        title: 'Reminders'
    };
    var rules = {
        href: '',
        title: 'About'
    }; 
    var conditions = {
        href: 'home.conditions',
        title: 'Conditions'
    }; 
    var animators = {
        href: 'home.animators.main',
        title: 'Animators'
    }; 
    var cafes = {
        href: 'home.cafes.main',
        title: 'Cafés'
    }; 
    var myCafe = {
        href: 'home.mycafe',
        title: 'My Café'
    };
    var participants = {
        href: 'home.participants.main',
        title: 'Participants'
    };
    var sessions = {
        href: 'home.sessions.main',
        title: 'Sessions'
    };
    var adminsessions = {
        href: 'home.adminsessions',
        title: 'Sessions'
    };
    var adminconditions = {
        href: 'home.adminconditions',
        title: 'Conditions'
    };
    var video = {
        href: 'home.video',
        title: 'Video'
    };
    
    //Top-Right Nav
    var userBox = 'user';
    
    var nav = {
        left: [],
        right:[]
    };
    
    switch(user.role){
        case '1':
            nav.left.push(home);
            nav.left.push(animators);
            nav.left.push(cafes);
            nav.left.push(adminsessions);
            nav.left.push(adminconditions);
            nav.left.push(audit);
            nav.right.push(userBox);
        break;
        case '2':
            nav.left.push(home);
            nav.left.push(myCafe);
            nav.left.push(participants);
            nav.left.push(sessions);
           // nav.left.push(video);
            nav.right.push(userBox);
        break;
        case '3':
            nav.left.push(home);
            nav.left.push(reminders);
            nav.left.push(rules);
            nav.right.push(userBox);
        break;
    }
    callback(nav);
};

function buildHome(user, callback){
    var homes = [];
    switch(user.role){
        case '1':
            homes.push('administrator');
        break;
        case '2':
            homes.push('animator');
        break;
        case '3':
            homes.push('participant');
        break;
    }
    callback(homes);
};