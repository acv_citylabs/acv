var jsonwebtoken = require('jsonwebtoken');
var fs = require('fs');
var dbUser = require('../models/user');
var dbCafe = require('../models/cafe');
var nconf = require('nconf');
var tokenManager = require('../token_manager');
var audit = require('../audit-log');
var crypto = require('crypto');
var nodemailer = require('nodemailer');
var _ = require('underscore');
var formidable = require("formidable");

//Config
nconf.file("config/server.json");
var mailerConfig = nconf.get('mailer');

exports.create = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if (token != null) {
        var actorID = jsonwebtoken.decode(token).id;
        var form = new formidable.IncomingForm();
        form.parse(req, function(err, fields, files) {
            var email = fields.email || '';
            var firstname = fields.firstname || '';
            var lastname = fields.lastname || '';
            var username = fields.username || '';
            var cafeID = fields.cafeID || '';
            var who = fields.who || '';
            var language = fields.language || '';
            var role;

            if (email === '' || firstname === '' || lastname === '' || username === '') {
                audit.logEvent(actorID, 'Users ctrl', 'Create', '', '', 'failed',
                    'The actor could not create a user account because one or more params of the request was not defined');
                return res.sendStatus(400);
            } else {
                dbUser.userModel.findOne({
                    _id: actorID
                }).exec(function(err, theUser) {
                    if (err) {
                        console.log(err);
                        audit.logEvent('[mongodb]', 'Users ctrl', 'Create', "actorID", actorID, 'failed',
                            "Mongodb attempted to find the actor");
                        return res.status(500).send(err);
                    } else {
                        if (theUser === null) {
                            return res.sendStatus(500);
                        } else {
                            if (theUser.role === '1') {
                                role = 2
                            } else {
                                role = 3
                            }

                            var user = new dbUser.userModel();
                            user.email = email;
                            user.firstname = firstname;
                            user.lastname = lastname;
                            user.username = username;
                            user.role = role;

                            if (language !== '') {
                                user.language = language;
                            }
                            if (cafeID !== '') {
                                user.cafeID = cafeID;
                            }
                            if (who !== '') {
                                user.who = who;
                            }
                            if (role === 3) {
                                user.animatorID = actorID;
                            }

                            crypto.randomBytes(20, function(err, bufPassword) {
                                if (err) {
                                    console.log(err);
                                    return res.status(500).send(err);
                                } else {
                                    var tmpPassword = bufPassword.toString('hex');
                                    user.password = tmpPassword;

                                    //Create a token
                                    crypto.randomBytes(20, function(err, buf) {
                                        if (err) {
                                            console.log(err);
                                            return res.status(500).send(err);
                                        } else {
                                            var myToken = buf.toString('hex');
                                            user.resetPasswordToken = myToken;
                                            user.activationToken = "1";
                                            user.save(function(err) {
                                                if (err) {
                                                    console.log(err);
                                                    audit.logEvent('[mongodb]', 'Users ctrl', 'Create', "username", username, 'failed',
                                                        "Mongodb attempted to save the new user");
                                                    return res.status(500).send(err);
                                                } else {
                                                    var who1 = "Your animator",
                                                        who2 = "The administrator";
                                                    if (user.language === 'FR') {
                                                        who1 = "Votre animateur"
                                                        who2 = "L'administrateur";
                                                    } else if (user.language === 'NL') {
                                                        who1 = "De gespreksleider"
                                                        who2 = "De beheerder";
                                                    }

                                                    var rep = [
                                                        ['[name]', user.firstname + " " + user.lastname],
                                                        ['[who]', who1],
                                                        ['[animator]', theUser.firstname + " " + theUser.lastname],
                                                        ['[link1]', "https://" + req.headers.host + "/conditions/public"],
                                                        ['[link2]', "https://" + req.headers.host + "/signup/" + myToken]
                                                    ];

                                                    if (theUser.role === '1') {
                                                        rep[1][1] = who2;
                                                    }

                                                    replaceInFile('./emails/' + user.language + '_activation.html', rep, function(err, result) {
                                                        if (err) {
                                                            console.log(err);
                                                            return res.status(500).send(err);
                                                        } else {
                                                            //Send an email with a link containing the key

                                                            sendMail({ to: user.email, subject: 'Activation', html: result }, function(err) {
                                                                if (err) {
                                                                    console.log(err);
                                                                    return res.status(500).send(err);
                                                                } else {
                                                                    return res.sendStatus(200);
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });

                                }
                            });
                        }
                    }
                });
            }
        });
    } else {
        audit.logEvent('[anonymous]', 'Users ctrl', 'search', '', '', 'failed', 'The actor was not authenticated');
        return res.sendStatus(401);
    }
};

exports.sendPassword = function(req, res) {
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
        var userID = fields.userID || '';
        if (userID === '') {
            audit.logEvent('[anonymous]', 'Users ctrl', 'Send password', '', '', 'failed',
                'The actor tried to send an email to the user to reset password in but one or more params of the request was not defined');
            return res.sendStatus(400);
        } else {
            dbUser.userModel.findOne({ _id: userID }, function(err, user) {
                if (err) {
                    console.log(err);
                    audit.logEvent('[mongodb]', 'Users ctrl', 'Send password', 'userID', userID, 'failed', 'Mongodb attempted to find the userID');
                    return res.status(500).send(err);
                } else {
                    if (user === null) {
                        audit.logEvent('[anonymous]', 'Users ctrl', 'Send password', 'used email', email, 'failed',
                            'The actor tried to send an email to reset password but the userID does not exist');
                        return res.sendStatus(403);
                    } else {
                        //Create a token
                        crypto.randomBytes(20, function(err, buf) {
                            if (err) {
                                console.log(err);
                                return res.status(500).send(err);
                            } else {
                                var token = buf.toString('hex');
                                user.resetPasswordToken = token;
                                user.resetPasswordExpires = Date.now() + 3600000;
                                user.save(function(err) {
                                    if (err) {
                                        console.log(err);
                                        audit.logEvent('[mongodb]', 'Users ctrl', 'Send password', "username", user.username, 'failed',
                                            "Mongodb attempted to save the modified user");
                                        return res.status(500).send(err);
                                    } else {
                                        var subject = 'New password';
                                        if (user.language === 'FR') {
                                            subject = "Nouveau mot de passe";
                                        }

                                        var rep = [
                                            ['[name]', user.firstname + " " + user.lastname],
                                            ['[link]', "https://" + req.headers.host + "/recovery/reset/" + token]
                                        ];
                                        replaceInFile('./emails/' + user.language + '_reset.html', rep, function(err, result) {
                                            if (err) {
                                                console.log(err);
                                                return res.status(500).send(err);
                                            } else {
                                                //Send an email with a link containing the key
                                                sendMail({ to: user.email, subject: subject, html: result }, function(err) {
                                                    if (err) {
                                                        console.log(err);
                                                        return res.status(500).send(err);
                                                    }
                                                    return res.sendStatus(200);
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }
    });
};

exports.update = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if (token != null) {
        var actorID = jsonwebtoken.decode(token).id;
        if (req.params.username == undefined) {
            audit.logEvent(actorID, 'Users ctrl', 'Update', '', '', 'failed',
                'The actor could not update the account of the user because one or more params of the request was not defined');
            return res.sendStatus(400);
        } else {
            dbUser.userModel.findOne({
                username: req.params.username
            }, {
                _id: 1
            }).exec(function(err, user) {
                var form = new formidable.IncomingForm();
                form.parse(req, function(err, fields, files) {
                    var phone = fields.phone;
                    var cafeID = fields.cafeID;
                    var email = fields.email;
                    var firstname = fields.firstname;
                    var lastname = fields.lastname;
                    var language = fields.language;
                    var who = fields.who;

                    var toUpdate = {};
                    var toRemove = {};

                    // -- Required if exist --
                    if (firstname !== undefined && firstname != '') {
                        toUpdate['firstname'] = firstname;
                    }
                    if (lastname !== undefined && lastname != '') {
                        toUpdate['lastname'] = lastname;
                    }
                    if (email !== undefined && email != '') {
                        toUpdate['email'] = email;
                    }
                    if (language !== undefined && language != '') {
                        toUpdate['language'] = language
                    };
                    if (who !== undefined && who != '') {
                        toUpdate['who'] = who
                    };

                    // -- Not Required --
                    if (phone !== undefined && phone !== '') {
                        toUpdate['phone'] = phone
                    } else if (phone === '') {
                        toRemove['phone'] = 1
                    };

                    if (cafeID !== undefined && cafeID !== '') {
                        toUpdate['cafeID'] = cafeID
                    } else if (cafeID === '') {
                        toRemove['cafeID'] = 1
                    };

                    dbUser.userModel.findOne({ _id: user._id }, function(err, user) {
                        if (err) {
                            console.log(err);
                            return res.send(500).send(err);
                        } else {
                            if (user === null) {
                                return res.sendStatus(401);
                            } else {
                                dbUser.userModel.findOne({
                                    _id: actorID
                                }, { role: 1 }).exec(function(err, theUser) {
                                    if (err) {
                                        console.log(err);
                                        audit.logEvent('[mongodb]', 'Users ctrl', 'Update', "actorID", actorID, 'failed',
                                            "Mongodb attempted to find the actor");
                                        return res.status(500).send(err);
                                    } else {
                                        if (theUser === null) {
                                            return res.sendStatus(500);
                                        } else {
                                            if ((user.role === '2' && theUser.role === '1') || (user.role === '3' && theUser.role === '2')) {
                                                if (Object.keys(toRemove).length > 0 && Object.keys(toUpdate).length > 0) {
                                                    //remove AND update
                                                    dbUser.userModel.update({ _id: user._id }, { $unset: toRemove }, function(err, nbRow) {
                                                        if (err) {
                                                            console.log(err);
                                                            return res.status(500).send(err);
                                                        } else {
                                                            user.save(function(err) {
                                                                if (err) {
                                                                    console.log(err);
                                                                    audit.logEvent('[mongodb]', 'Users ctrl', 'Update', "username", user.username, 'failed',
                                                                        "Mongodb attempted to save the new user");
                                                                    return res.status(500).send(err);
                                                                } else {
                                                                    dbUser.userModel.update({ _id: user._id }, toUpdate, function(err, nbRow) {
                                                                        if (err) {
                                                                            console.log(err);
                                                                            return res.status(500).send(err);
                                                                        } else {
                                                                            user.save(function(err) {
                                                                                if (err) {
                                                                                    console.log(err);
                                                                                    audit.logEvent('[mongodb]', 'Users ctrl', 'Update', "username", user.username, 'failed',
                                                                                        "Mongodb attempted to save the new user");
                                                                                    return res.status(500).send(err);
                                                                                } else {
                                                                                    return res.sendStatus(200);
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                } else if (Object.keys(toRemove).length == 0 && Object.keys(toUpdate).length > 0) {
                                                    //just update
                                                    dbUser.userModel.update({ _id: user._id }, toUpdate, function(err, nbRow) {
                                                        if (err) {
                                                            console.log(err);
                                                            return res.status(500).send(err);
                                                        } else {
                                                            user.save(function(err) {
                                                                if (err) {
                                                                    console.log(err);
                                                                    audit.logEvent('[mongodb]', 'Users ctrl', 'Update', "username", user.username, 'failed',
                                                                        "Mongodb attempted to save the new user");
                                                                    return res.status(500).send(err);
                                                                } else {
                                                                    return res.sendStatus(200);
                                                                }
                                                            });
                                                        }
                                                    });
                                                } else if (Object.keys(toRemove).length > 0 && Object.keys(toUpdate).length == 0) {
                                                    //just remove
                                                    dbUser.userModel.update({ _id: user._id }, { $unset: toRemove }, function(err, nbRow) {
                                                        if (err) {
                                                            console.log(err);
                                                            return res.status(500).send(err);
                                                        } else {
                                                            user.save(function(err) {
                                                                if (err) {
                                                                    console.log(err);
                                                                    audit.logEvent('[mongodb]', 'Users ctrl', 'Update', "username", user.username, 'failed',
                                                                        "Mongodb attempted to save the new user");
                                                                    return res.status(500).send(err);
                                                                } else {
                                                                    return res.sendStatus(200);
                                                                }
                                                            });
                                                        }
                                                    })
                                                }
                                            } else {
                                                return res.sendStatus(403);
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    });
                });
            });
        }
    } else {
        return res.send(401);
    }
};


exports.read = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if (token != null) {
        var actorID = jsonwebtoken.decode(token).id;
        if (req.params.username == undefined) {
            audit.logEvent(actorID, 'Users ctrl', 'Read', '', '', 'failed',
                'The actor could not read the user because one or more params of the request was not defined');
            return res.sendStatus(400);
        } else {
            var role;
            var query = {
                _id: 1,
                username: 1,
                role: 1,
                "preferences.avatar": 1,
                created: 1
            };

            if (req.originalUrl.lastIndexOf("/participant") === req.originalUrl.length - 12) {
                role = '3';
                query.email = 1;
                query.firstname = 1;
                query.lastname = 1;
                query.language = 1;
                query.cafeID = 1;
                query.animatorID = 1;
                query.who = 1;
            } else if (req.originalUrl.lastIndexOf("/animator") === req.originalUrl.length - 9) {
                role = '2';
                query.email = 1;
                query.firstname = 1;
                query.lastname = 1;
                query.language = 1;
                query.cafeID = 1;
            }

            dbUser.userModel.findOne({
                username: req.params.username,
                role: role
            }, query).lean().exec(function(err, user) {
                if (err) {
                    console.log(err);
                    audit.logEvent('[mongodb]', 'Users ctrl', 'Read', "username", req.params.username, 'failed',
                        "Mongodb attempted to find the user");
                    return res.status(500).send(err);
                } else {
                    if (user === null) {
                        audit.logEvent('[mongodb]', 'Users ctrl', 'Read', 'username', req.params.username, 'failed',
                            'Mongodb attempted to find the user but it revealed not defined');
                        return res.sendStatus(403);
                    } else {
                        if (user.preferences !== undefined && user.preferences.avatar !== undefined) {
                            user.avatar = user.preferences.avatar;
                            delete user.preferences;
                        }
                        return res.json(user);
                    }
                }
            });
        }
    } else {
        audit.logEvent('[anonymous]', 'Users ctrl', 'Read', '', '', 'failed', 'The actor was not authenticated');
        return res.send(401);
    }
};


exports.delete = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if (token != null) {
        var actorID = jsonwebtoken.decode(token).id;
        if (req.params.username == undefined) {
            audit.logEvent(actorID, 'Users ctrl', 'Delete', '', '', 'failed',
                'The actor could not delete a user because one or more params of the request was not defined');
            return res.sendStatus(400);
        } else {
            dbUser.userModel.findOne({
                username: req.params.username
            }, {
                _id: 1
            }).exec(function(err, user) {
                //delete account
                dbUser.userModel.remove({ _id: user._id }, function(err) {
                    if (err) {
                        console.log(err);
                        return res.status(500).send(err);
                    } else {
                        return res.sendStatus(200);
                    }
                });
            });
        }
    } else {
        audit.logEvent('[anonymous]', 'Users ctrl', 'Delete', '', '', 'failed', 'The user was not authenticated');
        return res.send(401);
    }
}

exports.list = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if (token != null) {
        var actorID = jsonwebtoken.decode(token).id;
        dbUser.userModel.findOne({
            _id: actorID
        }).exec(function(err, user) {
            if (err) {
                console.log(err);
                audit.logEvent('[mongodb]', 'Users ctrl', 'List', '', '', 'failed', 'Mongodb attempted to retrieve a user');
                return res.status(500).send(err);
            } else {
                var query = {};
                if (user.role == "1") {
                    query = {
                        role: 2
                    };
                } else if (user.role == "2") {
                    query = {
                        role: 3
                    };
                }

                dbUser.userModel.find(query, function(err, users) {
                    if (err) {
                        console.log(err);
                        audit.logEvent('[mongodb]', 'Users ctrl', 'List', '', '', 'failed', 'Mongodb attempted to retrieve users list');
                        return res.status(500).send(err);
                    } else {
                        if (query.role === 3) {
                            /* Case orga */
                            function myLoop1(i) {
                                if (i < users.length) {
                                    if (users[i].activationToken === '0') {
                                        users[i].activationToken = 'activated';
                                    } else {
                                        users[i].activationToken = 'unactivated';
                                    }

                                    dbUser.userModel.findOne({
                                        _id: users[i].animatorID
                                    }).lean().exec(function(err, animator) {
                                        if (err) {
                                            console.log(err);
                                            return res.status(500).send(err);
                                        } else {
                                            if (animator !== null) {
                                                users[i].animatorID = animator.firstname + " " + animator.lastname;
                                            } else {
                                                users[i].animatorID = 'unknown';
                                            }
                                            myLoop1(i + 1);
                                        }
                                    });
                                } else {
                                    return res.json(users);
                                }
                            }
                            myLoop1(0);
                        } else {
                            /* Case admin */
                            function myLoop2(j) {
                                if (j < users.length) {
                                    if (users[j].cafeID) {
                                        dbCafe.cafeModel.findOne({
                                            _id: users[j].cafeID
                                        }).lean().exec(function(err, cafe) {
                                            if (err) {
                                                console.log(err);
                                                return res.status(500).send(err);
                                            } else {
                                                if (cafe !== null) {
                                                    users[j].cafeID = cafe.name;
                                                } else {
                                                    users[j].cafeID = 'unknown';
                                                }
                                                myLoop2(j + 1);
                                            }
                                        });
                                    } else {
                                        // Users are returned even if one of them has no cafe assigned.
                                    }
                                }
                            }
                            myLoop2(0);
                            return res.json(users);
                        }
                    }
                });
            }
        });
    } else {
        audit.logEvent('[anonymous]', 'Users ctrl', 'List', '', '', 'failed', 'The actor was not authenticated');
        return res.send(401);
    }
}



function sendMail(mail, callback) {
    var smtpTransport = nodemailer.createTransport({
        host: mailerConfig.host,
        port: mailerConfig.port,
        secure: true,
        auth: {
            user: mailerConfig.auth.user,
            pass: mailerConfig.auth.pass
        },
        debug: true,
        logger: true
    });

    var mailOptions = {
        to: mail.to,
        from: mailerConfig.sender.name + ' <' + mailerConfig.sender.address + '>',
        subject: mail.subject,
        html: mail.html
    };

    smtpTransport.sendMail(mailOptions, function(err) {
        if (err) {
            console.log(err);
            callback(err);
        } else {
            callback(null);
        }
    });
}

function escapeRegExp(string) {
    return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function replaceAll(string, find, replace) {
    return string.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function replaceInFile(file, toReplace, callback) {
    fs.readFile(file, 'utf8', function(err, data) {
        if (err) {
            console.log(err);
            callback(err);
        }
        for (var i = 0; i < toReplace.length; i++) {
            for (var j = 0; j < toReplace[i].length - 1; j++) {
                data = replaceAll(data, toReplace[i][j], toReplace[i][j + 1]);
            }
        }
        callback(null, data);
    });
};