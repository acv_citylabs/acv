var jsonwebtoken    = require('jsonwebtoken');
var dbVideo          = require('../models/video');
var dbUser          = require('../models/user');
var audit           = require('../audit-log');
var tokenManager    = require('../token_manager');
var formidable      = require("formidable");

// Create a video
exports.update = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if(token != null){
        
      var form = new formidable.IncomingForm();
      form.parse(req, function(err, fields, files) {
          var link = fields.link;

          dbVideo.videoModel.findOneAndUpdate({name: 'homeVideo'}, 
                                              {link: link},
                                              {upsert : true},
                                              function (err, video) {
              if (err) {
                console.log(err);
                return res.status(500).send(err);
              } else {
              	return res.sendStatus(200);
              }
                  
          });
      });
        
    } else {
        return res.send(401);
    }
};

// Get the home video
exports.getHomeVideo = function (req, res) {
    var token = tokenManager.getToken(req.headers);
    if (token !== null) {
      dbVideo.videoModel.findOne({
          name: 'homeVideo'
      }).exec(function (err, video) {
          if (err) {
              audit.logEvent('[mongodb]', 'Videos ctrl', 'Read', 'VideoID', req.params.id, 'failed', 'Mongodb attempted to retrieve a video');
              return res.status(500).send(err);
          } else {
             return res.json(video);
          }
      });
        
    } else {
        audit.logEvent('[anonymous]', 'Videos ctrl', 'Read', '', '', 'failed', 'The actor was not authenticated');
        return res.sendStatus(401);
    }
};
