var mongoose = require('mongoose');

var Schema = mongoose.Schema;

// Cafe schema
var Cafe = new Schema({
    name: { type: String, required: true },
    description: { type: String, required: false },
    place: { type: String, required: false },
    created: { type: Date, default: Date.now }
}, { collection: 'cafes' });

// Define Model
var cafeModel = mongoose.model('Cafe', Cafe);

// Exports Model
exports.cafeModel = cafeModel;