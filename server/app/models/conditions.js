var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var Conditions = new Schema({
    content: { type: String, required: true },
    updated: { type: Date, default: Date.now }
}, { collection: 'conditions' });

var conditionsModel = mongoose.model('Conditions', Conditions);

exports.conditionsModel = conditionsModel;