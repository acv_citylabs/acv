var mongoose = require('mongoose');

var Schema = mongoose.Schema;

// Session schema
var Session = new Schema({
    userID : { type: String, required: true },
    cafeID : { type: String, required: true },
    title: { type: String, required: true },
    start : { type: Date, required: true },
    followers: [{
        userID: { type: String, required: false },
        reminders: [{
            when: { type: String, required: false },
            sent: { type: Boolean, required: false }
        }]
    }],
    peers: [{
        userID: { type: String, required: false },
        firstname: { type: String, required: false },
        avatar: { type: String, required: false },
        socketID: { type: String, required: false },
        lastPoll: { type: Date, required: false, default: Date.now },
        token: { type: String, required: false },
        peerID: { type: String, required: false },
        isAnimator: { type: Boolean, required: false, default: false }
    }],
    activePeers: [{ type: String, required: false }],
    currentRotation: { type: Number, required: false },
    animatorJoined: { type: Boolean, required: false },
    description: { type: String, required: false },
    image: { type: String, required: false },
    disabled: { type: Boolean, default: false },
    created: { type: Date, default: Date.now }
});

// Define Model
var sessionModel = mongoose.model('Session', Session);

// Exports Model
exports.sessionModel = sessionModel;