var mongoose        = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var bcrypt          = require('bcrypt');

var SALT_WORK_FACTOR = 10;
var Schema = mongoose.Schema;

// User schema
var User = new Schema({
    firstname: { type: String, required: true },
    lastname: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    activationToken: { type: String, required: true },
    resetPasswordToken: { type: String, required: false },
    resetPasswordExpires: { type: Date, required: false },
    role: { type: String, required: true },
    who: { type: String, required: false },
    language: { type: String, default: 'EN' },
    cafeID: { type: String, required: false},
    animatorID: { type: String, required: false},
    phone: { type: String, required: false },
    homeAddress: { type: String, required: false },
    preferences: {
        app: { type: [String], required: false},
        avatar: { type: String, required: false }
    },
    created: { type: Date, default: Date.now }
});

// Apply the uniqueValidator plugin to schema
User.plugin(uniqueValidator);

// Bcrypt middleware on UserSchema
User.pre('save', function(next) {
    var user = this;
    if (!user.isModified('password')) return next();
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);
            user.password = hash;
            next();
        });
    });
});

//Password verification
User.methods.comparePassword = function(password, cb) {
    bcrypt.compare(password, this.password, function(err, isMatch) {
        if (err) return cb(err);
         cb(isMatch);
       //cb(true);
    });
};


//Define Models
var userModel = mongoose.model('User', User);


// Export Models
exports.userModel = userModel;