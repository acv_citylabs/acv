var mongoose = require('mongoose');

var Schema = mongoose.Schema;

// Video schema
var Video = new Schema({
  name : { type: String, required: true },  
	link: { type: String, required: true }
});

// Define Model
var videoModel = mongoose.model('Video', Video);

// Exports Model
exports.videoModel = videoModel;