var jwt             = require('express-jwt');
var path            = require('path');
var jsonwebtoken    = require('jsonwebtoken');
var CronJob         = require('cron');
var _               = require('underscore');
var tokenManager    = require('./token_manager');
var nconf           = require('nconf');nconf.file("config/server.json");
var secret          = nconf.get('token').secret;
var activatorConfig = nconf.get('activator');
var dbUser          = require('./models/user');
var dbConditions    = require('./models/conditions');
var aclRoutes       = require('./acl/routes.json');
var aclRoles        = require('./acl/roles.json');
var audit           = require('./audit-log');


// Controllers
var controllers = {};
controllers.account = require('./controllers/account');
controllers.rooms = require('./controllers/rooms');
controllers.audit = require('./controllers/audit');
controllers.cafes = require('./controllers/cafes');
controllers.conditions = require('./controllers/conditions');
controllers.sessions = require('./controllers/sessions');
controllers.video = require('./controllers/video');
controllers.users = require('./controllers/users');
controllers.ui = require('./controllers/ui');
controllers.angular = function(req, res) {res.sendFile(path.join(__dirname, '../public', 'index.html'));};


firstLaunch();
sendReminders();

// Routes
var routes = [
    
    // API ROUTES
		// ===============================================================
    // === ACCOUNT ROUTES
		// ========================================================
    // Send a link to reset user's passowrd
    {
        path: _.findWhere(aclRoutes, {id: 54}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 54}).method,
        middleware: [controllers.account.lostPassword]
    },
    
    // Reset a password
    {
        path: _.findWhere(aclRoutes, {id: 55}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 55}).method,
        middleware: [controllers.account.resetPassword]
    },
    
    // Sign in
    {
        path: _.findWhere(aclRoutes, {id: 0}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 0}).method,
        middleware: [controllers.account.signin]
    },
    
    // Activate a user account
    {
        path: _.findWhere(aclRoutes, {id: 56}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 56}).method,
        middleware: [controllers.account.activation]
    },

    // Sign out
    {
        path: _.findWhere(aclRoutes, {id: 2}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 2}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.account.signout]
    },
    
    // Change user's password
    {
        path: _.findWhere(aclRoutes, {id: 13}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 13}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.account.changePassword]
    },
    
    // Update user infos
    {
        path: _.findWhere(aclRoutes, {id: 33}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 33}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.account.update]
    },
    
    // Get user's self-profile
    {
        path: _.findWhere(aclRoutes, {id: 25}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 25}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.account.profile],
        access: _.findWhere(aclRoutes, {id: 25}).roles
    },
    
    // === UI ROUTES ==========================================================
    // Build user's nav
    {
        path: _.findWhere(aclRoutes, {id: 17}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 17}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.ui.nav],
        access: _.findWhere(aclRoutes, {id: 17}).roles
    },
    
    // Build user's home
    {
        path: _.findWhere(aclRoutes, {id: 18}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 18}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.ui.home],
        access: _.findWhere(aclRoutes, {id: 18}).roles
    },
    
    // Notify that the user got it
    {
        path: _.findWhere(aclRoutes, {id: 61}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 61}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.ui.appToggle],
        access: _.findWhere(aclRoutes, {id: 61}).roles
    },
    
    // Verify if the user got it
    {
        path: _.findWhere(aclRoutes, {id: 62}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 62}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.ui.app],
        access: _.findWhere(aclRoutes, {id: 62}).roles
    },


    // === ROOMS ROUTES ========================================================
    // Join a room
    {
        path: _.findWhere(aclRoutes, {id: 9}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 9}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.rooms.join],
        access: _.findWhere(aclRoutes, {id: 9}).roles
    },
    // Read a room
    {
        path: _.findWhere(aclRoutes, {id: 7}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 7}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.rooms.read],
        access: _.findWhere(aclRoutes, {id: 7}).roles
    },
    // Read a room and get a fresh signaling token
    {
        path: _.findWhere(aclRoutes, {id: 10}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 10}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.rooms.getToken],
        access: _.findWhere(aclRoutes, {id: 10}).roles
    },
    
    // === AUDIT ROUTES ========================================================
    // Retrieve audit logs by date
    {
        path: _.findWhere(aclRoutes, {id: 27}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 27}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.audit.listByDate],
        access: _.findWhere(aclRoutes, {id: 27}).roles
    },

    // === CAFES ROUTES
		// ==========================================================
    // Retrieve cafes
    {
        path: _.findWhere(aclRoutes, {id: 4}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 4}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.cafes.list],
        access: _.findWhere(aclRoutes, {id: 4}).roles
    },
    // Create a cafe
    {
        path: _.findWhere(aclRoutes, {id: 6}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 6}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.cafes.create],
        access: _.findWhere(aclRoutes, {id: 6}).roles  
    },
    // Update a cafe
    {
        path: _.findWhere(aclRoutes, {id: 14}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 14}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.cafes.update],
        access: _.findWhere(aclRoutes, {id: 14}).roles  
    },
    // Read a cafe
    {
        path: _.findWhere(aclRoutes, {id: 12}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 12}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.cafes.read],
        access: _.findWhere(aclRoutes, {id: 12}).roles
    },
    // Read its own cafe
    {
        path: _.findWhere(aclRoutes, {id: 20}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 20}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.cafes.readOwn],
        access: _.findWhere(aclRoutes, {id: 20}).roles
    },
    // Delete a cafe
    {
        path: _.findWhere(aclRoutes, {id: 5}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 5}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.cafes.delete],
        access: _.findWhere(aclRoutes, {id: 5}).roles
    },
    
    // === CONDITIONS ROUTES
		// ==========================================================
    // Retrieve cafes
    // Update conditions
    {
        path: _.findWhere(aclRoutes, {id: 75}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 75}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.conditions.update],
        access: _.findWhere(aclRoutes, {id: 75}).roles  
    },
    // Read conditions
    {
        path: _.findWhere(aclRoutes, {id: 74}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 74}).method,
        middleware: [controllers.conditions.read],
        access: _.findWhere(aclRoutes, {id: 74}).roles
    },
    
    // === SESSIONS ROUTES
		// ==========================================================
    // Retrieve sessions FROM/ROLES
    {
        path: _.findWhere(aclRoutes, {id: 64}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 64}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.sessions.list],
        access: _.findWhere(aclRoutes, {id: 64}).roles
    },
    // Create a session
    {
        path: _.findWhere(aclRoutes, {id: 66}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 66}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.sessions.create],
        access: _.findWhere(aclRoutes, {id: 66}).roles  
    },
    // Update a session
    {
        path: _.findWhere(aclRoutes, {id: 68}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 68}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.sessions.update],
        access: _.findWhere(aclRoutes, {id: 68}).roles
    },
    // Disable a session
    {
        path: _.findWhere(aclRoutes, {id: 72}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 72}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.sessions.toggle],
        access: _.findWhere(aclRoutes, {id: 72}).roles  
    },
    // Read a session
    {
        path: _.findWhere(aclRoutes, {id: 67}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 67}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.sessions.read],
        access: _.findWhere(aclRoutes, {id: 67}).roles
    },
    // Print a session
    {
        path: _.findWhere(aclRoutes, {id: 8}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 8}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.sessions.print],
        access: _.findWhere(aclRoutes, {id: 8}).roles
    },
    // Delete a session
    {
        path: _.findWhere(aclRoutes, {id: 65}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 65}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.sessions.delete],
        access: _.findWhere(aclRoutes, {id: 65}).roles
    },
    
    // Retrieve reminders
    {
        path: _.findWhere(aclRoutes, {id: 16}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 16}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.sessions.reminders],
        access: _.findWhere(aclRoutes, {id: 16}).roles
    },
    // Un/Follow a reminder
    {
        path: _.findWhere(aclRoutes, {id: 23}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 23}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.sessions.follow],
        access: _.findWhere(aclRoutes, {id: 23}).roles  
    },

    // === USERS ROUTES
		// ==========================================================
    // Users list
    {
        path: _.findWhere(aclRoutes, {id: 28}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 28}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.users.list],
        access: _.findWhere(aclRoutes, {id: 28}).roles
    },
    
    // Create a user
    {
        path: _.findWhere(aclRoutes, {id: 34}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 34}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.users.create],
        access: _.findWhere(aclRoutes, {id: 34}).roles
    },
    
    // Send password email to the user
    {
        path: _.findWhere(aclRoutes, {id: 73}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 73}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.users.sendPassword],
        access: _.findWhere(aclRoutes, {id: 73}).roles
    },
    
    // Update a user
    {
        path: _.findWhere(aclRoutes, {id: 31}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 31}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.users.update],
        access: _.findWhere(aclRoutes, {id: 31}).roles
    },
    
    // Delete a user
    {
        path: _.findWhere(aclRoutes, {id: 38}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 38}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.users.delete],
        access: _.findWhere(aclRoutes, {id: 38}).roles
    },
    
    // Read a participant
    {
        path: _.findWhere(aclRoutes, {id: 45}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 45}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.users.read],
        access: _.findWhere(aclRoutes, {id: 45}).roles
    },
    
    // Read an animator
    {
        path: _.findWhere(aclRoutes, {id: 19}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 19}).method,
        middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.users.read],
        access: _.findWhere(aclRoutes, {id: 19}).roles
    },

    // VIDEO ROUTES ========================================================
    // Route to handle all angular requests
    {
      path: _.findWhere(aclRoutes, {id: 77}).uri,
      httpMethod: _.findWhere(aclRoutes, {id: 77}).method,
      middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.video.getHomeVideo],
      access: _.findWhere(aclRoutes, {id: 77}).roles
    },
    {
      path: _.findWhere(aclRoutes, {id: 78}).uri,
      httpMethod: _.findWhere(aclRoutes, {id: 78}).method,
      middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.video.update],
      access: _.findWhere(aclRoutes, {id: 78}).roles
    },
    // FRONTEND ROUTES ========================================================
    // Route to handle all angular requests
    {
        path: _.findWhere(aclRoutes, {id: 11}).uri,
        httpMethod: _.findWhere(aclRoutes, {id: 11}).method,
        middleware: [controllers.angular]
    }
];


module.exports = function(app) {
    _.each(routes, function(route) {
        route.middleware.unshift(ensureAuthorized);
        var args = _.flatten([route.path, route.middleware]);
        switch(route.httpMethod.toUpperCase()) {
            case 'GET':
                app.get.apply(app, args);
                break;
            case 'POST':
                app.post.apply(app, args);
                break;
            case 'PUT':
                app.put.apply(app, args);
                break;
            case 'DELETE':
                app.delete.apply(app, args);
                break;
            default:
                throw new Error('Invalid HTTP method specified for route ' + route.path);
                break;
        }
    });
};

function ensureAuthorized(req, res, next) {
    if(_.contains([
        "*",
        "/api/account/signin",
        "/api/account/resetPassword",
        "/api/account/activation",
        "/api/account/lostPassword",
        "/api/conditions/read"
    ], req.route.path)){
        return next();
    } else {
        var token, completeDecodedToken;
        token = tokenManager.getToken(req.headers);
        if (token) completeDecodedToken = jsonwebtoken.decode(token, {complete:true});
        if (completeDecodedToken && typeof completeDecodedToken.payload.id !== 'undefined') {
            if (completeDecodedToken.header.alg === 'HS256') {
                var decodedToken = completeDecodedToken.payload;
                var userID = decodedToken.id;
                dbUser.userModel.findOne({_id: userID}, function (err, user) {
                    if(err){
                        console.log(err);
                        return res.sendStatus(500);
                    } else {
                        if (user !== null) {
                            var userRole = parseInt(user.role);
                            var route = _.findWhere(routes, {
                                path: req.route.path, 
                                httpMethod: req.method
                            });
                            var allowedRoles = route.access;
                            if (typeof(allowedRoles) !== "undefined") {
                                var accessGranted = false;
                                for(i=0; i<allowedRoles.length; i++){
                                    if (userRole === allowedRoles[i]) accessGranted = true;
                                }
                                
                                if (accessGranted) {
                                    return next();
                                } else {
                                    audit.logEvent(user.username, 'routes.js', 'Use the route : ' + req.route.path, '', '', 'failed',
                                                           'The user tried to access a route which is forbidden for his role');
                                    return res.sendStatus(403);
                                }
                            } else {// typeof allowedRoles is undefined
                                return next();
                            }
                        } else {
                            console.log('ensureAuthorized : user not found : ' + userID);
                            return res.sendStatus(401);
                        }
                    }
                });
            } else {
                console.log('ensureAuthorized received a suspicious token with customized algorithm (' + completeDecodedToken.header.alg + ')');
                return res.sendStatus(401);
            }
        } else {
            console.log('ensureAuthorized did not receive a valid token ("', token, '")');
            return res.sendStatus(401);
        }
    }
}


function firstLaunch(){
    var email = "admin@alzheimer.be";
    var firstname = "Firstname";
    var lastname = "Lastname";
    var username = "admin";
    var password = "pass";
    var role = '1';
    var activationToken = "0";

    dbUser.userModel.findOne({
        role: '1'
    }).exec(function (err, theUser) {
        if (err) {
            console.log(err);
            audit.logEvent('[mongodb]', 'Routes', 'FirstLaunch', "", "", 'failed', "Mongodb attempted to find the admin");
            return res.status(500).send(err);
        } else {
            if (theUser === null) {
                var user = new dbUser.userModel();
                user.email = email;
                user.firstname = firstname;
                user.lastname = lastname;
                user.username = username;
                user.password = password;
                user.role = role;
                user.activationToken = activationToken;
                user.save(function(err) {
                    if (err) {
                        console.log(err);
                        audit.logEvent('[mongodb]', 'Routes', 'FirstLaunch', "", "", 'failed', "Mongodb attempted to save the new admin");
                    }
                });
            }
        }
    });
    
    dbConditions.conditionsModel.findOne().exec(function (err, conditions) {
        if (err) {
            console.log(err);
            audit.logEvent('[mongodb]', 'Routes', 'FirstLaunch', "", "", 'failed', "Mongodb attempted to find conditions");
            return res.status(500).send(err);
        } else {
            if (conditions === null) {
                var conditions = new dbConditions.conditionsModel();
                conditions.content = "<p><span style=\"background-color: transparent;\"><b>Il"+
                " est important de lire ce document très attentivement.  en accédant au site de "+
                "l’alzheimer café virtuel et en utilisant ses fonctionnalités, vous reconnaissez"+
                " avoir lu, compris et être d’accord avec tous les termes de notre politique de "+
                "vie privée ainsi qu’avec toutes nos règles & conditions d’utilisation. si vous "+
                "n’êtes pas d’accord avec ces termes, veuillez quitter cette page et ne pas accéder "+
                "à notre site ni l’utiliser. </b></span></p><p><span style=\"background-color: "+
                "transparent;\"><b>Si vous souhaitez que votre compte (créé par votre animateur) "+
                "soit supprimé, veuillez contacter la ligue alzheimer asbl au : 04/229.58.10 ou "+
                "via <a href=\"mailto:ligue.alzheimer@alzheimer.be\" target=\"\">ligue.alzheimer@alzheimer.be"+
                "</a>.</b></span></p><p><span style=\"background-color: transparent;\"><i>Les conditions"+
                " d'utilisation et la déclaration en matière de respect de la vie privée applicables aux"+
                " fonctions de l’alzheimer café virtuel (ac virtuel) sont exposées ci-après :</i><br/>"+
                "</span></p><p><span style=\"background-color: transparent;\">1.    <u>Généralités</u>"+
                "<br/>1.1    En cliquant sur le bouton « Activer », vous déclarez :</span></p><p></p>"+
                "<ol><li><span style=\"background-color: transparent;\">avoir lu et compris les conditions"+
                " d'utilisation et la déclaration en matière de respect de la vie privée et en accepter"+
                " le contenu ; </span></li><li><span style=\"background-color: transparent;\">être"+
                " légalement capable de conclure un engagement ;</span></li><li><span style=\"background-color"+
                ": transparent;\">souhaiter activer votre compte « Alzheimer Café Virtuel » "+
                "(compte-ACV)</span></li></ol><p></p><p></p><p><span style=\"background-color: "+
                "transparent;\">Vous reconnaissez que le fait de cliquer sur ce bouton constitue"+
                " une preuve valable et suffisante de votre approbation des présentes conditions"+
                " d'utilisation et déclaration en matière de respect de la vie privée.<br/></span>"+
                "</p><p><span style=\"background-color: transparent;\">1.2</span><span "+
                "style=\"background-color: transparent;\">    </span><span style=\"background-color"+
                ": transparent;\">Le terme « Alzheimer Café Virtuel » ou « AC Virtuel » utilisé"+
                " dans les conditions d'utilisation et la déclaration en matière de respect de"+
                " la vie privée désigne un produit de la LIGUE ALZHEIMER ASBL ; la  rencontre"+
                " Alzheimer Café de la LIGUE ALZHEIMER ASBL est ainsi réalisée en ligne par le"+
                " biais de la vidéoconférence.</span></p><p>1.3<span style=\"background-color:"+
                " transparent;\">    </span><span style=\"background-color: transparent;\">Les"+
                " conditions d'utilisation et la déclaration en matière de respect de la vie"+
                " privée sont régies par le droit belge. Tout litige sera soumis à la compétence"+
                " des tribunaux belges.</span></p><p><span style=\"background-color: transparent;\">1.4"+
                "    Le fait que le tribunal invalide ou annule une ou plusieurs dispositions des présentes"+
                " conditions d'utilisation ou de la déclaration en matière de respect de la vie privée ne nuit"+
                " pas à la validité des autres dispositions.</span></p><p><span style=\"background-color:"+
                " transparent;\">2.</span><span style=\"background-color: transparent;\">    </span><span"+
                " style=\"background-color: transparent;\"><u>Règles et conditions d’utilisation</u></span></p>"+
                "<p><span style=\"background-color: transparent;\">2.1    Le <b>principe de l’AC Virtuel</b>"+
                " est de vous permettre de converser avec l’animateur et les autres participants de la séance"+
                " au moyen de l'image et du son. C’est la fonction de vidéoconférence.  Vous pouvez voir,"+
                " par un défilement aléatoire, l’animateur ainsi que les participants à la rencontre et"+
                " réciproquement, ceux-ci peuvent vous voir. <br/><br/><b>Si vous avez désactivé votre caméra"+
                "</b> dans les paramètres de votre fonction vidéoconférence, il est important de la réactiver."+
                "<br/></span></p><p><span style=\"background-color: transparent;\">2.2</span><span"+
                " style=\"background-color: transparent;\">    </span><span style=\"background-color:"+
                " transparent;\">La fonction de vidéoconférence porte sur l’application « AC Virtuel ». </span>"+
                "</p><p><span style=\"background-color: transparent;\">La fonction de vidéoconférence n’est"+
                " utilisable et utilisée que durant les séances d’AC Virtuel.</span></p><p><span style=\"background-color: "+
                "transparent;\">La LIGUE ALZHEIMER ASBL décline toute responsabilité en cas d'interruption ou d'arrêt des fonctions"+
                " de vidéoconférence pour des raisons techniques (incluant, sans s'y limiter, une panne d'Internet, une panne d'ordinateur, une panne du matériel de télécommunication ou d'un quelconque autre équipement).<br/><br/>2.3    La fonction de vidéoconférence n’est proposée que sur le <b>site Internet sécurisé de</b> ….. .</span></p><p><span style=\"background-color: transparent;\">Vérifiez, avant de l’utiliser, que vous vous trouvez bien sur le site Internet sécurisé …… En ce qui concerne le site"+
                " Internet, c'est très simple : l'adresse Internet dans la barre d'adresse en haut de votre navigateur"+
                " doit commencer par « http<b>s</b>://www.......... » ou par « http<b>s</b>://........be » (« s » signifiant « secure »). <br/><br/>2.4    Une fois que votre compte-Alzheimer Café Virtuel (compte-ACV) est activé, vous pouvez aller <b>compléter vos données de profil</b> via :………… Il vous sera également demandé de créer votre mot de passe.<br/>"+
                "De même, vous pourrez toujours procéder à des modifications de vos données de profil via : …………………….<br/><br/>2.5"+
                "    Il vous est <b>permis de refuser de communiquer des informations</b> comme votre numéro de téléphone et votre adresse postale. Par contre, pour le bon fonctionnement de l’AC Virtuel et votre accès à celui-ci nous donner votre nom, votre prénom, votre adresse e-mail et déterminer votre mot de passe est nécessaire.  <br/><br/>2.6    <b>Ne communiquez jamais votre numéro de carte bancaire ou carte SIM ou vos codes secrets</b> (aussi bien les codes utilisés pour les opérations bancaires"+
                " en ligne que le code PIN de votre carte bancaire ou d’autres codes et mots de passe divers) par vidéoconférence. Aucun animateur de l’AC virtuel ne vous les demandera jamais.<br/>Ne communiquez pas non plus vos coordonnées personnelles lors des séances d’AC Virtuel. Ces renseignements ne sont nécessaires que pour la création et la gestion de votre compte-ACV. Il n’est pas nécessaire"+
                " de fournir ces informations aux autres participants des séances.<br/><br/>2.7    Les collaborateurs de l’AC Virtuel ne peuvent exécuter aucune transaction financière (ouverture d'un compte bancaire, virement, placement, etc.).</span></p><p><span style=\"background-color: transparent;\">2.8    Vous portez l'entière responsabilité : </span></p><p></p><ol><li><span style=\"background-color: transparent;\">des questions, réponses et autres commentaires que vous adressez pendant la session "+
                "d’un AC Virtuel ;</span></li><li><span style=\"background-color: transparent;\">de la sécurité et du bon usage de l'appareil dont vous vous servez pour converser par vidéoconférence.</span></li></ol><p></p><p></p><p><span style=\"background-color: transparent;\">2.9    En tant que participant, vous vous engagez à ne pas communiquer <b>votre mot de passe d’accès à l’AC"+
                " Virtuel à d’autres personnes</b>. Ce site est sécurisé et doit le rester"+
                " pour le bien de tous. <br/>Veillez à<b> toujours vous déconnecter</b> de l’AC Virtuel une fois votre utilisation terminée.<br/><br/>2.10    La <b>connexion à l’AC Virtuel se fait avec un seul profil d’utilisateur</b>. Toute personne faisant usage de plusieurs profils se verra avertie et sanctionnée.<br/><br/>2.11"+
                "    La LIGUE ALZHEIMER ASBL ne peut être tenue pour responsable des conséquences dommageables et des préjudices pouvant résulter de l'utilisation de la fonction de vidéoconférence, pas plus que de l'utilisation que vous faites des données obtenues dans le contexte de l'utilisation de cette fonction.<br/><br/>2.12"+
                "    <b>Vous assumez l'entière responsabilité</b> vis-à-vis de l’AC Virtuel et de la"+
                " LIGUE ALZHEIMER ASBL si votre connexion au site …  est utilisée dans le but d'expédier des codes informatiques, des fichiers ou des programmes susceptibles d'interrompre, de limiter ou d'annihiler le fonctionnement des logiciels ou du matériel de la LIGUE ALZHEIMER ASBL  et de l’AC Virtuel ou encore, de son matériel de télécommunication.<br/><br/>2.13    La LIGUE ALZHEIMER ASBL  et l’équipe de l’AC Virtuel ne garantissent pas le fonctionnement continu ou exempt d'erreurs, de virus ou "+
                "d'autres éléments dommageables, de la fonction de vidéoconférence.<br/><br/>2.14    C’est la LIGUE"+
                " ALZHEIMER ASBL qui, via ses animateurs, <b>crée le compte « AC Virtuel » d’un participant</b>. Celui-ci va en être informé par e-mail et sera invité à activer son compte après avoir lu le présent document. <br/><br/>2.15    Tout <b>utilisateur souhaitant</b> que son <b>compte</b> soit <b>supprimé</b> doit en faire la demande auprès de la LIGUE ALZHEIMER ASBL via :………… Sans directive de votre part, vos données restent enregistrées dans la base de données de"+
                " l’AC Virtuel.</span></p><p><span style=\"background-color: transparent;\">2.16    La LIGUE ALZHEIMER ASBL, par le biais de l’équipe de l’AC Virtuel et de ses animateurs a le droit de <b>mettre immédiatement fin à l'échange voire de supprimer votre compte</b> :</span></p><p></p><ol><li><span style=\"background-color: transparent;\">si vos dires ou vos écrits peuvent être considérés comme inconvenants, trompeurs, diffamatoires, harcelants, obscènes ou injurieux d'une quelconque manière ;"+
                " </span></li><li><span style=\"background-color: transparent;\">pour des raisons de sécurité ;</span></li><li><span style=\"background-color: transparent;\">si vous utilisez l’application à des fins commerciales, de recrutement, de publicité ; </span></li><li><span style=\"background-color: transparent;\">pour le non-respect des règles et conditions énoncées dans le présent document"+
                " ainsi que pour toute autre nuisance au bon fonctionnement de l’AC Virtuel. </span></li></ol><p><span style=\"background-color: transparent;\">2.17    Dans l’AC Virtuel le discours politique, commercial, religieux, offensant ou diffamatoire n’a pas sa place. Toute personne s’inscrivant dans une attitude de jugement, de discrimination, de recrutement, de vente ou d’endoctrinement <b>sera exclue de l’AC Virtuel</b>.<br/><br/>2.18    Pour utiliser ce site, vous devez être âgé de <b>18 ans"+
                " au minimum</b>. <br/><br/>2.19    Pour des raisons pratiques, <b>une séance d’AC Virtuel</b> peut accueillir un nombre limité de participants à savoir un <b>maximum de 15 personnes</b>. <br/><br/>2.20    Il n’y a <b>pas de réservation préalable</b> possible pour l’accès à une séance. Par contre, si vous êtes intéressé par l’une d’entre elles, vous pouvez cliquer sur le bouton « suivre"+
                " » et demander à recevoir des rappels pour celle-ci. <br/>Le"+
                " jour de la séance, à l’heure dite, connectez-vous à l’AC Virtuel, sélectionnez la séance et cliquez sur le bouton « joindre ». L’accès étant limité à un nombre maximum de personnes, le principe est celui du « premier arrivé, premier servi ». Les 15 premières personnes connectées ayant cliqué sur « Joindre »"+
                " pourront accéder à la séance.<br/><br/>2.21    L’AC Virtuel offre un <b>large choix de séances</b>. Si vous n’avez pas pu accéder à l’une d’entre elles, vous êtes invités à en rejoindre une autre de votre choix.<br/><br/>2.22    Une <b>séance d’AC Virtuel</b> dure <b>2 heures</b>. C’est l’animateur qui en détermine"+
                " le titre, la date et l’horaire.<br/><br/>2.23    C’est l’animateur de l’AC qui ouvre"+
                " <b>l’accès à une de ses séances </b>et qui la <b>clôture</b> également.<br/><br/>2.24"+
                "    C’est l’animateur de l’AC qui régule le <b>temps de parole</b> et la"+
                " distribution de celle-ci. Il est important de ne pas interrompre une personne en train de parler. Attendez votre tour et annoncez-vous lorsque vous prenez la parole (en énonçant votre prénom) afin que chacun puisse identifier qui a la parole. <br/><br/>2.25    L’<b>animateur de l’AC Virtuel</b> est là pour :</span></p><p></p><p></p><ul><li><span"+
                " style=\"background-color: transparent;\">Faciliter les échanges ;</span></li><li><span style=\"background-color: transparent;\">Favoriser l’expression ;</span></li><li><span style=\"background-color: transparent;\">Être à l’écoute ;</span></li><li><span style=\"background-color: transparent;\">Assurer la modération et le respect des règles ;</span></li><li><span style=\"background-color: transparent;\">Apporter les informations et renseignements qu’il a à sa disposition si cela"+
                " s’avère nécessaire ;</span></li><li><span style=\"background-color: transparent;\">… </span></li></ul><p>"+"<span style=\"background-color: transparent;\">3.    <u style=\"color: rgb(85, 85, 85);background-color: rgb(255, 255, 255);\">Déclaration en matière de respect de la vie privée</u></span></p><p><span style=\"background-color: transparent;\">Cette politique de vie privée décrit les"+
                " pratiques de respect de la vie privée de l’application « Alzheimer"+
                " Café Virtuel ».  Nous vous invitons donc à lire le texte suivant afin de prendre connaissance de ce que nous"+
                " collectons comme données à propos de vous (« utilisateur ») et ce que nous faisons de ces données. Si vous avez des questions supplémentaires au sujet de notre politique de vie privée, contactez-nous par e-mail (<a href=\"mailto:ligue.alzheimer@alzheimer.be\" target=\"\">ligue.alzheimer@alzheimer.be</a>).<br/><br/>3.1    La LIGUE ALZHEIMER ASBL attache beaucoup d’importance à la protection de la vie privée. Elle s’efforce de traiter les données à caractère personnel obtenues par"+
                " l’inscription et la vidéoconférence en toute légalité, honnêteté et transparence. <br/><br/>De ce fait, nous suivons les <b>règles générales étant en vigueur en Belgique concernant la protection de la vie privée</b> car celles-ci sont également applicables à la fonction de vidéoconférence. <br/>Pour une vue détaillée de cette règlementation vous pouvez consulter le lien suivant issu du site"+
                " internet de la <b>Commission de la Protection de la Vie Privée (CPVP)</b>: <a href=\"https://www.privacycommission.be/sites/privacycommission/files/documents/CONS_loi_vie_privee_08_12_1992.pdf\" target=\"\">https://www.privacycommission.be/sites/privacycommission/files/documents/CONS_loi_vie_privee_08_12_1992.pdf</a><br/><br/>En publiant la présente Déclaration particulière en matière de respect de la vie privée pour la vidéoconférence, la LIGUE ALZHEIMER ASBL entend vous informer"+
                " précisément sur le traitement de vos données personnelles dans le cadre spécifique de l'utilisation de l’AC Virtuel.<br/><br/>3.2    La LIGUE ALZHEIMER ASBL (Montagne sainte-Walburge 4b, B-4000 Liège) est responsable du traitement des données personnelles obtenues par le biais des fonctions d’inscription et de vidéoconférence.<br/><br/>3.3    La LIGUE ALZHEIMER ASBL s’engage à ne pas diffuser,"+
                " partager, louer ou vendre vos données à des tiers. <br/>"+
                "<br/>3.4    Pour pouvoir soutenir la fonction de vidéoconférence,  l’AC Virtuel doit pouvoir <b>accéder à un certain nombre de données techniques</b>, parmi lesquelles le type et la version de votre appareil, son système d'exploitation et son navigateur. Pour vous authentifier, la LIGUE ALZHEIMER ASBL traite"+" une série de <b>données d'authentification</b>, parmi lesquelles votre adresse e-mail et votre mot de passe. Ces données sont cryptées et non accessibles à des tiers. Concernant les"+
                " <b>autres données constituant votre profil</b> (numéro de téléphone, adresse postale, …), celles-ci ne sont consultables que par"+
                " vous-même et par l’équipe de l’Alzheimer Café Virtuel (c’est-à-dire la LIGUE ALZHEIMER"+
                " ASBL et les animateurs des séances). Les participants aux séances n’ont pas accès aux"+
                " profils les uns des autres. <br/><br/>3.5    Pour le traitement des données,"+
                " la LIGUE ALZHEIMER ASBL ne fait pas appel à des sous-traitants au sein du groupe ni à des tiers. <b>Les données sont hébergées de manière cryptées sur les serveurs de l’Université Catholique de Louvain</b>. Ces données ne sont pas accessibles, utilisables ni exploitables par l’Université Catholique de Louvain, par leurs sous-traitants ni par des"+
                " tiers.  <br/><br/>3.6    Le traitement des données à caractère personnel fait partie de la fonction de vidéoconférence. Vos mouvements de navigation sur le site sont traités par la LIGUE ALZHEIMER ASBL.<br/><br/>3.7    La LIGUE ALZHEIMER ASBL traite vos données à caractère personnel dans le but de vous fournir assistance et information sur ses services lorsque vous utilisez l’application de l’AC Virtuel en ligne mais également pour vous envoyer des <b>informations sur ses autres activités"+
                "</b> (conférences, colloque, Alzheimer Cafés, …). <br/><b>Si vous ne souhaitez plus recevoir les"+
                " informations concernant nos activités</b>, veuillez prendre contact avec la LIGUE ALZHEIMER ASBL via le 04/229.58.10 ou <a href=\"mailto:ligue.alzheimer@alzheimer.be\" target=\"\">ligue.alzheimer@alzheimer.be</a><br/><br/>Les données à caractère personnel peuvent également être traitées par la LIGUE ALZHEIMER ASBL à des fins d’administration de la preuve.<br/><br/>3.8    La LIGUE ALZHEIMER ASBL se réserve le droit <b>d'enregistrer et de conserver les"+
                " données suivantes</b> :</span></p><p>"+
                "</p><ol><li><span style=\"background-color: transparent;\">l'utilisation des fonctions de vidéoconférence et votre accord à propos des conditions d'utilisation et de la déclaration en matière de respect de la vie privée ;</span></li><li><span style=\"background-color: transparent;\">les échanges logistiques entre le collaborateur de l’AC Virtuel (la LIGUE ALZHEIMER ASBL et l’équipe de l’AC Virtuel) et vous ayant lieu en-dehors des séances-mêmes"+
                " ; </span></li></ol><p><span style=\"background-color: transparent;\">Les <b>échanges oraux  et les images"+
                " vidéo</b> ne sont, quant à eux, <b>ni enregistrés ni conservés</b>, sauf cas exceptionnel. <b>En cas d’enregistrement des images vidéo et des échanges oraux</b>, votre animateur vous en avertira en début de séance. Si la LIGUE ALZHEIMER ASBL souhaite exploiter ces enregistrements, elle"+" sera tenue de vous demander votre consentement écrit.<br/><br/>3.9    Le site de l’AC Virtuel utilise une fonction de Web Storage pour ce qui concerne l’enregistrement de votre langue d’utilisation (retient"+
                " que vous parlez français par exemple) ainsi que pour la clé permettant le cryptage de votre mot de passe.<br/><br/>3.10    Malgré les multiples mesures de sécurité que nous mettons en place pour protéger vos données, la LIGUE ALZHEIMER ASBL ne peut garantir une protection à 100%. En effet, de manière générale, aucune transmission d’information via internet ou réseau sans fil ne peut être sécurisée à 100%. Dans cette matière, le risque zéro"+
                " n’existe pas. Par ce fait, la LIGUE ALZHEIMER ASBL ne peut être tenue responsable des actions de quelque tiers que ce soit qui recevrait ces informations. <br/><br/>3.11    La LIGUE ALZHEIMER ASBL se réserve le droit de <b>modifier la présente déclaration</b> à tout moment, veuillez donc"+
                " consulter régulièrement ce document. Néanmoins, si des modifications substantielles sont apportées, vous en serez averti par une notification sur la page d’accueil de l’AC Virtuel lorsque vous vous connecterez. Les modifications seront ajoutées clairement dans le présent document.<br/></span></p><p><span"+
                " style=\"background-color: transparent;\">4.    <u style=\"color: rgb(85, 85, 85);background-color: rgb(255, 255, 255);\">Données de contact</u><br/></span></p><p><span style=\"background-color: transparent;\"><span style=\"color: rgb(85, 85, 85);background-color: rgb(255, 255, 255);\">LIGUE ALZHEIMER ASBL<br/>Montagne"+
                " Sainte-Walburge, 4b<br/>4000 Liège<br/>Téléphone : +32 4 229 58 10<br/>E-mail :"+
                " <a href=\"mailto:ligue.alzheimer@alzheimer.be\" target=\"\">ligue.alzheimer@alzheimer.be</a>"+
                "<br/>Site internet : <a href=\"http://www.alzheimer.be\""+
                " target=\"\">www.alzheimer.be</a><br/></span></span></p><p></p><p></p>";
                
                conditions.save(function(err) {
                    if (err) {
                        console.log(err);
                        audit.logEvent('[mongodb]', 'Routes', 'FirstLaunch', "", "", 'failed', "Mongodb attempted to save the new conditions");
                    }
                });
            }
        }
    });
}


function sendReminders(){
    controllers.sessions.cron();
    var CronJob = require('cron').CronJob;
    new CronJob('0 */30 * * * *', function() {
    	controllers.sessions.cron();
    }, null, true);
}