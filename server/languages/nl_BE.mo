��    \      �     �      �     �     �  	   �     �     �     �  5         6  	   =     G  
   P     [     b     i     r  )   y     �     �     �     �     �     �     �     �     �     �     	     	     )	     2	     9	     @	     E	     L	     Y	     o	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  
   �	     �	  	   �	     �	     �	     
     
     #
     +
     4
     =
  !   C
     e
     m
     
     �
     �
     �
     �
     �
     �
  	   �
     �
      �
            G        d     |     �     �     �     �     �     �     �     �     �  	   �     �     �  	   �  '   �              -  	   /     9     E     K     S  /   `     �     �  
   �  	   �  	   �     �     �     �  &   �                         "     .     D     K     T     [     h  -   �     �  
   �     �     �     �  	   �     �     �                       	   %     /     5     9  	   I     S     [     `     l     r     �     �     �     �     �  
   �     �     �     �               +     4     ;     W  
   n     y     �  	   �     �  &   �     �     �  H   �     2  	   K     U     [     `     h     p     u     |     �     �     �     �     �     �  &   �     �                 0   1          E      X   I   =      T   Y   ;   6      &   V                  [          Z      8      $                             5      *       K   :   P   N      ,       F      @           C   L   O   W       (   H          2          '              >           !   #   S       D   M   	      3         <   G   )       %      4   9      Q   A   /                                           U   -   "   .   
       R   7      \      B   +           J      ?    ? Add Allergies April Archive Archived Are you sure you want to delete the conversation with August Birthdate Birthday Blood type Cancel Change Converse Create Create you <strong>Eglé</strong> Account Current Date Day December Delete Delete this conversation Disease Edit Email Emergency contact Event details Failed to add the entry! February Female Friday From Gender Home address Internal Server Error Invalid birthdate January July June Language Male March May Medical record Messages Monday Name Need help? New New event No conversation yet? No results for Notifications November October Password Personal Phone Please choose a valid date range. Profile Re-enter Password Saturday Save Search for contacts Search for patients See All Send Send message... September Settings Sorry, this page isn't available Stay signed in Sunday The link you followed may be broken, or the page may have been removed. The minimum value is 1. Thursday Title To Today Tuesday Type Update Username Value Walking Wednesday Week Weight Yesterday Your password was changed with success. unknown Project-Id-Version: healthcompass_nl
POT-Creation-Date: 
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
Language: nl_BE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.7.1
Plural-Forms: nplurals=2; plural=(n != 1);
 ? Toevoegen Allergieën April Archief Gearchiveerd Ben je zeker het gesprek willen verwijderen met Augustus Geboortedatum Verjaardag Bloedtype Annuleren Wijzigen Babbelen Creëren Maak uw <strong>Eglé</strong>-account Stroom Datum Dag December Verwijderen Verwijder dit gesprek Ziekte Uitgeven E-mail Contact nood Details van het evenement Niet in geslaagd de vermelding toe te voegen! Februari Vrouwelijk Vrijdag Van Geslacht Huisadres Interne serverfout Ongeldige geboortedatum Januari Juli Juni Taal Mannelijk Maart Mei Medisch dossier Berichten Maandag Naam Hulp nodig? Nieuw Nieuw evenement Nog geen gesprek? Geen resultaten voor Bekendmakingen November Oktober Wachtwoord Persoonlijk Telefoon Kies een geldige datum range. Profiel Wachtwoord opnieuw invoeren Zaterdag Sparen Zoeken naar contactpersonen Zoeken voor patiënten Alles Zien Sturen Verstuur bericht ... September Instellingen Sorry, deze pagina is niet beschikbaar Blijf ingelogd Zondag De gevolgde link kan worden verbroken, of de pagina kan zijn verwijderd. De minimale waarde is 1. Donderdag titel Naar Vandaag Dinsdag Type Update Gebruikersnaam Waarde Wandelen Woensdag Week Gewicht Gisteren Je wachtwoord is gewijzigd met succes. onbekend 