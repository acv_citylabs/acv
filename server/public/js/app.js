angular.module('app', [
    'ui.router',
    'routes',
    'oc.lazyLoad',
    'angular-jwt',
    'TokenInterceptorService',
    'dateFormatFilters',
    'mappingFilters',
    'limitFilters',
    'gettext',
    'AutofocusDirective',
    'pickadate',
    'angularModalService',
    'ngFileUpload',
    'textAngular',
    'vcRecaptcha',
    'ng.deviceDetector',
]);

var underscore = angular.module('underscore', []);
    underscore.factory('_', function() {
    return window._;
});