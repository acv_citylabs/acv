angular.module('dateFormatFilters', [])
.filter('dayFormat', function(gettextCatalog) {
    var weekDays = new Array(gettextCatalog.getString("Sunday"), 
                            gettextCatalog.getString("Monday"), 
                            gettextCatalog.getString("Tuesday"), 
                            gettextCatalog.getString("Wednesday"), 
                            gettextCatalog.getString("Thursday"), 
                            gettextCatalog.getString("Friday"), 
                            gettextCatalog.getString("Saturday"));
    return function (date) {
        return weekDays[stringToDate(date).getDay()];
    }
})
.filter('fullMonthDateFormat', function(gettextCatalog, $window) {
    var weekDays = new Array(gettextCatalog.getString("Sunday"), 
                            gettextCatalog.getString("Monday"), 
                            gettextCatalog.getString("Tuesday"), 
                            gettextCatalog.getString("Wednesday"), 
                            gettextCatalog.getString("Thursday"), 
                            gettextCatalog.getString("Friday"), 
                            gettextCatalog.getString("Saturday"));
    var months = new Array(gettextCatalog.getString("January"),
                          gettextCatalog.getString("February"),
                          gettextCatalog.getString("March"), 
                          gettextCatalog.getString("April"), 
                          gettextCatalog.getString("May"), 
                          gettextCatalog.getString("June"), 
                          gettextCatalog.getString("July"), 
                          gettextCatalog.getString("August"), 
                          gettextCatalog.getString("September"), 
                          gettextCatalog.getString("October"), 
                          gettextCatalog.getString("November"), 
                          gettextCatalog.getString("December"));
    return function (date) {
        var value = '';
        if($window.localStorage.language == 'FR'){
            value =  weekDays[stringToDate(date).getDay()] + ' ' + stringToDate(date).getDate() + ' ' + months[stringToDate(date).getMonth()] + ' ' + stringToDate(date).getFullYear();
        } else {
            value =  months[stringToDate(date).getMonth()] + ' ' + stringToDate(date).getDate() + ' ' + stringToDate(date).getFullYear();

        }
        return value;
        
    }
})
.filter('dateFormat', function() {
    return function (date) {
        console.log(typeof date);
        var d = addZero(stringToDate(date).getDate());
        var m = addZero(stringToDate(date).getMonth()+1);
        var y = addZero(stringToDate(date).getFullYear());
        return d + '/' + m + '/' + y;
    }
})
.filter('timeFormat', function() {
    return function (date) {
        return timeFormat(date);
    }
})
.filter('endTimeFormat', function() {
    return function (date) {
        return timeFormat(date, true);
    }
})
.filter('smartDatetime', function(gettextCatalog){
    return function (datetime) {
        var myDate = stringToDate(datetime);
        var now = new Date;
        if(myDate.getDate() == now.getDate() && myDate.getMonth() == now.getMonth() && myDate.getFullYear() == now.getFullYear()){
            return timeFormat(myDate);
        }
        else if(myDate.getDate()+1 == now.getDate() && myDate.getMonth() == now.getMonth() && myDate.getFullYear() == now.getFullYear()){
            return gettextCatalog.getString("Yesterday");
        }
        else if(now.getDate()-7 < myDate.getDate() && myDate.getMonth() == now.getMonth() && myDate.getFullYear() == now.getFullYear()){
            var weekDays = new Array(gettextCatalog.getString("Sunday"), 
                            gettextCatalog.getString("Monday"), 
                            gettextCatalog.getString("Tuesday"), 
                            gettextCatalog.getString("Wednesday"), 
                            gettextCatalog.getString("Thursday"), 
                            gettextCatalog.getString("Friday"), 
                            gettextCatalog.getString("Saturday"));
            return weekDays[myDate.getDay()];
        }
        else{
            var months = new Array(gettextCatalog.getString("January"),
                          gettextCatalog.getString("February"),
                          gettextCatalog.getString("March"), 
                          gettextCatalog.getString("April"), 
                          gettextCatalog.getString("May"), 
                          gettextCatalog.getString("June"), 
                          gettextCatalog.getString("July"), 
                          gettextCatalog.getString("August"), 
                          gettextCatalog.getString("September"), 
                          gettextCatalog.getString("October"), 
                          gettextCatalog.getString("November"), 
                          gettextCatalog.getString("December"));
            return months[myDate.getMonth()] + ' ' + myDate.getDate();
        }
    }
});

angular.module('limitFilters', [])
.filter('limitNameXS', function() {
    return function (value) {
        var count = "";
        if(value.length > 19){
            count = value.substr(value.indexOf("("), 3);
            value = value.substr(0, 16) + "...";
        }
        value = value + count;
        return value;
    }
})

angular.module('mappingFilters', [])
.filter('whoFormat', function(gettextCatalog) {
    return function (who) {
        var whoList = new Array(gettextCatalog.getString("Patient"), 
                        gettextCatalog.getString("Family member"), 
                        gettextCatalog.getString("Friend"), 
                        gettextCatalog.getString("Visitor/Interested"), 
                        gettextCatalog.getString("Student"), 
                        gettextCatalog.getString("Professional"), 
                        gettextCatalog.getString("Volunteer"));
        return whoList[who];
    }
})
.filter('languageFormat', function(gettextCatalog) {
    return function (who) {
        var whoList = new Array(gettextCatalog.getString("Patient"), 
                        gettextCatalog.getString("Family member"), 
                        gettextCatalog.getString("Friend"), 
                        gettextCatalog.getString("Visitor/Interested"), 
                        gettextCatalog.getString("Student"), 
                        gettextCatalog.getString("Professional"), 
                        gettextCatalog.getString("Volunteer"));
        return whoList[who];
    }
})
.filter('languageFormat', function(gettextCatalog) {
    return function (language) {
        var toReturn = "";
        if(language == 'FR')
            toReturn = gettextCatalog.getString("French");
        else if(language == 'EN')
            toReturn = gettextCatalog.getString("English");
        else if(language == 'NL')
            toReturn = gettextCatalog.getString("Dutch");
        else
            toReturn = gettextCatalog.getString("unknown");
        return toReturn;
    }
})
.filter('unknownFormat', function(gettextCatalog) {
    return function (value) {
        var toReturn = value;
        if(toReturn === undefined || toReturn == " " || toReturn == "")
            toReturn = gettextCatalog.getString("unknown");
        return toReturn;
    }
})
.filter('phoneFormat', function(gettextCatalog) {
    return function (phone) {
        var toReturn = "";
        if(phone !== undefined && phone.substr(0,2) == "00" && phone.length > 8)
            toReturn = phone.substr(0,4) + " " + phone.substr(4,1) + " " + phone.substr(5, phone.length);
        else if(phone !== undefined && phone.length == 9)
            toReturn = phone.substr(0,3) + " " + phone.substr(3,2) + " " + phone.substr(5,2) + " " + phone.substr(7,2);
        else if(phone !== undefined && phone.length == 10)
            toReturn = phone.substr(0,4) + " " + phone.substr(4,2) + " " + phone.substr(6,2) + " " + phone.substr(8,2);
        else
            toReturn = gettextCatalog.getString("unknown");
        return toReturn;
    }
})
.filter('html', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
});

function timeFormat(date, end){
    if(typeof date == 'string'){
        date = stringToDate(date);
    }
    //UTC
    date = new Date(date.getTime() - date.getTimezoneOffset() * 60000);
    if(end){
        date.setHours(date.getHours()+2);
    }
    var h = addZero(date.getHours());
    var m = addZero(date.getMinutes());
    return h + "H" + m;
}

function stringToDate(date){
    var a = date.split(/[^0-9]/);
    return new Date (a[0],a[1]-1,a[2],a[3],a[4],a[5] );
}

function addZero(str){
    str = str.toString();
    if(str.length == 1){
        str = "0" + str;
    }
    return str;
}

function dateToMilli(strDate){
    var date = new Date(strDate);
    return date.getTime();
}