angular.module('AuditCtrl', []).controller('AuditController', function($scope, gettextCatalog, $state, $ocLazyLoad, $injector, $rootScope) {
    $ocLazyLoad.load([
        'bower_components/pickadate/lib/themes/classic.css',
        'bower_components/pickadate/lib/themes/classic.date.css',
        'css/templates/audit.css'
    ]);
    
    $scope.dateRange = {
        from: new Date(new Date().setDate(new Date().getDate() - 1)),
        to: new Date()
    }

    $scope.fetchResults = function (){
        if(($scope.dateRange.from && $scope.dateRange.to) && ($scope.dateRange.from <= $scope.dateRange.to)){
            $ocLazyLoad.load('js/services/AuditService.js').then(function() {
                var Audit = $injector.get('Audit');
                Audit.listByDate({
                    from: $scope.dateRange.from,
                    to: $scope.dateRange.to
                }).success(function(data) {
                    $scope.logs = data;
                }).error(function(status, data){
                    console.log(data);
                    console.log(status);
                });
            });
        } else {
            $rootScope.rootAlerts.push({
                type:'warning',
                msg: gettextCatalog.getString('Please choose a valid date range.'),
                priority: 3
            });
            $scope.logs = [];
        }
    }
    
    $scope.fetchResults();
});