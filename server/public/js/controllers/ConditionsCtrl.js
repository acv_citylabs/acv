angular.module('ConditionsCtrl', []).controller('ConditionsController', function($scope, gettextCatalog, $state, $ocLazyLoad, $injector, $rootScope, taOptions) {
    $ocLazyLoad.load('js/services/ConditionsService.js').then(function() {
        var Conditions = $injector.get('Conditions');

          $scope.toolbar = [
              ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'pre'],
              ['bold', 'italics', 'underline', 'strikeThrough', 'ul', 'ol', 'redo', 'undo', 'clear'],
              ['justifyLeft', 'justifyCenter', 'justifyRight', 'indent', 'outdent'],
              ['html', 'insertImage','insertLink', 'insertVideo', 'wordcount', 'charcount']
          ];
        
        Conditions.read({
        }).success(function(data){
            $scope.content = data.content;
        }).error(function(status, data) {
            $rootScope.rootAlerts.push({
                type:'danger',
                msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                priority: 2
            });
        });
        
        $scope.update = function(){
            Conditions.update({
                content: $scope.content
            }).success(function(data){
                $rootScope.rootAlerts.push({
                     type:'success',
                     msg: gettextCatalog.getString('The conditions were updated.'),
                     priority: 4
                });
            }).error(function(status, data) {
                $rootScope.rootAlerts.push({
                    type:'danger',
                    msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                    priority: 2
                });
            });
        }
    });
});