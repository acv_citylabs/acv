angular.module('HeaderCtrl', [[
    'css/templates/sidebar.css',
]]).controller('HeaderController', function($scope, $state, $window, $timeout, gettextCatalog, $ocLazyLoad, $injector, $rootScope, ModalService) {
    $scope.alert = {};
    $scope.night = (new Date).getHours() > 18  ? true : false;
    $scope.currentState = $state.current.name;
    $scope.dropdown = false;
    
    $rootScope.$on("ERR_CONNECTION_REFUSED", function () {
        if (Object.keys($scope.alert).length == 0 && $rootScope.ERR_CONNECTION_REFUSED) {
            var msg = "";
            if (navigator.onLine) {
                msg = gettextCatalog.getString('Eglé Server Unreachable.');
            } else {
                msg = gettextCatalog.getString('No Internet Connection.');
            }
            $scope.alert = {
                msg: msg, 
                type: 'black',
                priority: 1
            };
        } else if(Object.keys($scope.alert).length > 0 && !$rootScope.ERR_CONNECTION_REFUSED){
            $scope.alert = {};
        }
    });
    
    //--<about>
    function rule(number){
        $('.modal-backdrop').remove();
        ModalService.showModal({
            templateUrl: "templates/modals/about/"+number+".html",
            controller: function($scope, close){
                $scope.close = function(result) {
                    close(result, 500); // close, but give 500ms for bootstrap to animate
                };
            }
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                if(result){
                    if(number < 7){
                        rule(number+1);
                    }
                } else {
                    rule(number-1);
                }
            });
        });
    }
    
    function about(){
        rule(1);
    }
    
    
    $scope.about = function(){
        about();
    }
    
    function autoHide(sec){
        $timeout(function() {
            $scope.alert = {};
        }, sec);
    };
    
    $rootScope.$watch('rootAlerts', function(newValue, oldValue) {
        if(newValue.length > 0){
            $scope.alert = {
                msg: newValue[newValue.length - 1].msg,
                type: newValue[newValue.length  - 1].type
            };
            if(newValue[newValue.length - 1].priority > 3){
                autoHide(3000);
            } else if(newValue[newValue.length - 1].priority === 3){
                autoHide(5000);
            }
        }
    }, true);
    
    $ocLazyLoad.load('js/services/AccountService.js').then(function() {
        var Account = $injector.get('Account');
        
        $scope.signout = function signout() {
            if ($window.localStorage.token) {
                Account.signout().success(function(data) {
                    delete $window.localStorage.language;
                    delete $window.localStorage.token;
                    delete $window.localStorage.roomToken;
                    $state.go("signin", {}, {reload: true});
                }).error(function(status, data) {
                    console.log(status);
                    console.log(data);
                });
            }
        };

        $ocLazyLoad.load('js/services/UIService.js').then(function() {
            var UI = $injector.get('UI');
            UI.nav().success(function(data) {
                $scope.navLeft = [];
                $scope.navLeftBonus = [];
                for(var i=0; i< data.nav.left.length; i++){
                     data.nav.left[i].title = gettextCatalog.getString(data.nav.left[i].title);
                    if(data.nav.left[i].href == ''){
                        $scope.navLeftBonus.push(data.nav.left[i]);
                    } else {
                        $scope.navLeft.push(data.nav.left[i]);
                    }
                }
                $scope.navRight = data.nav.right;
            }).error(function(status, data) {
                console.log(status);
                console.log(data);
            });
        });
    });
});