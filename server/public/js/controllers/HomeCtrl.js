
angular.module('HomeCtrl', []).controller('HomeController', function($sce, $scope, gettextCatalog, $ocLazyLoad, $injector, $rootScope, ModalService) {    
    //Build home
    function home(){
        $ocLazyLoad.load('js/services/UIService.js').then(function() {
            var UI = $injector.get('UI');
            UI.home().success(function(data) {
                $scope.homes = data.homes;
            }).error(function(status, data) {
                console.log(status);
                console.log(data);
            });
        });
        
        $ocLazyLoad.load('js/services/VideoService.js').then(function() {
          var Video = $injector.get('Video');
          Video.read().success(function(data){
          		/*var vid = data;
          		if(data){
          			vid = data;
          		}else{*/
          			var vid = {
          					name : "homeVideo",
          					link : "JJ_w_C0mHNY" // "olFEpeMwgHk"
          			}
          		//}
          		$scope.vidLink = $sce.trustAsResourceUrl('https://www.youtube.com/embed/' + vid.link + '?rel=0&amp;controls=0&amp;showinfo=0');

          }).error(function(status, data) {
              $rootScope.rootAlerts.push({
                  type:'danger',
                  msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                  priority: 2
              });
          });
        });
        
    };
    
    //--<about>
    function rule(number){
        $('.modal-backdrop').remove();
        ModalService.showModal({
            templateUrl: "templates/modals/about/"+number+".html",
            controller: function($scope, close){
                $scope.close = function(result) {
                    close(result, 500); // close, but give 500ms for bootstrap to animate
                };
            }
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                if(result){
                    if(number < 7){
                        rule(number+1);
                    } else {
                        gotit();
                    }
                } else {
                    rule(number-1);
                }
            });
        });
    }
    
    //Verify if we have to display about
    function about(){
        $ocLazyLoad.load('js/services/UIService.js').then(function() {
            var UI = $injector.get('UI');
            UI.app({name: 'about'}).success(function(data) {
                if(data.display){
                    rule(1);
                }
            });
        });
    }
    
    //Got it
    function gotit(){
        $ocLazyLoad.load('js/services/UIService.js').then(function() {
            var UI = $injector.get('UI');
            UI.toggle({name:'about'}).success(function(data) {
            });
        });
    }
    //--</about>

    $scope.buildHome = function(){
        home();
        about();
    }
    $scope.buildHome();
});