angular.module('ProfileCtrl', [[
    'bower_components/pickadate/lib/themes/classic.css',
    'bower_components/pickadate/lib/themes/classic.date.css',
    'bower_components/pickadate/lib/themes/classic.time.css'
]]).controller('ProfileController', function ($scope, gettextCatalog, $location, $timeout, $state, $ocLazyLoad, $injector, $filter) {
    $ocLazyLoad.load('js/services/AccountService.js').then(function () {
        var Account = $injector.get('Account');
        
        $scope.numeric = /^\d+$/;

        $scope.whosList = [
            {name:gettextCatalog.getString("Patient"), value:'0'},
            {name:gettextCatalog.getString("Family member"), value:'1'},
            {name:gettextCatalog.getString("Friend"), value:'2'},
            {name:gettextCatalog.getString("Visitor/Interested"), value:'3'},
            {name:gettextCatalog.getString("Student"), value:'4'},
            {name:gettextCatalog.getString("Professional"), value:'5'},
            {name:gettextCatalog.getString("Volunteer"), value:'6'}
        ];

        $scope.colorsList = ['#bebebe','#E91E63','#9C27B0','#00BCD4','#CDDC39','#FFC107'];
        
        //Get user profile
        $ocLazyLoad.load('js/utils/date.js').then(function () {
            Account.read().success(function (data) {
                if ($location.path().indexOf('avatar') > -1) {
                    if (data.preferences && data.preferences.avatar) {
                        $scope.color = data.preferences.avatar;
                    } else {
                        $scope.color = $scope.colorsList[0];
                    }
                }
                $scope.profile = data;
            }).error(function (status, data) {
                console.log(data);
                console.log(status);
            });
        });
        
        $scope.selectColor = function (color) {
            $scope.color = color;
        };
        
        $scope.updateAvatar = function () {
            Account.update({
                avatar: $scope.color
            }).success(function (data) {
                $state.go("home.profile.main", {}, {reload: true});
            }).error(function (status, data) {
                console.log(data);
                console.log(status);
            });
        };

        $scope.updateBasic = function () {
            Account.update({
                firstname: $scope.profile.firstname,
                lastname: $scope.profile.lastname,
                homeAddress: $scope.profile.homeAddress,
                phone: $scope.profile.phone,
                email: $scope.profile.email,
                who: $scope.profile.who
            }).success(function (data) {
                $state.go("home.profile.main", {}, {reload: true});
            }).error(function (status, data) {
                console.log(data);
                console.log(status);
            });

        };
    });
});


