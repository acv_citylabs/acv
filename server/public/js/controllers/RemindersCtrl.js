angular.module('RemindersCtrl', []).controller('RemindersController', function($scope, gettextCatalog, $state, $ocLazyLoad, $injector, $rootScope, ModalService) {
    $ocLazyLoad.load('js/services/SessionService.js').then(function() {
        var Session = $injector.get('Session');
        var helper = {
            title: gettextCatalog.getString("No reminder yet?"),
            text: gettextCatalog.getString("You can add a reminder on a session via the home page")
        };
        $scope.whenList = [gettextCatalog.getString('1 hour before'), gettextCatalog.getString('1 day before')];
        $scope.sessions = [], $scope.helper = [];

        function getReminders(){
            $scope.helper = [];
            Session.reminders().success(function(data){
                if(data.length == 0 && $scope.helper.length == 0){
                    $scope.helper = helper;
                }
                for(i=0;i<data.length;i++){
                    if(data[i].image){
                        data[i].image = {'background': 'url(../../files/sessions/'+data[i].image+') no-repeat center center #eee', 'background-size': '90px'};
                    } else {
                        data[i].image = {'background': 'url(../../img/logo-xs.png) no-repeat center center #eee', 'background-size': '90px'};
                    }
                }
                $scope.sessions = data;
            }).error(function(status, data) {
                console.log(data);
                console.log(status);
            });
        }
        getReminders();

        function deleteReminder(id){
            Session.follow({
                id : id
            }).success(function(data){
                getReminders();
            }).error(function(status, data) {
                console.log(data);
                console.log(status);
            });
        }
        
        $scope.details = function(session){
            ModalService.showModal({
                templateUrl: "templates/modals/sessionDetails.html",
                controller: function($scope, close){
                    $scope.session = session;
                    $scope.close = function(result) {
                        close(result, 500); // close, but give 500ms for bootstrap to animate
                    };
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(result) {
                    
                });
            });
        }
        
        
        $scope.deleteReminder = function(session){
            ModalService.showModal({
                templateUrl: "templates/modals/disableReminder.html",
                controller: function($scope, close){
                    $scope.session = session;
                    $scope.close = function(result) {
                        close(result, 500); // close, but give 500ms for bootstrap to animate
                    };
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(result) {
                    if(result){
                        deleteReminder(session._id);
                    }
                });
            });
        }
    });
});