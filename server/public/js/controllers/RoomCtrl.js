angular.module('RoomCtrl', []).controller('RoomController',

    function($scope, $log, $window, gettextCatalog, $stateParams, $state,
        $ocLazyLoad, $injector, $rootScope, ModalService, jwtHelper, deviceDetector) {

        var roomID = $stateParams.id;
        var api, profile, name, role, buttons;
        // Allows to detect if the OS user is Android or iOS, in order to advise him to download the mobile Jitsi App 
        // and remind ACV is not optimized for such devices.
        $scope.deviceDetector = deviceDetector;


        // Get room and session data
        $ocLazyLoad.load('js/services/RoomService.js').then(function() {
            var Room = $injector.get('Room');
            Room.read({
                sessionID: roomID,
                token: $window.sessionStorage.roomToken
            }).success(function(data) {
                if (data.ok) {
                    $window.sessionStorage.roomToken = data.token;
                    // copy the entire session data into the $scope, but...

                    $scope.session = data.session;
                    //$log.debug("Hey", $scope.session);
                }
            }).error(function(data) {
                console.log(data);
            });
        });


        $scope.isConnected = false;

        $scope.showDeviceWarning = function(device_infos) {
            ModalService.showModal({
                templateUrl: "templates/modals/jitsiMobile.html",
                controller: function($scope, close) {
                    $scope.text = device_infos ; //"Please install the mobile app"+ 
                    $scope.close = function(result) {
                        close(result, 500); // close, but give 500ms for bootstrap to animate
                    };
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(result) {
                    $state.go("home.room");
                });
            });
        }

        $scope.launchjitsi = function(customDom) {
            //Get user profile
            $ocLazyLoad.load('js/utils/date.js').then(function() {
                var Account = $injector.get('Account');
                Account.read().success(function(data) {

                    var myDom, myHosts, myBosh;
                    switch (customDom) {
                        case 'UCL':
                            myDom = 'jitsi.acv.tele.crt1.net';
                            myHosts = {
                                domain: myDom,
                                muc: 'conference.jitsi.acv.tele.crt1.net',
                                focus: 'focus.jitsi.acv.tele.crt1.net' // FIXME: use XEP-0030
                            };
                            myBosh = '//jitsi.acv.tele.crt1.net/http-bind';
                            break;
                        case 'JITSI':
                            myDom = 'meet.jit.si';
                            myHosts = {
                                domain: myDom,
                                muc: 'conference.meet.jit.si', // FIXME: use XEP-0030
                                focus: 'focus.meet.jit.si'
                            };
                            myBosh = '//meet.jit.si/http-bind';
                            break;
                        default:
                            myDom = 'jitsi.acv.tele.crt1.net';
                            myHosts = {
                                domain: myDom,
                                muc: 'conference.jitsi.acv.tele.crt1.net',
                                focus: 'focus.jitsi.acv.tele.crt1.net' // FIXME: use XEP-0030
                            };
                            myBosh = '//jitsi.acv.tele.crt1.net/http-bind';
                            break;
                    }

                    profile = data;
                    name = profile.firstname + " " + profile.lastname;
                    role = profile.role;

                    var config = { // eslint-disable-line no-unused-vars
                        //    configLocation: './config.json', // see ./modules/HttpConfigFetch.js
                        /* hosts: {
                            domain: 'jitsi.acv.tele.crt1.net',
                            muc: 'conference.jitsi.acv.tele.crt1.net' // FIXME: use XEP-0030
                        }, */
                        hosts: myHosts,
                        bosh: myBosh, //'//jitsi.acv.tele.crt1.net/http-bind', // FIXME: use xep-0156 for that


                        //  getroomnode: function (path) { return 'someprefixpossiblybasedonpath'; },
                        //  useStunTurn: true, // use XEP-0215 to fetch STUN and TURN server
                        //  useIPv6: true, // ipv6 support. use at your own risk
                        useNicks: false,
                        clientNode: 'http://jitsi.org/jitsimeet', // The name of client node advertised in XEP-0115 'c' stanza
                        //focusUserJid: 'focus@auth.jitsi.acv.tele.crt1.net', // The real JID of focus participant - can be overridden here
                        //defaultSipNumber: '', // Default SIP number

                        // Desktop sharing method. Can be set to 'ext', 'webrtc' or false to disable.
                        desktopSharingChromeMethod: 'ext',
                        // The ID of the jidesha extension for Chrome.
                        desktopSharingChromeExtId: 'diibjkoicjeejcmhdnailmkgecihlobk',
                        // The media sources to use when using screen sharing with the Chrome
                        // extension.
                        desktopSharingChromeSources: ['screen', 'window', 'tab'],
                        // Required version of Chrome extension
                        desktopSharingChromeMinExtVersion: '0.1',

                        // The ID of the jidesha extension for Firefox. If null, we assume that no
                        // extension is required.
                        desktopSharingFirefoxExtId: null,
                        // Whether desktop sharing should be disabled on Firefox.
                        desktopSharingFirefoxDisabled: true,
                        // The maximum version of Firefox which requires a jidesha extension.
                        // Example: if set to 41, we will require the extension for Firefox versions
                        // up to and including 41. On Firefox 42 and higher, we will run without the
                        // extension.
                        // If set to -1, an extension will be required for all versions of Firefox.
                        desktopSharingFirefoxMaxVersionExtRequired: -1,
                        // The URL to the Firefox extension for desktop sharing.
                        desktopSharingFirefoxExtensionURL: null,

                        // Disables ICE/UDP by filtering out local and remote UDP candidates in signalling.
                        webrtcIceUdpDisable: false,
                        // Disables ICE/TCP by filtering out local and remote TCP candidates in signalling.
                        webrtcIceTcpDisable: false,

                        openSctp: true, // Toggle to enable/disable SCTP channels
                        disableStats: false,
                        disableAudioLevels: false,
                        channelLastN: -1, // The default value of the channel attribute last-n.
                        adaptiveLastN: false,
                        //disableAdaptiveSimulcast: false,
                        enableRecording: false,
                        enableWelcomePage: true,
                        //enableClosePage: false, // enabling the close page will ignore the welcome
                        // page redirection when call is hangup
                        disableSimulcast: false,
                        logStats: false, // Enable logging of PeerConnection stats via the focus
                        //    requireDisplayName: true, // Forces the participants that doesn't have display name to enter it when they enter the room.
                        //    startAudioMuted: 10, // every participant after the Nth will start audio muted
                        //    startVideoMuted: 10, // every participant after the Nth will start video muted
                        //    defaultLanguage: "en",
                        // To enable sending statistics to callstats.io you should provide Applicaiton ID and Secret.
                        //    callStatsID: "", // Application ID for callstats.io API
                        //    callStatsSecret: "", // Secret for callstats.io API
                        /*noticeMessage: 'Service update is scheduled for 16th March 2015. ' +
                        'During that time service will not be available. ' +
                        'Apologise for inconvenience.',*/
                        disableThirdPartyRequests: false,
                        minHDHeight: 540,
                        // If true - all users without token will be considered guests and all users
                        // with token will be considered non-guests. Only guests will be allowed to
                        // edit their profile.
                        enableUserRolesBasedOnToken: false,
                        // Suspending video might cause problems with audio playback. Disabling until these are fixed.
                        disableSuspendVideo: true
                    };


                    if (role == '3') {
                        buttons = [
                            //main toolbar
                            'microphone', 'camera', 'fullscreen', 'hangup',
                            //extended toolbar
                            'contacts', 'chat', 'raisehand', 'etherpad', 'settings'
                        ];

                        settings = ['language', 'devices'];

                    } else {
                        buttons = [
                            //main toolbar
                            'microphone', 'camera', 'fullscreen', 'hangup',
                            //extended toolbar
                            'contacts', 'chat', 'raisehand', 'etherpad', 'settings', 'sharedvideo'
                        ];

                        settings = ['language', 'devices', 'moderator'];

                    }

                    var interfaceConfig = {

                       // DEFAULT_REMOTE_DISPLAY_NAME: name,
                        // DEFAULT_LOCAL_DISPLAY_NAME: "me",
                        SHOW_JITSI_WATERMARK: false,
                        // if watermark is disabled by default, it can be shown only for guests
                        SHOW_WATERMARK_FOR_GUESTS: false,
                        // BRAND_WATERMARK_LINK: "",
                        SHOW_POWERED_BY: false,
                        GENERATE_ROOMNAMES_ON_WELCOME_PAGE: false,
                        APP_NAME: "ACV - Jitsi Meet",
                        LANG_DETECTION: true,
                        SHOW_CONTACTLIST_AVATARS: true,
                        //  CLOSE_PAGE_GUEST_HINT: false,
                        ENABLE_FEEDBACK_ANIMATION: false,
                        LANG_DETECTION: true,
                        //  DISABLE_FOCUS_INDICATOR: false,
                        //  DISABLE_DOMINANT_SPEAKER_INDICATOR: false,
                        TOOLBAR_BUTTONS: buttons, // jshint ignore:line
                        /**
                         * Main Toolbar Buttons
                         * All of them should be in TOOLBAR_BUTTONS
                         */
                        MAIN_TOOLBAR_BUTTONS: ['microphone', 'camera', 'fullscreen', 'hangup'],
                        MOBILE_APP_PROMO: true
                        // jshint ignore:line
                    };

                    // var domain = 'jitsi.acv.tele.crt1.net'; //"meet.jit.si";
                    var domain = myDom;
                    var room = "acv213";
                    var width = 960;
                    var height = 600;
                    // var room_token = $window.sessionStorage.roomToken;
                    var htmlElement = document.querySelector('#meet');

                    api = new JitsiMeetExternalAPI(domain, room, width, height, htmlElement, config, interfaceConfig);
                    api.executeCommand('displayName', name);
                    $scope.isConnected = true;

                }).error(function(status, data) {
                    console.log(data);
                    console.log(status);
                });
            });
        }

        //  Automatically launch Jitsi when entering the room (disabled for testing purpose
        //  )
        $scope.launchjitsi('UCL');

        $scope.closejitsi = function() {
            api.dispose();
            $scope.isConnected = false;
            $state.go("home.main");
            $rootScope.rootAlerts.push({
                type: 'info',
                msg: gettextCatalog.getString("You left the session."),
                priority: 3
            });
        }


        if ($scope.isConnected) {
            $scope.$on('$locationChangeStart', function(event) {
                $scope.closejitsi();
            });
        }



    });

/* ====================== OLD - Before integrating Jitsi Meet =============================================================== 
 var roomID = $stateParams.id;

// keep a copy of the root scope
var rootScope = $scope.$root;

$ocLazyLoad.load('js/services/RoomService.js').then(function() {
    $scope.isWebRTCSupported = $window.mozRTCPeerConnection || $window.webkitRTCPeerConnection;
    var Room = $injector.get('Room');
    rootScope.roomReadInterval = null;
    rootScope.roomReadTimeout = null;
    var period = 1500;
    $scope.peers = [];
    var currentMapping = null;

    // remember who we are dealing with, and for which cycle
    var requestedOutgoingVideos = {};
    var requestedIncomingVideos = {};

    var updateConnectionStatus = function() {
        if (rootScope.webrtc) {
            var currentStatus;
            currentStatus = rootScope.webrtc.status();
            $scope.peers.forEach(function(peer) {
                if (!peer.socketID) {
                    peer.connected = false;
                    peer.receivingVideo = false;
                    peer.sendingVideo = false;
                } else if (peer.socketID === rootScope.webrtc.connection.getSessionid()) {
                    // this peer is ourself
                    // let's assume we are connected if localMedia is ready and the socket is authenticated
                    if (currentStatus.localMedia && currentStatus.socketIsAuthenticated) {
                        peer.connected = true;
                    } else {
                        peer.connected = false;
                    }
                } else {
                    var audioPeerDesc = peer.socketID + '/_audioIN_audioOUT';
                    var audioStatus = currentStatus.peersStates[audioPeerDesc];
                    if (typeof audioStatus === 'object') {
                        peer.connected = audioStatus.connected;
                    } else {
                        peer.connected = false;
                    }

                    var videoInDesc = peer.socketID + '/_videoIN';
                    var videoInStatus = currentStatus.peersStates[videoInDesc];
                    if (typeof videoInStatus === 'object') {
                        peer.receivingVideo = videoInStatus.connected;
                    } else {
                        peer.receivingVideo = false;
                    }

                    var videoOutDesc = peer.socketID + '/_videoOUT';
                    var videoOutStatus = currentStatus.peersStates[videoOutDesc];
                    if (typeof videoOutStatus === 'object') {
                        peer.sendingVideo = videoOutStatus.connected;
                    } else {
                        peer.sendingVideo = false;
                    }
                }
            });
        } else {
            //                console.log('could not update connection status : webrtc object not yet available');
        }
    }


    var sendVideoTo = function(peerID) {
        rootScope.webrtc.addPeer(peerID, { audio: { receive: false, send: false }, video: { receive: false, send: true } });
    }

    var stopSendingVideoTo = function(peerID) {
        rootScope.webrtc.removePeer(peerID, { audio: { receive: false, send: false }, video: { receive: false, send: true } });
    }


    var receiveVideoFrom = function(peerID) {
        rootScope.webrtc.addPeer(peerID, { audio: { receive: false, send: false }, video: { receive: true, send: false } });
    }

    var stopReceivingVideoFrom = function(peerID) {
        rootScope.webrtc.removePeer(peerID, { audio: { receive: false, send: false }, video: { receive: true, send: false } });
    }


    // maintains the video connections that we initiated
    function updateVideoConnections() {
        // remove the videos that we initiated but are not referenced anymore
        rootScope.webrtc.peers.forEach(function(peerDesc) {
            var peerID = peerDesc.split('/')[0];
            if ((peerDesc.indexOf('_videoIN') > -1) && (!requestedIncomingVideos[peerID])) {
                //                    console.log('updateVideoConnections : removing video ', peerDesc);
                stopReceivingVideoFrom(peerID);
            }
            if ((peerDesc.indexOf('_videoOUT') > -1) && (!requestedOutgoingVideos[peerID])) {
                //                    console.log('updateVideoConnections : removing video ', peerDesc);
                stopSendingVideoTo(peerID);
            }
        });

        // add the referenced videos - it will silently ignore the pre-existing peers
        for (var peerID in requestedOutgoingVideos) {
            sendVideoTo(peerID);
        }
        for (var peerID in requestedIncomingVideos) {
            receiveVideoFrom(peerID);
        }
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * For the given peer ID, computes the peers that it should receive the video from and send its video to.
     * If the optional skip offset is provided, which must be positive, the function will recursively find the next recipient.
     * "next" doesn't mean our user's recipient for the next modulus, but the recipient of its recipient for this modulus (for skip === 1)
     * or its recipient (for skip === 2) etc...
     * Note : The skip is ignored if we are not in the peers list (because we should not send a video stream before the next cycle)
     * Note 2 : If there is only one other peer, we send and receive from him
     * params : 
     *      userID        our subject
     *      peersList     array of the peers currently in the session 
     *      modulus       the current modulus given by the server
     *      skip          the optional offset to find the following recipients in the sequence
     * returns :
     *      an object of this format :
     *      {
     *          receiveFrom : 'someID',         null if we are alone
     *          sendTo : 'someID',              null if we are not in the list
     *          skipped : nb                    number of peers skipped
     *      }
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/* function computeVideoMapping(userID, list, modulus, skip) {
            // sanity check
            if (!!skip && skip < 0) {
                console.error('error in computeVideoMapping : received a negative "skip" parameter :', skip);
                skip = 0;
            }

            var result = {
                receiveFrom: null,
                sendTo: null,
                skipped: skip
            }


            var userWasIncluded = list.indexOf(userID) > -1;

            var includingList = list.slice();
            if (!userWasIncluded) includingList.unshift(userID);
            includingList.sort();

            var userPosition = includingList.indexOf(userID);
            var nbPeers = includingList.length;

            if (nbPeers < 2) {
                result.sendTo = result.receiveFrom = null;
            } else if (nbPeers === 2) {
                result.sendTo = result.receiveFrom = includingList[(userPosition + 1) % 2];
            } else {
                var modulo = modulus % (nbPeers - 1);
                var sendToIndex = (userPosition + 1 + modulo) % nbPeers;
                var receiveFromIndex = (userPosition - 1 - modulo + nbPeers) % nbPeers; //+nbPeers is to stay in the positive

                console.log('indices : from ', receiveFromIndex, ' me', userPosition, ' to ', sendToIndex);

                result.sendTo = includingList[sendToIndex];
                result.receiveFrom = includingList[receiveFromIndex];

                // users who are not in the list should not send to anyone except if there is only one other peer
                if (!userWasIncluded) {
                    result.sendTo = null;
                } else {
                    // issue recursive calls to "skip" one or more recipients
                    if (arguments.length === 4 && skip > 0) {
                        result.sendTo = computeVideoMapping(result.sendTo, list, modulus, skip - 1).sendTo;
                    }
                }
            }

            return result;
        }

        testComputeVideoMapping = function() {
            var list = [
                "--*---",
                "-*----",
                "*-----",
                "----*-",
                "-----*",
                "---*--"
            ];
            var modulus = Math.floor(new Date().getTime() / 1000);

            for (var i = 0; i < 5; i++) {
                var user = '--*---';
                var result = computeVideoMapping(user, list, modulus, i);
                console.log('video mapping for modulus ', modulus, 'user : ', { user: user }, result);
            }
            console.log('------------');
        }

        function read() {
            Room.read({
                sessionID: roomID,
                token: $window.sessionStorage.roomToken
            }).success(function(data) {
                if (data.ok) {
                    $window.sessionStorage.roomToken = data.token;

                    var isNewRotation;
                    if ((!$scope.session) || (!currentMapping) ||
                        ($scope.session.currentRotation !== data.session.currentRotation))  {
                        isNewRotation = true;
                    } else {
                        isNewRotation = false;
                    }

                    // copy the entire session data into the $scope, but...
                    $scope.session = data.session;


                    // ...use our own peers array and modify it only when necessary to avoid intempestive angular refreshes
                    var peersToRemove = {};
                    var peersToUpdate = {};
                    var peersToAdd = {};
                    var removeCount = 0;

                    // fill in a list of the peers we want updated (simply all the peers)
                    $scope.peers.forEach(function(peer) {
                        peersToUpdate[peer.id] = true;
                    });

                    // if they are not known anymore by the server (not listed for update),
                    // or if they are new entries, fill in the lists accordingly 
                    data.session.peers.forEach(function(peer, index) {
                        if (peersToUpdate[peer._id]) {
                            peersToUpdate[peer._id] = peer;
                        } else {
                            peersToAdd[peer._id] = peer;
                        }
                    });

                    // update the scope's peers table and fill the list of peers that must be removed (those not updated or added)
                    $scope.peers.forEach(function(peer) {
                        var peerToUpdate = peersToUpdate[peer.id];
                        if (peerToUpdate === true) { // the peer is to be removed
                            peersToRemove[peer.id] = true;
                            delete peersToUpdate[peer._id];
                        } else { // the peer is to be updated, update now
                            peer.firstname = peerToUpdate.firstname;
                            peer.avatar = peerToUpdate.avatar;
                            peer.socketID = peerToUpdate.socketID;
                        }
                    });

                    // add the new peers
                    for (peerID in peersToAdd) {
                        var peerToAdd = peersToAdd[peerID];
                        $scope.peers.push({
                            id: peerToAdd._id,
                            firstname: peerToAdd.firstname,
                            socketID: peerToAdd.socketID,
                            connected: false,
                            receivingVideo: false,
                            sendingVideo: false,
                            speaking: false,
                            volume: 0
                        });
                    }

                    // remove the old peers
                    if (peersToRemove.length > 0) {
                        $scope.peers = $scope.peers.filter(function(peer) {
                            return !(peersToRemove[peer.id] === true);
                        });
                    }

                    // update the displayed list of users
                    updateConnectionStatus();

                    // add the div element which displays the remote video's participant's name if it does not exist yet
                    $scope.peers.forEach(function(peer) {
                        if (peer.receivingVideo) {
                            var container = document.getElementById('container_' + peer.socketID + '_video_incoming');
                            if (container) {
                                if (!document.getElementById(peer.socketID + '_firstName')) {
                                    var nameDiv = document.createElement('div');
                                    nameDiv.id = peer.socketID + '_firstName';
                                    nameDiv.className = "who";
                                    nameDiv.innerHTML = peer.firstname;
                                    container.insertBefore(nameDiv, container.children[0]);
                                }
                            }
                        }
                    });

                    // handle video rotation
                    if (rootScope.webrtc) {
                        var peerID = rootScope.webrtc.getId();
                        var modulus = data.session.currentRotation;

                        // keep track of the current mapping, so that if our target peer leaves, we can directly send to the following one
                        // for the rest of this cycle, if it is not responding neither, the next one etc
                        if (isNewRotation) {
                            currentMapping = computeVideoMapping(peerID, data.session.activePeers, modulus);
                        }
                        //                        console.log(peerID, data.session.activePeers, currentMapping);

                        if (currentMapping.sendTo) requestedOutgoingVideos[currentMapping.sendTo] = modulus;
                        else if (currentMapping.receiveFrom) {
                            // we are not in the current activePeers list, ask for a video to our "preceding" peer,
                            // the peer which would be sending to us if we were in the current list
                            requestedIncomingVideos[currentMapping.receiveFrom] = modulus;
                        }


                        if (!data.remainingRotationTime) {
                            console.error('error in read() : the server did not send the remainingRotationTime info');
                        } else {
                            if (data.remainingRotationTime > 1500) {
                                schedulePoll(data.remainingRotationTime - 1500);
                            } else {
                                // schedule a poll just after the next peers list becomes definitive (at the beginning of the next cycle)
                                schedulePoll(data.remainingRotationTime);

                                // connect to the next peer already
                                var sendTo = computeVideoMapping(peerID, data.session.activePeers, modulus + 1).sendTo;
                                if (sendTo) requestedOutgoingVideos[sendTo] = modulus + 1;
                            }
                        }

                        // clean the old stuff
                        for (var id in requestedOutgoingVideos) {
                            if (requestedOutgoingVideos[id] < modulus) {
                                delete requestedOutgoingVideos[id];
                            }
                        }
                        for (var id in requestedIncomingVideos) {
                            if (requestedIncomingVideos[id] < modulus) {
                                delete requestedIncomingVideos[id];
                            }
                        }

                        if (isNewRotation)
                            console.log('is new rotation: ', data.session.currentRotation);

                        console.log('currentMapping', currentMapping, ' with peers', $scope.session.activePeers);
                        console.log('data', data);
                        console.log('data.remainingRotationTime: ', data.remainingRotationTime);
                        console.log('requestedOutgoingVideos: ', requestedOutgoingVideos);
                        console.log('requestedIncomingVideos: ', requestedIncomingVideos);
                        updateVideoConnections();
                        console.log('current peers : ', w.peers);

                    }
                } else {
                    $state.go("home.main");

                    if (data.tooSoon) {
                        $rootScope.rootAlerts.push({
                            type: 'info',
                            msg: gettextCatalog.getString("You can join the session 15 minutes before it starts."),
                            priority: 4
                        });
                    } else if (data.duplicate) {
                        $rootScope.rootAlerts.push({
                            type: 'warning',
                            msg: gettextCatalog.getString("You already have joined this session."),
                            priority: 3
                        });
                    } else if (data.tooLate) {
                        $rootScope.rootAlerts.push({
                            type: 'info',
                            msg: gettextCatalog.getString("The session has already started."),
                            priority: 3
                        });
                    } else if (data.animatorFirst) {
                        $rootScope.rootAlerts.push({
                            type: 'info',
                            msg: gettextCatalog.getString("Sorry, but you have to wait for the animator."),
                            priority: 4
                        });
                    } else if (data.full) {
                        $rootScope.rootAlerts.push({
                            type: 'info',
                            msg: gettextCatalog.getString("Sorry, but this session is full."),
                            priority: 4
                        });
                    }
                }
            }).error(function(status, data) {
                $rootScope.rootAlerts.push({
                    type: 'danger',
                    msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                    priority: 2
                });
            });
        }

        function poll() {
            if (rootScope.roomReadInterval) {
                clearInterval(rootScope.roomReadInterval);
                rootScope.roomReadInterval = null;
            }

            rootScope.roomReadInterval = setInterval(function() {
                read();
            }, period);
            read();
        }
        poll();

        function schedulePoll(time) {
            if (rootScope.roomReadTimeout) clearTimeout(rootScope.roomReadTimeout);
            rootScope.roomReadTimeout = setTimeout(function() {
                poll();
                rootScope.roomReadTimeout = null;
            }, time);
        }

        $scope.$on('$stateChangeStart', function(event) {
            if (rootScope.roomReadInterval) {
                clearInterval(rootScope.roomReadInterval);
                rootScope.roomReadInterval = null;
            }

            if (rootScope.roomReadTimeout) {
                clearTimeout(rootScope.roomReadTimeout);
                rootScope.roomReadTimeout = null;
            }

            if (rootScope.webrtc) {
                rootScope.webrtc.stop();
            }
        });

        $scope.leave = function() {
            $state.go("home.main");
            $rootScope.rootAlerts.push({
                type: 'info',
                msg: gettextCatalog.getString("You left the session."),
                priority: 3
            });
        }

       if ($scope.isWebRTCSupported) {
            $ocLazyLoad.load('additional_components/socket.io/socket.io.0.9.16.js').then(function () {
                $ocLazyLoad.load('additional_components/simplewebrtc/simplewebrtc.bundle-2.1.0.js').then(function () {
                    $ocLazyLoad.load('additional_components/webrtc/webrtc.js').then(function () {

                        var getSignalingToken = function _getSignalingToken(socketID, cb) {
                            if (!roomID) console.error('getSignalingToken was called but we have no roomID');
                            if (window.location.pathname.indexOf(rootScope.webrtc.room) === -1) {
                                rootScope.webrtc.stop();
                                return cb("Error : stopping webrtc because the location has changed (url)");
                            } else {
                                Room.getToken({
                                    sessionID: roomID,
                                    token: $window.sessionStorage.roomToken,
                                    socketID: socketID
                                }).success(function(data) {
                                    return cb(null, data.signalingToken);
                                }).error(function (err) {
                                    console.error('Error: could not get a signaling token for room "' + roomID + '" ', err);
                                    if ($rootScope.rootAlerts.length === 0) {
                                        $rootScope.rootAlerts.push({
                                            type: 'danger',
                                            msg: gettextCatalog.getString('An error occurred, please try again later'),
                                            show: true
                                        });
                                    }
                                    return cb(err);
                                });
                            }
                        }

                        if (! rootScope.webrtc) {
                            w =
                            rootScope.webrtc = new WebRTC(roomID, "https://virtuel.alzheimercafe.be:8889", getSignalingToken);
                            rootScope.webrtc.onStopped = function() {}
                            rootScope.webrtc.onMediaClosed = function() {}
                            rootScope.webrtc.onMediaOpened = function() {
//                                console.log('ON MEDIA OPENED');

                            }
                            rootScope.webrtc.onReadyToCall = function() {
//                                console.log('ON READY TO CALL');
                            }
                        }

                        rootScope.webrtc.onPeerAdded = function (peer, container) {
                            if (container.id.indexOf("video") > -1) {
                                var name = null;
                                $scope.session.peers.forEach(function(peerInPeers) {
                                    if (peerInPeers.socketID === peer.id) name = peerInPeers.firstname;
                                });
                                if (!name) {
                                    name = "name not found";
                                    poll();
                                } else {
                                    var nameDiv = document.createElement('div');
                                    nameDiv.className = "who";
                                    nameDiv.innerHTML = name;
                                    container.insertBefore(nameDiv, container.children[0]);
                                }
                            }
                        }
                        
                        rootScope.webrtc.webrtc.on('channelMessage', function (peer, label, data) {
                            if (data.type == 'speaking') {
                                $scope.peers.forEach(function(item) {
                                    if (peer.id === item.socketID) {
                                        item.speaking = true;
                                    }
                                });
                            }
                        });
                        
                        rootScope.webrtc.webrtc.on('speaking', function (peer) {
                            $scope.peers.forEach(function(item) {
                                if (rootScope.webrtc.getId() === item.socketID) {
                                    item.speaking = true;
                                }
                            });
                        });
                        
                        rootScope.webrtc.webrtc.on('channelMessage', function (peer, label, data) {
                            if (data.type == 'stoppedSpeaking') {
                                $scope.peers.forEach(function(item) {
                                    if (peer.id === item.socketID) {
                                        item.speaking = false;
                                    }
                                });
                            }
                        });
                        
                        rootScope.webrtc.webrtc.on('stoppedSpeaking', function (peer) {
                            $scope.peers.forEach(function(item) {
                                if (rootScope.webrtc.getId() === item.socketID) {
                                    item.speaking = false;
                                }
                            });
                        });
                        
                        rootScope.webrtc.webrtc.on('volumeChange', function (volume, treshold) {
                            $scope.peers.forEach(function(item) {
                                if (rootScope.webrtc.getId() === item.socketID) {
                                    item.volume = Math.max(0, 100-Math.abs(volume));
                                }
                            });
                        });
                        
                        rootScope.webrtc.webrtc.on('remoteVolumeChange', function (peer, volume) {
                            $scope.peers.forEach(function(item) {
                                if (peer.id === item.socketID) {
                                    item.volume = Math.max(0, 100-Math.abs(volume));
                                }
                            });
                        });
                        
                        

                        rootScope.webrtc.start(true,  true,  true);



                        // helper functions (for debugging in the console only)
                        wr = w.webrtc.webrtc;
                        ls = wr.localStreams;
                        rs = rootScope;
                        stat = function() { console.log(w.status()); }
                        pause = function() { wr.pause(); }
                        mute = function() { wr.mute(); }
                        resume = function() { wr.resume(); }
                        target = function(media, signaling, peer) {
                            rootScope.webrtc.start(media, signaling, peer);
                        }
                        toggle = $scope.toggle = function() { if (rootScope.webrtc.peer) target(0,0,0); else target(1,1,1); }
                        tgm = tgmedia = function() { 
                            if (rootScope.webrtc.media) target(0,rootScope.webrtc.signaling,0);
                            else                        target(1,rootScope.webrtc.signaling,rootScope.webrtc.signaling && rootScope.webrtc.peer);
                        }
                        tgs = tgsignal = function() { 
                            if (rootScope.webrtc.signaling) target(rootScope.webrtc.media,0,0);
                            else                            target(rootScope.webrtc.media,1,rootScope.webrtc.media && rootScope.webrtc.peer);
                        }
                        tgp = tgpeer = function() { 
                            if (rootScope.webrtc.peer) target(rootScope.webrtc.media, rootScope.webrtc.signaling, 0);
                            else                       target(1,1,1);
                        }
                        r = room = function() { rootScope.webrtc.connection.emit('getRoomDescription'); }

                        p = function() { return wr.getPeers()[0]; }
                        ps = function() { return wr.getPeers(); }
                        pc = function() { return p().pc; }

                        showPeerTracks = function(peer) {
                            console.log('remote stream :');
                            peer.pc.getRemoteStreams()[0].getTracks().forEach(function(track) {
                                console.log(peer.id, track.type, track.readyState ? track.readyState : 'readyState not supported');
                            });
                            console.log('local stream :');
                        }

                        showPeersTracks = function(peer) {
                            wr.getPeers().forEach(function(peer) { showPeerTracks(peer); });
                        }

                        stats = function() {
                            wr.peers.forEach(function(peer) {
                                peer.pc.getStats(function(error, data){
                                    var sent = 0,
                                        received = 0;
                                    console.log(data)
                                    for (var attr in data) {
                                        if (typeof data[attr]['ssrc'] !== "undefined") {
                                            console.log('SSRC  '+('____________'+data[attr]['ssrc']).slice(-12)+'__ :  '+data[attr]['id']);
                                            console.log('    sent : ' + ("          "+((typeof data[attr]['bytesSent'] !== 'undefined') ?
                                                                                     data[attr]['bytesSent']:0)).slice(-10) + ' bytes    ' + attr);
                                            console.log('received : ' + ("          "+((typeof data[attr]['bytesReceived'] !== 'undefined') ?
                                                                                     data[attr]['bytesReceived']:0)).slice(-10) + ' bytes    ' + attr);

                                            console.log('\n');
                                        }
                                        sent     += new Number((typeof data[attr]['bytesSent']     !== 'undefined') ? data[attr]['bytesSent'] : 0);
                                        received += new Number((typeof data[attr]['bytesReceived'] !== 'undefined') ? data[attr]['bytesReceived'] : 0);
                                    }
                                    console.log('total bytes sent : ', sent, '  bytes received : ', received);
                                });
                            });
                        }


                        listCommands = function() {
                            console.log('wr            =>    w.webrtc.webrtc;');
                            console.log('ls            =>    w.webrtc.webrtc.localStreams;');
                            console.log('stat          =>    console.log(w.status());');
                            console.log('pause         =>    w.webrtc.webrtc.pause();');
                            console.log('mute          =>    w.webrtc.webrtc.mute();');
                            console.log('resume        =>    w.webrtc.webrtc.resume();');
                            console.log();
                            console.log('target        =>    rootScope.webrtc.start(media, signaling, peer);');
                            console.log();
                            console.log('toggle        =>    if (rootScope.webrtc.peer) target(0,0,0); else target(1,1,1);');
                            console.log('tgm, tgmedia  =>    switches media');
                            console.log('tgs, tgsignal =>    switches signaling');
                            console.log('tgp, tgpeer   =>    switches peer');
                            console.log();
                            console.log('r, room       =>    sends "getRoomDescription" to get an updated list of peers');
                            console.log();
                            console.log('p             =>    w.webrtc.webrtc.getPeers()[0];');
                            console.log('pc            =>    w.webrtc.webrtc.getPeers()[0].pc;');
                            console.log('ps            =>    w.webrtc.webrtc.getPeers();');
                            console.log();
                            console.log('stats         =>    returns a table of sent and received bytes by peer/stream/track/rtp flow');
                        }
                    }); //$ocLazyLoad.load('additional_components/webrtc/webrtc.js')
                }); //$ocLazyLoad.load('additional_components/simplewebrtc/simplewebrtc.bundle-2.1.0.js')
            }); //$ocLazyLoad.load('additional_components/socket.io/socket.io.0.9.16.js')
        } else {
            console.log('error: WebRTC is not supported')
        } //if WebRTC Supported 
    });
});*/