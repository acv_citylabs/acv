angular.module('SettingsCtrl', []).controller('SettingsController', function($scope, $state, $window, gettextCatalog, $timeout, $ocLazyLoad, $injector, $rootScope) {
    $ocLazyLoad.load('js/services/AccountService.js').then(function() {
        var Account = $injector.get('Account');
        
        $scope.languagesList = [
            {name:'English', value:'EN'},
            {name:'Français', value:'FR'},
            {name:'Nederlands', value:'NL'}
        ];
        $scope.language = $window.localStorage.language;

        $scope.oldPassword = "";
        $scope.newPassword = "";
        $scope.newPasswordConfirmation = "";
        
        $scope.changeLanguage = function(){
            Account.update({
                language: $scope.language,
            }).success(function(data) {
                $window.localStorage.language = $scope.language;
                $state.go("home.settings.main",{}, {reload: true});
                $rootScope.rootAlerts.push({
                    type:'success',
                    msg: gettextCatalog.getString("Language changed!"),
                    priority: 5
                });
            }).error(function(status, data) {
                console.log(data);
                console.log(status);
            });
        };

        $scope.changePassword = function(form){
            if(form.newPassword.$valid && $scope.newPassword === $scope.newPasswordConfirmation){
                Account.changePassword({
                    oldPassword: $scope.oldPassword,
                    newPassword: $scope.newPassword,
                    newPasswordConfirmation: $scope.newPasswordConfirmation
                }).success(function(data) {
                    if(!data.new){
                        $scope.oldPassword= "";
                        $scope.newPassword= "";
                        $scope.newPasswordConfirmation= "";
                        $rootScope.rootAlerts.push({
                            type:'warning',
                            msg: gettextCatalog.getString("These passwords don't match."),
                            priority: 3
                        });
                    } else {
                        if(!data.old){
                            $scope.oldPassword= "";
                            $scope.newPassword= "";
                            $scope.newPasswordConfirmation= "";
                            $rootScope.rootAlerts.push({
                                type:'warning',
                                msg: gettextCatalog.getString("The current password you entered is incorrect"),
                                priority: 3
                            });
                        } else {
                            $scope.oldPassword= "";
                            $scope.newPassword= "";
                            $scope.newPasswordConfirmation= "";
                            $state.go("home.settings.main");
                            $rootScope.rootAlerts.push({
                                type:'success',
                                msg: gettextCatalog.getString("Your password was changed with success."),
                                priority: 5
                            });
                        }
                    }
                }).error(function(status, data) {
                    console.log("error password");
                });
            } else {
                if(!form.newPassword.$valid || $scope.newPassword.length < 6 ||
                   !form.newPasswordConfirmation.$valid && $scope.newPasswordConfirmation.length < 6){
                    $rootScope.rootAlerts.push({
                        type:'warning',
                        msg: gettextCatalog.getString("Please, use at least 6 characters for your password."),
                        priority: 3
                    });
                    $scope.newPassword = "";
                    $scope.newPasswordConfirmation = "";
                } else if($scope.newPassword !== $scope.newPasswordConfirmation){
                    $rootScope.rootAlerts.push({
                        type:'warning',
                        msg: gettextCatalog.getString("Sorry, but passwords don't match."),
                        priority: 3
                    });
                    $scope.newPassword = "";
                    $scope.newPasswordConfirmation = "";
                } else {
                    $scope.oldPassword= "";
                    $scope.newPassword= "";
                    $scope.newPasswordConfirmation= "";
                    $rootScope.rootAlerts.push({
                        type:'warning',
                        msg: gettextCatalog.getString("Please, fill out the form correctly."),
                        priority: 3
                    });
                }
            }
        };
    });
});
