angular.module('SigninCtrl', []).controller('SigninController', function($scope, $state, $window, gettextCatalog, $ocLazyLoad, $injector, $stateParams, ModalService) {    
    $ocLazyLoad.load('js/services/AccountService.js').then(function() {
        var Account = $injector.get('Account');
        $scope.rememberme = true;
        $scope.rememberLabel = gettextCatalog.getString('Stay signed in');
        $scope.alert = {};

        if($stateParams.isNew && $stateParams.email){
            ModalService.showModal({
                templateUrl: "templates/modals/signActivation.html",
                controller: function($scope, close){
                    $scope.close = function(result) {
                        close(result, 500); // close, but give 500ms for bootstrap to animate
                    };
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(result) {
                    $scope.email = $stateParams.email;
                    $('#password').focus();
                    $stateParams.isNew = null;
                    $stateParams.email = null;
                });
            });
        }
        
        if($stateParams.isResetted && $stateParams.email){
            ModalService.showModal({
                templateUrl: "templates/modals/signReset.html",
                controller: function($scope, close){
                    $scope.close = function(result) {
                        close(result, 500); // close, but give 500ms for bootstrap to animate
                    };
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(result) {
                    $scope.email = $stateParams.email;
                    $('#password').focus();
                    $stateParams.isResetted = null;
                    $stateParams.email = null;
                });
            });
        }
        
        if($stateParams.isLost){
            ModalService.showModal({
                templateUrl: "templates/modals/signSent.html",
                controller: function($scope, close){
                    $scope.close = function(result) {
                        close(result, 500); // close, but give 500ms for bootstrap to animate
                    };
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(result) {
                    $stateParams.isLost = null;
                });
            });
        }

        if($window.localStorage.token){
            $state.transitionTo('home.main');
        }
        
        if($stateParams.token){
            Account.activation({
                token: $stateParams.token
            }).success(function(data) {
                if(data.activated){
                    $scope.email = data.email;
                    $('#password').focus();
                    $scope.alert = {
                        type:'success', 
                        msg: gettextCatalog.getString('Your account has been activated! You can now sign in.')
                    };
                } else {
                    $scope.alert = {
                        type:'danger', 
                        msg: gettextCatalog.getString('The link you used to activate your account is invalid.')
                    };
                }
            }).error(function(status, data) {
                console.log(status);
                console.log(data);
                $scope.alert = {
                    type:'danger', 
                    msg: gettextCatalog.getString('Internal Server Error')
                };
            });
        }

        $scope.signin = function signin() {
            $scope.alert = {};
            if ($scope.email !== undefined && $scope.password !== undefined) {
                Account.signin({
                    email: $scope.email,
                    password: $scope.password,
                    rememberme: $scope.rememberme
                }).success(function(data) {
                    if(data.activated){
                        $window.localStorage.language = data.language;
                        $window.localStorage.token = data.token;
                        $state.transitionTo('home.main');
                    }
                    else{
                        $scope.alert = {
                            type:'warning', 
                            msg: gettextCatalog.getString('You need to activate your account.')
                        };
                    }
                }).error(function(status, data) {
                    $scope.password = "";
                    if(data === 401){
                        $scope.alert = {
                            type:'danger', 
                            msg: gettextCatalog.getString('The email or password you entered is incorrect.')
                        };
                    }
                    else{
                        $scope.alert = {
                            type:'danger', 
                            msg: status
                        };
                    }
                });
            }
            else{
                $scope.alert = {
                    type:'danger', 
                    msg: gettextCatalog.getString('The email or password is empty.')
                };
            }
        }
    });
});
