angular.module('SignupCtrl', []).controller('SignupController', function($scope, $state, $window, gettextCatalog, $ocLazyLoad, $injector, $stateParams) {
    $scope.smAlerts = [];

    $ocLazyLoad.load('js/services/AccountService.js').then(function() {
        var Account = $injector.get('Account');

        $scope.reset = function(form) {
            $scope.smAlerts = [];
            if(form.newPassword.$valid && $scope.newPassword === $scope.newPasswordConfirmation){
                Account.resetPassword({
                    token: $stateParams.token,
                    newPassword: $scope.newPassword,
                    newPasswordConfirmation: $scope.newPasswordConfirmation
                }).success(function(data) {
                    if(!data.token){
                        $scope.newPassword = "";
                        $scope.newPasswordConfirmation = "";
                        $scope.smAlerts.push({
                            class:'text-warning', 
                            msg: gettextCatalog.getString('The link you used to create a password is invalid or has expired.'), 
                            show: true 
                        });
                    } else {
                        if(!data.changed){
                            $scope.newPassword = "";
                            $scope.newPasswordConfirmation = "";
                            $scope.smAlerts.push({ 
                                class:'text-warning', 
                                msg: gettextCatalog.getString('Please, fill out the form correctly.'), 
                                show: true 
                            });
                        } else {
                            $state.go("signin", {"isNew": true, "email": data.email});
                        }
                    }
                }).error(function(status, data) {
                    $scope.newPassword = "";
                    $scope.newPasswordConfirmation = "";
                    $scope.smAlerts.push({ 
                        class:'text-danger', 
                        msg: gettextCatalog.getString('Internal Server Error'), 
                        show: true 
                    });
                });
            }
            else{
                $scope.newPassword = "";
                $scope.newPasswordConfirmation = "";
                $scope.smAlerts.push({ 
                    class:'text-warning', 
                    msg: gettextCatalog.getString('Please, fill out the form correctly.'), 
                    show: true 
                });
            }
        }
    });
});
