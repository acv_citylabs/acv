angular.module('VideoCtrl', []).controller('VideoController', function($scope, $window, gettextCatalog, $stateParams, $state, $ocLazyLoad, $injector, $rootScope, $timeout) {
	$ocLazyLoad.load('js/services/VideoService.js').then(function() {
    var Video = $injector.get('Video');
    Video.read().success(function(data){
    		if(data){
    			$scope.video = data;
    		}else{
    			$scope.video = {
    					name : "homeVideo",
    					link : ""
    			}
    		}
    }).error(function(status, data) {
        $rootScope.rootAlerts.push({
            type:'danger',
            msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
            priority: 2
        });
    });
  });
  
  $scope.submit = function(){
      $ocLazyLoad.load('js/services/VideoService.js').then(function() {
          var Video = $injector.get('Video');
          if($scope.video.link != ''){
          	
          	var code = $scope.video.link.split("=");
          	$scope.video.link = code[1];
            Video.update(
                $scope.video
            ).success(function(data){
                $state.transitionTo('home.main');
                $rootScope.rootAlerts.push({
                     type:'success',
                     msg: gettextCatalog.getString('The video was updated.'),
                     priority: 4
                });
            }).error(function(status, data) {
                $rootScope.rootAlerts.push({
                    type:'danger',
                    msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                    priority: 2
                });
            });
          }
      });
  }
});
