angular.module('AnimatorCtrl', []).controller('AnimatorController', function($scope, $window, gettextCatalog, $stateParams, $state, $ocLazyLoad, $injector, $rootScope, ModalService, $location) {
    $scope.languagesList = [
        {name:'English', value:'EN'},
        {name:'Français', value:'FR'},
        {name:'Nederlands', value:'NL'}
    ];
    $scope.cafesList = [];
    $scope.loading = false;
    $scope.sending = false;
    
    $ocLazyLoad.load('js/services/UserService.js').then(function() {
        var User = $injector.get('User');
        $ocLazyLoad.load('js/services/CafeService.js').then(function() {
            var Cafe = $injector.get('Cafe');
            
            function getCafes(){
                Cafe.list().success(function(data){
                    for(i=0; i<data.length; i++){
                        $scope.cafesList.push({
                            name: data[i].name,
                            value: data[i]._id
                        })
                    }
                }).error(function(status, data) {
                    console.log(data);
                    console.log(status);
                });
            }
            getCafes();

            if($stateParams.username !== undefined){
                $scope.new = false;
                User.readAnimator({
                    username : $stateParams.username
                }).success(function(data){
                    if($location.path().indexOf('profile') > -1){
                        for(i=0; i<$scope.cafesList.length; i++){
                            if($scope.cafesList[i].value === data.cafeID){
                                data.cafeID = $scope.cafesList[i].name;
                            }
                        }
                    }
                    $scope.animator = data;
                }).error(function(status, data) {
                    $rootScope.rootAlerts.push({
                        type:'danger',
                        msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                        priority: 2
                    });
                });
                
                function deleteAnimator(username){
                    User.delete({
                        username : username
                    }).success(function(data){
                        $state.transitionTo('home.animators.main');
                    }).error(function(status, data) {
                        console.log(data);
                        console.log(status);
                    });
                }

                $scope.deleteAnimator = function(animator){
                    ModalService.showModal({
                        templateUrl: "templates/modals/deleteAnimator.html",
                        controller: function($scope, close){
                            $scope.animator = animator;
                            $scope.close = function(result) {
                                close(result, 500); // close, but give 500ms for bootstrap to animate
                            };
                        }
                    }).then(function(modal) {
                        modal.element.modal();
                        modal.close.then(function(result) {
                            if(result){
                                deleteAnimator(animator.username);
                            }
                        });
                    });
                }
                
                $scope.send = function(){
                    $scope.sending = true;
                    User.sendPassword({
                        userID: $scope.animator._id
                    }).success(function(data){
                        $scope.sending = false;
                        $rootScope.rootAlerts.push({
                             type:'success',
                             msg: gettextCatalog.getString('Email sent.'),
                             priority: 4
                        });
                    }).error(function(status, data) {
                        $scope.sending = false;
                        $rootScope.rootAlerts.push({
                            type:'danger',
                            msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                            priority: 2
                        });
                    });
                }

                $scope.submit = function(){
                    User.update(
                        $scope.animator
                    ).success(function(data){
                        $state.transitionTo('home.animators.main');
                        $rootScope.rootAlerts.push({
                             type:'success',
                             msg: gettextCatalog.getString('The animator was updated.'),
                             priority: 4
                        });
                    }).error(function(status, data) {
                        $rootScope.rootAlerts.push({
                            type:'danger',
                            msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                            priority: 2
                        });
                    });
                }
            } else {
                $scope.new = true;
                $scope.submit = function(){
                    $scope.loading = true;
                    User.create(
                        $scope.animator
                    ).success(function(data){
                        $scope.loading = false;
                        $state.transitionTo('home.animators.main');
                        $rootScope.rootAlerts.push({
                             type:'success',
                             msg: gettextCatalog.getString('The animator was created.'),
                             priority: 4
                        });
                    }).error(function(err, status) {
                        $scope.loading = false;
                        if(err.errors.email){
                            $rootScope.rootAlerts.push({
                                type:'warning',
                                msg: gettextCatalog.getString("This email is already used."),
                                priority: 3
                            });
                        } else if(err.errors.username) {
                            $rootScope.rootAlerts.push({
                                type:'warning', 
                                msg: gettextCatalog.getString('This username is already used. Please pick another one.'),
                                priority: 3
                            });
                        } else {
                            $rootScope.rootAlerts.push({
                                type: 'danger',
                                msg: gettextCatalog.getString('An error occurred, please try again later'),
                                priority: 2
                            });
                        }
                    });
                }
            }
        });
    });
});
