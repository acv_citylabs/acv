angular.module('AnimatorsCtrl', []).controller('AnimatorsController', function($scope, $state, $window, gettextCatalog, $ocLazyLoad, $injector, ModalService) {
    $ocLazyLoad.load('js/services/UserService.js').then(function() {
        var User = $injector.get('User');
        var helper = {
            title: gettextCatalog.getString("No animator yet?"),
            text: gettextCatalog.getString("Use the green button above to create one.")
        };
        $scope.animators = [], $scope.helper = [];
        $scope.searchText = '';

        function getAnimators(){
            $scope.helper = [];
            User.list().success(function(data){
                if(data.length == 0 && $scope.helper.length == 0){
                    $scope.helper = helper;
                }
                $scope.animators = data;
            }).error(function(status, data) {
                console.log(data);
                console.log(status);
            });
        }
        getAnimators();

        function deleteAnimator(username){
            User.delete({
                username : username
            }).success(function(data){
                getAnimators();
            }).error(function(status, data) {
                console.log(data);
                console.log(status);
            });
        }
        
        
        $scope.deleteAnimator = function(animator){
            ModalService.showModal({
                templateUrl: "templates/modals/deleteAnimator.html",
                controller: function($scope, close){
                    $scope.animator = animator;
                    $scope.close = function(result) {
                        close(result, 500); // close, but give 500ms for bootstrap to animate
                    };
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(result) {
                    if(result){
                        deleteAnimator(animator.username);
                    }
                });
            });
        }
    });
});
