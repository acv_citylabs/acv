angular.module('CafeCtrl', []).controller('CafeController', function($scope, $window, gettextCatalog, $stateParams, $state, $ocLazyLoad, $injector, $rootScope) {
    if($stateParams.id !== undefined){
        $scope.new = false;
        $ocLazyLoad.load('js/services/CafeService.js').then(function() {
            var Cafe = $injector.get('Cafe');
            Cafe.read({
                id : $stateParams.id
            }).success(function(data){
                $scope.cafe = data;
            }).error(function(status, data) {
                $rootScope.rootAlerts.push({
                    type:'danger',
                    msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                    priority: 2
                });
            });
        });
        
        $scope.submit = function(){
            $ocLazyLoad.load('js/services/CafeService.js').then(function() {
                var Cafe = $injector.get('Cafe');
                $scope.cafe.id = $scope.cafe._id;
                Cafe.update(
                    $scope.cafe
                ).success(function(data){
                    $state.transitionTo('home.cafes.main');
                    $rootScope.rootAlerts.push({
                         type:'success',
                         msg: gettextCatalog.getString('The cafe was updated.'),
                         priority: 4
                    });
                }).error(function(status, data) {
                    $rootScope.rootAlerts.push({
                        type:'danger',
                        msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                        priority: 2
                    });
                });
            });
        }
    }
    else{
        $scope.new = true;
        $scope.submit = function(){
            $ocLazyLoad.load('js/services/CafeService.js').then(function() {
                var Cafe = $injector.get('Cafe');
                Cafe.create(
                    $scope.cafe
                ).success(function(data){
                    $state.transitionTo('home.cafes.main');
                    $rootScope.rootAlerts.push({
                         type:'success',
                         msg: gettextCatalog.getString('The cafe was created.'),
                         priority: 4
                    });
                }).error(function(status, data) {
                    $rootScope.rootAlerts.push({
                        type:'danger',
                        msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                        priority: 2
                    });
                });
            });
        }
    }
});
