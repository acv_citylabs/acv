angular.module('CafesCtrl', []).controller('CafesController', function($scope, $state, $window, gettextCatalog, $ocLazyLoad, $injector, ModalService) {
    $ocLazyLoad.load('js/services/CafeService.js').then(function() {
        var Cafe = $injector.get('Cafe');
        var helper = {
            title: gettextCatalog.getString("No café yet?"),
            text: gettextCatalog.getString("Use the green button above to create one.")
        };
        $scope.cafes = [], $scope.helper = [];
        $scope.searchText = '';

        function getCafes(){
            $scope.helper = [];
            Cafe.list().success(function(data){
                if(data.length == 0 && $scope.helper.length == 0){
                    $scope.helper = helper;
                }
                $scope.cafes = data;
            }).error(function(status, data) {
                console.log(data);
                console.log(status);
            });
        }
        getCafes();

        function deleteCafe(id){
            Cafe.delete({
                id : id
            }).success(function(data){
                getCafes();
            }).error(function(status, data) {
                console.log(data);
                console.log(status);
            });
        }
        
        
        $scope.deleteCafe = function(cafe){
            ModalService.showModal({
                templateUrl: "templates/modals/deleteCafe.html",
                controller: function($scope, close){
                    $scope.cafe = cafe;
                    $scope.close = function(result) {
                        close(result, 500); // close, but give 500ms for bootstrap to animate
                    };
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(result) {
                    if(result){
                        deleteCafe(cafe._id);
                    }
                });
            });
        }
    });
});
