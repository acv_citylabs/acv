angular.module('MycafeCtrl', []).controller('MycafeController', function($scope, gettextCatalog, $state, $ocLazyLoad, $injector, $rootScope) {
    $ocLazyLoad.load('js/services/CafeService.js').then(function() {
        var Cafe = $injector.get('Cafe');

        var helper = {
            title: gettextCatalog.getString("No café assigned"),
            text: gettextCatalog.getString("Please contact your administrator.")
        };
        $scope.helper = [];
        Cafe.readOwn({
        }).success(function(data){
            if(data.noCafe){
                $scope.helper = helper;
            } else {
                $scope.cafe = data;
            }
        }).error(function(status, data) {
            $rootScope.rootAlerts.push({
                type:'danger',
                msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                priority: 2
            });
        });
        
        $scope.submit = function(){
            var Cafe = $injector.get('Cafe');
            $scope.cafe.id = $scope.cafe._id;
            Cafe.update(
                $scope.cafe
            ).success(function(data){
                $rootScope.rootAlerts.push({
                     type:'success',
                     msg: gettextCatalog.getString('The cafe was updated.'),
                     priority: 4
                });
            }).error(function(status, data) {
                $rootScope.rootAlerts.push({
                    type:'danger',
                    msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                    priority: 2
                });
            });
        }
    });
});