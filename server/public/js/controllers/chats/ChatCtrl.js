angular.module('ChatCtrl', []).controller('ChatController', function ($scope, $window, gettextCatalog, $stateParams, $state, $ocLazyLoad, $injector, $rootScope, ModalService) {
    $ocLazyLoad.load('js/services/ChatService.js').then(function () {
        var Chat = $injector.get('Chat');
        $scope.hideOptions = true;
        $scope.forbidden = false;
        // keep a copy of the root scope
        var rootScope = $scope.$root;
        
        // contactUsername is the username of the contact if its account still exists,
        // otherwise it is underscore+old ID ("_23ANID3236234...")
        $scope.contactUsername = $stateParams.username;

        // check whether the contact account has been deleted
        $scope.contactIsDeleted = ($stateParams.username[0] === '_');

        if ($scope.contactUsername !== "") {
            $scope.alerts = [];
            $scope.isWebRTCSupported = $window.mozRTCPeerConnection || $window.webkitRTCPeerConnection;
            $scope.$root.isWebRTCActive = false;
            $scope.$root.callDirection = 'outgoing';
            $scope.isFullscreen = false;
            $scope.messages = [];
            $scope.accountDeleted = gettextCatalog.getString("Account deleted");
            var latestEventDate = null;
            var readChatInterval = null;
            var periodChat = 1000; // 1 second

            if (!$scope.contactIsDeleted) {
                $ocLazyLoad.load('js/services/ContactService.js').then(function () {
                    var Contact = $injector.get('Contact');
                    Contact.readLight({
                        username: $scope.contactUsername
                    }).success(function (profile) {
                        $scope.contactAvatar = profile.avatar;
                        $scope.contactUsername = profile.username;
                    });
                });
            }

            function readChat() {
                if (readChatInterval) {
                    clearInterval(readChatInterval);
                    readChatInterval = null;
                }

                readChatTimer();
                readChatInterval = setInterval(function () {
                    readChatTimer();
                }, periodChat);
            }

            var onWebRTC = function(data) {
//                console.log("onWebRTC : status : ", data);
                if (rootScope.callStatus !== data.status) rootScope.callStatus = data.status;
                if (rootScope.callDirection !== data.callDirection) rootScope.callDirection = data.callDirection;
            }

            function readChatTimer() {
                var command;
                if ($scope.$root.ringing || $scope.$root.calling) {
                    command = 'token';
                } else {
                    command = 'poll';
                }

                Chat.webrtc({
                    username: $stateParams.username,
                    command: command
                }).success(function(data){
                    onWebRTC(data);
                });

                Chat.readChat({
                    username: $scope.contactUsername,
                    afterDate: latestEventDate
                }).success(function (data) {
                    if ($scope.alerts.length > 0) {
                        $scope.alerts = [];
                    }
                    if(data.forbidden){
                        $scope.forbidden = true;
                        $scope.alerts.push({
                            type: 'warning',
                            msg: gettextCatalog.getString("You cannot reply to this conversation."),
                            show: true
                        });
                    }

                    if(data.archived !== null) {
                        $scope.hideOptions = false;
                        $scope.archived = data.archived;
                    }

                    // append to the current array
                    for (var i = 0; i < data.chat.length; i++) {
                        var documentDate = new Date(data.chat[i].datetime);
                        if (!latestEventDate || (documentDate > latestEventDate)) {
                            $scope.messages.push(data.chat[i]);
                            latestEventDate = documentDate;
                        }
                    }
                }).error(function (status, data) {
                    if ($scope.alerts.length === 0) {
                        $scope.alerts.push({
                            type: 'danger',
                            msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                            show: true
                        });
                    }
                });
            }
            readChat();

            $scope.sendMessage = function () {
                if ($scope.message && !$scope.contactIsDeleted) {
                    var message = $scope.message;
                    $scope.message = "";
                    Chat.sendMessage({
                        username: $stateParams.username,
                        msg: message
                    }).success(function (data) {
                        if ($scope.alerts.length > 0) {
                            $scope.alerts = [];
                        }
                        readChat();
                    }).error(function (status, data) {
                        $scope.message = message;
                        if ($scope.alerts.length === 0) {
                            $scope.alerts.push({
                                type: 'danger',
                                msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                                show: true
                            });
                        }
                    });
                }
            };

            $scope.toggleArchiveFlag = function () {
                var newFlag = !$scope.archived;
                Chat.setArchiveFlag({
                    username: $scope.contactUsername,
                    flag: newFlag
                }).success(function (data) {
                    if ($scope.alerts.length > 0) {
                        $scope.alerts = [];
                    }
                }).error(function (status, data) {
                    if ($scope.alerts.length === 0) {
                        $scope.alerts.push({
                            type: 'danger',
                            msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                            show: true
                        });
                    }
                });
            }
            
            $scope.deleteChat = function(){
                ModalService.showModal({
                    templateUrl: "templates/modals/deleteChat.html",
                    controller: function($scope, close){
                        $scope.chat = {username: $stateParams.username};
                        $scope.close = function(result) {
                            close(result, 500); // close, but give 500ms for bootstrap to animate
                        };
                    }
                }).then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(result) {
                        if(result){
                            Chat.deleteChat({
                                username: $stateParams.username
                            }).success(function (data) {
                                if ($scope.alerts.length > 0) {
                                    $scope.alerts = [];
                                }
                                $state.go("home.chats.main");
                            }).error(function (status, data) {
                                if ($scope.alerts.length === 0) {
                                    $scope.alerts.push({
                                        type: 'danger',
                                        msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                                        show: true
                                    });
                                }
                            });
                        }
                    });
                });
            }

            $scope.toggleFullScreen = function () {
                $scope.isFullscreen = !$scope.isFullscreen;
            };

            $scope.$on('$stateChangeStart', function (event) {
                if (readChatInterval) {
                    clearInterval(readChatInterval);
                    readChatInterval = null;
                }
            });

            // = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = //
            //    __      __           _         ___     _____     ___     //
            //    \ \    / /   ___    | |__     | _ \   |_   _|   / __|    //
            //     \ \/\/ /   / -_)   | '_ \    |   /     | |    | (__     //
            //      \_/\_/    \___|   |_.__/    |_|_\    _|_|_    \___|    //
            //    _|"""""|  _|"""""| _|"""""| _|"""""| _|"""""| _|"""""|   //
            //    "`-0-0-'  "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-'   //
            // = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = //
            //         _       __       __     ____  ______ ______         //
            //        | |     / /___   / /_   / __ \/_  __// ____/         //
            //        | | /| / // _ \ / __ \ / /_/ / / /  / /              //
            //        | |/ |/ //  __// /_/ // _, _/ / /  / /___            //
            //        |__/|__/ \___//_.___//_/ |_| /_/   \____/            //
            // = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = //
            if (!$scope.contactIsDeleted && $scope.isWebRTCSupported) {
                $ocLazyLoad.load('additional_components/socket.io/socket.io.0.9.16.js').then(function () {
                    $ocLazyLoad.load('additional_components/simplewebrtc/simplewebrtc.bundle-2.1.0.js').then(function () {
                        $ocLazyLoad.load('additional_components/webrtc/webrtc.js').then(function () {

                            rootScope.callStatus = 'inactive';

                            // compute the roomID (the two usernames sorted alphabetically and separated by '_')
                            // and keep it as a global variable
                            roomID = null;
                            if ($rootScope.user.username.localeCompare($scope.contactUsername) < 0){
                                roomID = $rootScope.user.username + '_' + $scope.contactUsername;
                            } else {
                                roomID = $scope.contactUsername + '_' + $rootScope.user.username;
                            }

                            var getSignalingToken = function _getSignalingToken(cb) {
                                console.log('getSignalingToken CALLED')
                                Chat.getSignalingToken(roomID).success(function (token) {
                                    console.log('getSignalingToken received token: ', token);
                                    console.log('getSignalingToken cb: ', cb);
                                    return cb(null, token);
                                }).error(function (err) {
                                    console.error('Error: could not get a signaling token for room "' + roomID + '" ', arguments);
                                    if ($scope.alerts.length === 0) {
                                        $scope.alerts.push({
                                            type: 'danger',
                                            msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                                            show: true
                                        });
                                    }
                                    return cb(null);
                                });
                            }

                            if (! rootScope.webrtc) {
                                w =
                                rootScope.webrtc = new WebRTC($rootScope.user.username, roomID, "https://beta.egle.be:8888", getSignalingToken);
                                rootScope.webrtc.onStopped = function() { rootScope.isWebRTCActive = false; }
                                rootScope.webrtc.onMediaClosed = function() { rootScope.isWebRTCActive = false; }
                                rootScope.webrtc.onMediaOpened = function() {
                                    rootScope.isWebRTCActive = true;
                                    console.log('ON MEDIA OPENED');

                                }
                                rootScope.webrtc.onReadyToCall = function() {
                                    rootScope.isWebRTCActive = true;
                                    console.log('ON READY TO CALL');
                                }
                            }


                            $scope.call = function _call() {
                                // if the server responds 'inactive', activate the media before issuing the call
                                Chat.webrtc({
                                    username: $stateParams.username,
                                    command: 'call'
                                }).success(function (data) {
                                    if (data.status !== rootScope.callStatus) {
                                        rootScope.webrtc.start({
                                            media: true,
                                            signaling: false
                                        });
                                    } else {
                                        rootScope.webrtc.start({
                                            media: true,
                                            signaling: true
                                        });
                                    }
                                    onWebRTC(data);

                                    rootScope.isWebRTCActive = true;
                                }).error(function (status, data) {
                                    if ($scope.alerts.length === 0) {
                                        $scope.alerts.push({
                                            type: 'danger',
                                            msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                                            show: true
                                        });
                                    }
                                });
                            }

                            $scope.hangup = function _hangup() {
                                rootScope.webrtc.stop();
                                rootScope.isWebRTCActive = false;
                                
                                // send hangup command to the server
                                Chat.webrtc({
                                    username: $stateParams.username,
                                    command: 'hangup'
                                }).success(function (data) {
                                    onWebRTC(data);
                                });
                            }

                            rootScope.$watch('callStatus', function(newVal, oldVal) {
                                console.log("WATCH : ", oldVal, '->', newVal);
                                if (newVal === 'calling') $scope.call();
                                else if (newVal === 'inactive') $scope.hangup();
                            });

                            // Trigger when exit this route
                            $scope.$on('$stateChangeStart', function (event) {
                                if (rootScope.callStatus !== 'inactive') $scope.hangup();
                            });


                            // helper functions (for the console)
                            wr = w.webrtc.webrtc;
                            ls = wr.localStreams;
                            rs = rootScope;
                            stat = function() { console.log(w.status()); }
                            pause = function() { wr.pause(); }
                            mute = function() { wr.mute(); }
                            resume = function() { wr.resume(); }
                            media = function(activate) { rootScope.webrtc.start({media:activate, signaling:false}); }
                            toggle = function() { rootScope.isWebRTCActive = !rootScope.isWebRTCActive; }
                            
                            remoteStreams = function() { return wr.peers[0].pc.pc.getRemoteStreams(); }
                            localStreams = function() { return wr.peers[0].pc.pc.getLocalStreams(); }
                            localStream = function() { return localStreams()[0]; }
                            remoteStream = function() { return remoteStreams()[0]; }
                            localTracks = function() { return localStream().getTracks() }
                            remoteTracks = function() { return remoteStream().getTracks() }
                            countStreams = function() { console.log('localStreams : ' + localStreams().length); console.log('remoteStreams : ' + remoteStreams().length); }
                            
                            p = function() { return wr.getPeers()[0]; }
                            pc = function() { return p().pc; }
                            
                            
                        }); //$ocLazyLoad.load('additional_components/webrtc/webrtc.js')
                    }); //$ocLazyLoad.load('additional_components/simplewebrtc/simplewebrtc.bundle-2.1.0.js')
                }); //$ocLazyLoad.load('additional_components/socket.io/socket.io.0.9.16.js')
            } //if WebRTC Supported
        } else {
            $state.go("home.chats.main");
        }  
    });
});
