angular.module('ParticipantCtrl', []).controller('ParticipantController', function($scope, $window, gettextCatalog, $stateParams, $state, $ocLazyLoad, $injector, $rootScope, ModalService) {
    $scope.languagesList = [
        {name:'English', value:'EN'},
        {name:'Français', value:'FR'},
        {name:'Nederlands', value:'NL'}
    ];
    $scope.loading = false;
    $scope.sending = false;
    $scope.whosList = [
        {name:gettextCatalog.getString("Patient"), value:'0'},
        {name:gettextCatalog.getString("Family member"), value:'1'},
        {name:gettextCatalog.getString("Friend"), value:'2'},
        {name:gettextCatalog.getString("Visitor/Interested"), value:'3'},
        {name:gettextCatalog.getString("Student"), value:'4'},
        {name:gettextCatalog.getString("Professional"), value:'5'},
        {name:gettextCatalog.getString("Volunteer"), value:'6'}
    ];
    
    
    $ocLazyLoad.load('js/services/UserService.js').then(function() {
        if($stateParams.username !== undefined){
            $scope.new = false;
            var User = $injector.get('User');
            User.readParticipant({
                username : $stateParams.username
            }).success(function(data){
                $scope.participant = data;
            }).error(function(status, data) {
                $rootScope.rootAlerts.push({
                    type:'danger',
                    msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                    priority: 2
                });
            });
            
            function deleteParticipant(username){
                User.delete({
                    username : username
                }).success(function(data){
                    getParticipants();
                }).error(function(status, data) {
                    console.log(data);
                    console.log(status);
                });
            }


            $scope.deleteParticipant = function(participant){
                ModalService.showModal({
                    templateUrl: "templates/modals/deleteParticipant.html",
                    controller: function($scope, close){
                        $scope.participant = participant;
                        $scope.close = function(result) {
                            close(result, 500); // close, but give 500ms for bootstrap to animate
                        };
                    }
                }).then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(result) {
                        if(result){
                            deleteParticipant(participant.username);
                        }
                    });
                });
            }


            $scope.send = function(){
                $scope.sending = true;
                User.sendPassword({
                    userID: $scope.participant._id
                }).success(function(data){
                    $scope.sending = false;
                    $rootScope.rootAlerts.push({
                         type:'success',
                         msg: gettextCatalog.getString('Email sent.'),
                         priority: 4
                    });
                }).error(function(status, data) {
                    $scope.sending = false;
                    $rootScope.rootAlerts.push({
                        type:'danger',
                        msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                        priority: 2
                    });
                });
            }

            $scope.submit = function(){
                var User = $injector.get('User');
                User.update(
                    $scope.participant
                ).success(function(data){
                    $state.transitionTo('home.participants.main');
                    $rootScope.rootAlerts.push({
                         type:'success',
                         msg: gettextCatalog.getString('The participant was updated.'),
                         priority: 4
                    });
                }).error(function(status, data) {
                    $rootScope.rootAlerts.push({
                        type:'danger',
                        msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                        priority: 2
                    });
                });
            }
        }
        else{
            $scope.new = true;
            $scope.submit = function(){
                if($scope.participant.who){
                    $scope.loading = true;
                    var User = $injector.get('User');
                    User.create(
                        $scope.participant
                    ).success(function(data){
                        $scope.loading = false;
                        $state.transitionTo('home.participants.main');
                        $rootScope.rootAlerts.push({
                             type:'success',
                             msg: gettextCatalog.getString('The participant was created.'),
                             priority: 4
                        });
                    }).error(function(err, status) {
                        $scope.loading = false;
                        if(err.errors.email){
                            $rootScope.rootAlerts.push({
                                type:'warning',
                                msg: gettextCatalog.getString("This email is already used."),
                                priority: 3
                            });
                        } else if(err.errors.username) {
                            $rootScope.rootAlerts.push({
                                type:'warning', 
                                msg: gettextCatalog.getString('This username is already used. Please pick another one.'),
                                priority: 3
                            });
                        } else {
                            $rootScope.rootAlerts.push({
                                type: 'danger',
                                msg: gettextCatalog.getString('An error occurred, please try again later'),
                                priority: 2
                            });
                        }
                    });
                } else {
                    $rootScope.rootAlerts.push({
                         type:'warning',
                         msg: gettextCatalog.getString("Please fill the 'Type' field"),
                         priority: 3
                    });
                }
            }
        }
    });
});
