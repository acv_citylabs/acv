angular.module('ParticipantsCtrl', []).controller('ParticipantsController', function($scope, $state, $window, gettextCatalog, $ocLazyLoad, $injector, ModalService, $rootScope) {
    $ocLazyLoad.load('js/services/UserService.js').then(function() {
        var User = $injector.get('User');
        var helperActivated = {
            title: gettextCatalog.getString("No activated participant"),
        };
        var helperUnactivated = {
            title: gettextCatalog.getString("No unactivated participant"),
        };
        var helperOthers = {
            title: gettextCatalog.getString("No other participant"),
        };
        $scope.participants = [], $scope.helperActivated = [], $scope.helperUnactivated = [], $scope.helperOthers = [];
        $scope.searchText = '';

        $scope.filterActivated = function (item) {
            return item.activationToken === 'activated';
        };
        
        $scope.filterUnactivated = function (item) {
            return item.activationToken === 'unactivated';
        };
        
        $scope.filterOthers = function (item) {
            return item.animatorID != $rootScope.user.firstname + ' ' + $rootScope.user.lastname;
        };
        
        function getParticipants(){
            $scope.helper = [];
            User.list().success(function(data){
                var tempHelperActivated = helperActivated;
                var tempHelperUnactivated = helperUnactivated;
                var tempHelperOthers = helperOthers;
                for(i=0;i<data.length;i++){
                    if(data[i].image){
                        data[i].image = {'background': 'url(../../files/sessions/'+data[i].image+') no-repeat center center #eee', 'background-size': '90px'};
                    } else {
                        data[i].image = {'background': 'url(../../img/logo-xs.png) no-repeat center center #eee', 'background-size': '90px'};
                    }
                    
                    if(data[i].activationToken === 'activated'){
                        tempHelperActivated = [];
                    }
                    if(data[i].activationToken === 'unactivated'){
                        tempHelperUnactivated = [];
                    }
                    if(data[i].animatorID != $rootScope.user.firstname + ' ' + $rootScope.user.lastname){
                        tempHelperOthers = [];
                    }
                }
                $scope.helperActivated = tempHelperActivated;
                $scope.helperUnactivated = tempHelperUnactivated;
                $scope.helperOthers = tempHelperOthers;
                
                $scope.participants = data;
            }).error(function(status, data) {
                console.log(data);
                console.log(status);
            });
        }
        getParticipants();

        function deleteParticipant(username){
            User.delete({
                username : username
            }).success(function(data){
                getParticipants();
            }).error(function(status, data) {
                console.log(data);
                console.log(status);
            });
        }
        
        
        $scope.deleteParticipant = function(participant){
            ModalService.showModal({
                templateUrl: "templates/modals/deleteParticipant.html",
                controller: function($scope, close){
                    $scope.participant = participant;
                    $scope.close = function(result) {
                        close(result, 500); // close, but give 500ms for bootstrap to animate
                    };
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(result) {
                    if(result){
                        deleteParticipant(participant.username);
                    }
                });
            });
        }
    });
});
