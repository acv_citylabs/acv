angular.module('AdminSessionsCtrl', []).controller('AdminSessionsController', function($scope, $state, $window, gettextCatalog, $ocLazyLoad, $injector, ModalService) {
    $ocLazyLoad.load('js/services/SessionService.js').then(function() {
        var Session = $injector.get('Session');
        var helperDisabled = {
            title: gettextCatalog.getString("No disabled session"),
        };
        var helperEnabled = {
            title: gettextCatalog.getString("No enabled session"),
        };
        $scope.sessions = [], $scope.helperDisabled = [], $scope.helperEnabled = [];

        $scope.filterDisabled = function (item) {
            return item.disabled;
        };
        
        $scope.filterEnabled = function (item) {
            return !item.disabled;
        };
        
        function getSessions(){
            $scope.helperDisabled = [];
            $scope.helperEnabled = [];
            Session.list({from:new Date(new Date().setHours(0,0,0,0))}).success(function(data){
                var tempHelperDisabled = helperDisabled;
                var tempHelperEnabled = helperEnabled;
                
                for(i=0;i<data.length;i++){
                    if(data[i].image){
                        data[i].image = {'background': 'url(../../files/sessions/'+data[i].image+') no-repeat center center #eee', 'background-size': '90px'};
                    } else {
                        data[i].image = {'background': 'url(../../img/logo-xs.png) no-repeat center center #eee', 'background-size': '90px'};
                    }
                    
                    if(data[i].disabled){
                        tempHelperDisabled = [];
                    }
                    if(!data[i].disabled){
                        tempHelperEnabled = [];
                    }
                }

                $scope.helperDisabled = tempHelperDisabled;
                $scope.helperEnabled = tempHelperEnabled;
                $scope.sessions = data;
            }).error(function(status, data) {
                console.log(data);
                console.log(status);
            });
        }
        getSessions();
        
        function deleteSession(id){
            Session.delete({
                id : id
            }).success(function(data){
                getSessions();
            }).error(function(status, data) {
                console.log(data);
                console.log(status);
            });
        }
        
        
        $scope.deleteSession = function(session){
            ModalService.showModal({
                templateUrl: "templates/modals/deleteSession.html",
                controller: function($scope, close){
                    $scope.session = session;
                    $scope.close = function(result) {
                        close(result, 500); // close, but give 500ms for bootstrap to animate
                    };
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(result) {
                    if(result){
                        deleteSession(session._id);
                    }
                });
            });
        }
        
        function toggleSession(id){
            Session.toggle({
                id : id
            }).success(function(data){
                getSessions();
            }).error(function(status, data) {
                console.log(data);
                console.log(status);
            });
        }
        
        
        $scope.reEnableSession = function(session){
            ModalService.showModal({
                templateUrl: "templates/modals/reEnableSession.html",
                controller: function($scope, close){
                    $scope.session = session;
                    $scope.close = function(result) {
                        close(result, 500); // close, but give 500ms for bootstrap to animate
                    };
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(result) {
                    if(result){
                        toggleSession(session._id);
                    }
                });
            });
        }
        
        $scope.disableSession = function(session){
            ModalService.showModal({
                templateUrl: "templates/modals/disableSession.html",
                controller: function($scope, close){
                    $scope.session = session;
                    $scope.close = function(result) {
                        close(result, 500); // close, but give 500ms for bootstrap to animate
                    };
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(result) {
                    if(result){
                        toggleSession(session._id);
                    }
                });
            });
        }
    });
});
