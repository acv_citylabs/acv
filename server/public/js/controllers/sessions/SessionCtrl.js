angular.module('SessionCtrl', [[
    'bower_components/pickadate/lib/themes/classic.css',
    'bower_components/pickadate/lib/themes/classic.date.css',
    'bower_components/pickadate/lib/themes/classic.time.css'
]]).controller('SessionController', function($scope, $window, gettextCatalog, $stateParams, $state, $ocLazyLoad, $injector, $rootScope, Upload, $timeout) {
    $scope.session = {
        startDate : new Date(new Date().setDate(new Date().getDate()+30)),
        startTime : new Date(new Date().setDate(new Date().getDate()+30))
    };

    $scope.checkDateInput = function () {
        var input = document.createElement('input');
        input.setAttribute('type','date');
        var notADateValue = 'not-a-date';
        input.setAttribute('value', notADateValue); 
        return !(input.value === notADateValue);
    }
    
    function mergeDatetime(aDate, aTime){
        return new Date(aDate.getFullYear(), aDate.getMonth(), aDate.getDate(), aTime.getHours(), aTime.getMinutes(), aTime.getSeconds());
    }
    
    $ocLazyLoad.load('js/services/SessionService.js').then(function() {
        var Session = $injector.get('Session');
        
        if($stateParams.id !== undefined){
            $scope.new = false;
            Session.read({
                id : $stateParams.id
            }).success(function(data){
               if(data.image){
                    data.image = {'background': 'url(../../files/sessions/'+data.image+') no-repeat center center #eee', 'background-size': '90px'};
                } else { 
                    data.image = {'background': 'url(../../img/logo-xs.png) no-repeat center center #eee', 'background-size': '90px'};
                }
                $scope.session = data;
                $scope.session.startDate = new Date(data.start);
                $scope.session.startTime = new Date(data.start);
            }).error(function(status, data) {
                $rootScope.rootAlerts.push({
                    type:'danger',
                    msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                    priority: 2
                });
            });

            $scope.submit = function(){
                $scope.session.start = mergeDatetime($scope.session.startDate, $scope.session.startTime);
                if($scope.session.file){
                    $scope.session.file.upload = Upload.upload({
                        url: '/api/sessions/'+$scope.session._id,
                        data: $scope.session,
                        method:'PUT'
                    });

                    $scope.session.file.upload.then(function (response) {
                        $timeout(function () {
                            $scope.session.file.result = response.data;
                            $state.transitionTo('home.sessions.main');
                            $rootScope.rootAlerts.push({
                                 type:'success',
                                 msg: gettextCatalog.getString('The session was created.'),
                                 priority: 4
                            });
                        });
                    }, function (response) {
                        console.log(response);
                        if (response.status > 0){
                            $scope.errorMsg = response.status + ': ' + response.data;
                        }
                    }, function (evt) {
                        // Math.min is to fix IE which reports 200% sometimes
                        $scope.session.file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                    });
                } else {
                    Session.update($scope.session).success(function(data){
                        $state.transitionTo('home.sessions.main');
                        $rootScope.rootAlerts.push({
                             type:'success',
                             msg: gettextCatalog.getString('The session was created.'),
                             priority: 4
                        });
                    }).error(function(status, data) {
                        $rootScope.rootAlerts.push({
                            type:'danger',
                            msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                            priority: 2
                        });
                    });
                }
            }
        } else {
            $scope.new = true;
            $scope.submit = function(){
                $scope.session.start = mergeDatetime($scope.session.startDate, $scope.session.startTime);
                if($scope.session.file){
                    $scope.session.file.upload = Upload.upload({
                        url: '/api/sessions',
                        data: $scope.session
                    });

                    $scope.session.file.upload.then(function (response) {
                        $timeout(function () {
                            $scope.session.file.result = response.data;
                            if(response.data.noCafe){
                                $rootScope.rootAlerts.push({
                                    type:'warning',
                                    msg: gettextCatalog.getString('Impossible to create a session. Cause: no Café assigned'),
                                    priority: 3
                                });
                            } else {
                                $state.transitionTo('home.sessions.main');
                                $rootScope.rootAlerts.push({
                                     type:'success',
                                     msg: gettextCatalog.getString('The session was created.'),
                                     priority: 4
                                });
                            };
                        });
                    }, function (response) {
                        console.log(response);
                        if (response.status > 0){
                            $scope.errorMsg = response.status + ': ' + response.data;
                        }
                    }, function (evt) {
                        // Math.min is to fix IE which reports 200% sometimes
                        $scope.session.file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                    });
                } else {
                    Session.create($scope.session).success(function(data){
                        if(data.noCafe){
                            $rootScope.rootAlerts.push({
                                type:'warning',
                                msg: gettextCatalog.getString('Impossible to create a session. Cause: no Café assigned'),
                                priority: 3
                            });
                        } else {
                            $state.transitionTo('home.sessions.main');
                            $rootScope.rootAlerts.push({
                                 type:'success',
                                 msg: gettextCatalog.getString('The session was created.'),
                                 priority: 4
                            });
                        };
                    }).error(function(status, data) {
                        $rootScope.rootAlerts.push({
                            type:'danger',
                            msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                            priority: 2
                        });
                    });
                }
            }
        }
    });
});
