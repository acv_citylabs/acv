angular.module('SessionsCtrl', []).controller('SessionsController', function($scope, $state, $window, gettextCatalog, $ocLazyLoad, $injector, ModalService, $rootScope) {
    $ocLazyLoad.load('js/services/SessionService.js').then(function() {
        var Session = $injector.get('Session');
        var helper = {
            title: gettextCatalog.getString("No coming session")
        };
        var helperArchived = {
            title: gettextCatalog.getString("No archived session")
        };
        $scope.sessions = [], $scope.helper = [];$scope.helperArchived = [];

        $scope.filterUpcoming = function (item) {
            return new Date(item.start) > new Date(new Date().setHours(23,59,59,999));
        };
        
        $scope.filterArchived = function (item) {
            return new Date(item.start) < new Date(new Date().setHours(0,0,0,0));
        };

        function getSessions(){
            $scope.helper = [];
            Session.list({}).success(function(data){
                var tempHelper = helper;
                var tempHelperArchived = helperArchived;
                for(i=0;i<data.length;i++){
                    if(data[i].image){
                        data[i].image = {'background': 'url(../../files/sessions/'+data[i].image+') no-repeat center center #eee', 'background-size': '90px'};
                    } else {
                        data[i].image = {'background': 'url(../../img/logo-xs.png) no-repeat center center #eee', 'background-size': '90px'};
                    }
                    
                    if(new Date(data[i].start) > new Date(new Date().setHours(23,59,59,999))){
                        tempHelper = [];
                    }
                    if(new Date(data[i].start) < new Date(new Date().setHours(0,0,0,0))){
                        tempHelperArchived = [];
                    }
                }
                $scope.helperArchived = tempHelperArchived;
                $scope.helper = tempHelper;
                $scope.sessions = data;
            }).error(function(status, data) {
                console.log(data);
                console.log(status);
            });
        }
        getSessions();
        
        function disableSession(id){
            Session.toggle({
                id : id
            }).success(function(data){
                getSessions();
            }).error(function(status, data) {
                console.log(data);
                console.log(status);
            });
        }
        
        
        $scope.disableSession = function(session){
            ModalService.showModal({
                templateUrl: "templates/modals/disableSession.html",
                controller: function($scope, close){
                    $scope.session = session;
                    $scope.close = function(result) {
                        close(result, 500); // close, but give 500ms for bootstrap to animate
                    };
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(result) {
                    if(result){
                        disableSession(session._id);
                    }
                });
            });
        }
    });
});
