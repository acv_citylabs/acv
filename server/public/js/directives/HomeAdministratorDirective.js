angular.module('HomeAdministratorDirective', []).directive('administrator', function(gettextCatalog, $ocLazyLoad, $injector, $rootScope, ModalService, $state, $window) {
    return {
        restrict: 'A',
        templateUrl: 'templates/home/homes/directives/administrator.html',
        link: function($scope, $element, $attrs) {
            $ocLazyLoad.load('js/services/SessionService.js').then(function() {
                var Session = $injector.get('Session');
                var helper = {
                    title: gettextCatalog.getString("No completed session")
                };
                $scope.sessions = [], $scope.helper = [];
                
                function getSessions(){
                    Session.list({to: new Date}).success(function(data){
                        for(i=0;i<data.length;i++){
                            if(data[i].image){
                                data[i].image = {'background': 'url(../../files/sessions/'+data[i].image+') no-repeat center center #eee', 'background-size': '90px'};
                            } else {
                                data[i].image = {'background': 'url(../../img/logo-xs.png) no-repeat center center #eee', 'background-size': '90px'};
                            }
                        }
                        if(data.length == 0){
                            $scope.helper = helper;
                        }
                        $scope.sessions = data;
                    }).error(function(status, data) {
                        console.log(data);
                        console.log(status);
                    });
                }
                getSessions();
                
                $scope.export = function(session){
                    Session.print({id: session._id}).success(function(data){
                        ModalService.showModal({
                            templateUrl: "templates/modals/exportSession.html",
                            controller: function($scope, close){
                                $scope.session = data;
                                $scope.print = function(){
                                    var printContents = document.getElementById('print').innerHTML;
                                    var popupWin = window.open('', '_blank');
                                    popupWin.document.open();
                                    popupWin.document.write('<html><head><link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"><link href="css/main.css" rel="stylesheet"></head><body onload="window.print()">' + printContents + '</body></html>');
                                    popupWin.document.close();
                                }
                                
                                $scope.close = function(result) {
                                    close(result, 500); // close, but give 500ms for bootstrap to animate
                                };
                            }
                        }).then(function(modal) {
                            modal.element.modal();
                            modal.close.then(function(result) {
                                if(result){
                                    
                                }
                            });
                        });
                    });
                }
            });
        }
    }
});