angular.module('HomeAnimatorDirective', []).directive('animator', function(gettextCatalog, $ocLazyLoad, $injector, $rootScope, ModalService, $state, $window) {
    return {
        restrict: 'A',
        templateUrl: 'templates/home/homes/directives/animator.html',
        link: function($scope, $element, $attrs) {
            $ocLazyLoad.load('js/services/SessionService.js').then(function() {
                var Session = $injector.get('Session');
                var helperToday = {
                    title: gettextCatalog.getString("No session planned today")
                };
                $scope.sessions = [], $scope.helperToday = [];
                
                $scope.filterToday = function (item) {
                    return new Date(item.start) >= new Date(new Date().setHours(0,0,0,0)) && new Date(item.start) <= new Date(new Date().setHours(23,59,59,999));
                };
                
                function getSessions(){
                    Session.list({from: new Date(new Date().setHours(0,0,0,0)), to: new Date(new Date().setHours(23,59,59,999))}).success(function(data){
                        var tempHelperToday = helperToday;
                        for(i=0;i<data.length;i++){
                            if(data[i].image){
                                data[i].image = {'background': 'url(../../files/sessions/'+data[i].image+') no-repeat center center #eee', 'background-size': '90px'};
                            } else {
                                data[i].image = {'background': 'url(../../img/logo-xs.png) no-repeat center center #eee', 'background-size': '90px'};
                            }
                            
                            if(new Date(data[i].start) >= new Date(new Date().setHours(0,0,0,0)) && new Date(data[i].start) <= new Date(new Date().setHours(23,59,59,999))){
                                tempHelperToday = [];
                            }
                        }
                        $scope.helperToday = tempHelperToday;
                        $scope.sessions = data;
                    }).error(function(status, data) {
                        console.log(data);
                        console.log(status);
                    });
                }
                getSessions();
                
                $scope.join = function(session){
                    $ocLazyLoad.load('js/services/RoomService.js').then(function () {
                        var Room = $injector.get('Room');
                        Room.join({
                            sessionID: session._id,
                            token: $window.sessionStorage.roomToken
                        }).success(function(data){
                            if(data.ok){
                                $window.sessionStorage.roomToken = data.token;
                                $state.go("home.room", {id:session._id});
                            } else {
                                if(data.tooSoon){
                                    $rootScope.rootAlerts.push({
                                         type:'info',
                                         msg: gettextCatalog.getString("You can join the session 15 minutes before it starts."),
                                         priority: 4
                                    });
                                } else if(data.duplicate){
                                    $rootScope.rootAlerts.push({
                                         type:'warning',
                                         msg: gettextCatalog.getString("You already have joined this session."),
                                         priority: 3
                                    });
                                } else if(data.tooLate){
                                    $rootScope.rootAlerts.push({
                                         type:'info',
                                         msg: gettextCatalog.getString("The session has already started."),
                                         priority: 3
                                    });
                                } else if(data.animatorFirst){
                                    $rootScope.rootAlerts.push({
                                         type:'info',
                                         msg: gettextCatalog.getString("Sorry, but you have to wait for the animator."),
                                         priority: 4
                                    });
                                } else if(data.full){
                                    $rootScope.rootAlerts.push({
                                         type:'info',
                                         msg: gettextCatalog.getString("Sorry, but this session is full."),
                                         priority: 4
                                    });
                                }
                            }        
                        }).error(function(status, data) {
                            $rootScope.rootAlerts.push({
                                type:'danger',
                                msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                                priority: 2
                            });
                        });
                    });
                }
                
                function disableSession(id){
                    Session.toggle({
                        id : id
                    }).success(function(data){
                        getSessions();
                    }).error(function(status, data) {
                        console.log(data);
                        console.log(status);
                    });
                }


                $scope.disableSession = function(session){
                    ModalService.showModal({
                        templateUrl: "templates/modals/disableSession.html",
                        controller: function($scope, close){
                            $scope.session = session;
                            $scope.close = function(result) {
                                close(result, 500); // close, but give 500ms for bootstrap to animate
                            };
                        }
                    }).then(function(modal) {
                        modal.element.modal();
                        modal.close.then(function(result) {
                            if(result){
                                disableSession(session._id);
                            }
                        });
                    });
                }
            });
        }
    }
});