angular.module('HomeParticipantDirective', []).directive('participant', function(gettextCatalog, $ocLazyLoad, $injector, $rootScope, ModalService, $state, $window) {
    return {
        restrict: 'A',
        templateUrl: 'templates/home/homes/directives/participant.html',
        link: function($scope, $element, $attrs) {
            $ocLazyLoad.load('js/services/SessionService.js').then(function() {
                var Session = $injector.get('Session');
                $scope.more = false;
                var helperToday = {
                    title: gettextCatalog.getString("No session planned today"),
                    text: gettextCatalog.getString("Please check below the coming sessions.")
                };
                var helper = {
                    title: gettextCatalog.getString("No upcoming session for now"),
                    text: gettextCatalog.getString("Thank you to come back later.")
                };
                $scope.remindersList = [
                    {name:gettextCatalog.getString('1 hour before'), value:'0'},
                    {name:gettextCatalog.getString('1 day before'), value:'1'}
                ];
                $scope.sessions = [], $scope.helper = [], $scope.helperToday = [];
                
                $scope.filterToday = function (item) {
                    return new Date(item.start) >= new Date(new Date().setHours(0,0,0,0)) && new Date(item.start) <= new Date(new Date().setHours(23,59,59,999));
                };
                
                $scope.filterUpcoming = function (item) {
                    return new Date(item.start) > new Date(new Date().setHours(23,59,59,999));
                };
                
                
                //Verify if we have to display about
                function how(){
                    $ocLazyLoad.load('js/services/UIService.js').then(function() {
                        var UI = $injector.get('UI');
                        UI.app({name: 'how'}).success(function(data) {
                            if(data.display){
                                $scope.more = true
                            }
                        });
                    });
                }

                //Toggle
                $scope.toggle = function(){
                    $ocLazyLoad.load('js/services/UIService.js').then(function() {
                        var UI = $injector.get('UI');
                        UI.toggle({name:'how'}).success(function(data) {
                        });
                    });
                }
                
                function getSessions(){
                    $scope.helper = [];
                    Session.list({from:new Date(new Date().setHours(0,0,0,0))}).success(function(data){
                        var tempHelper = helper;
                        var tempHelperToday = helperToday;
                        for(i=0;i<data.length;i++){
                            if(data[i].image){
                                data[i].image = {'background': 'url(../../files/sessions/'+data[i].image+') no-repeat center center #eee', 'background-size': '90px'};
                            } else {
                                data[i].image = {'background': 'url(../../img/logo-xs.png) no-repeat center center #eee', 'background-size': '90px'};
                            }
                            
                            if(new Date(data[i].start) >= new Date(new Date().setHours(0,0,0,0)) && new Date(data[i].start) <= new Date(new Date().setHours(23,59,59,999))){
                                tempHelperToday = [];
                            }
                            if(new Date(data[i].start) > new Date(new Date().setHours(23,59,59,999))){
                                tempHelper = [];
                            }
                        }
                        $scope.helperToday = tempHelperToday;
                        $scope.helper = tempHelper;
                        $scope.sessions = data;
                    }).error(function(status, data) {
                        console.log(data);
                        console.log(status);
                    });
                }
                how();
                getSessions();
                
                $scope.join = function(session){
                    $ocLazyLoad.load('js/services/RoomService.js').then(function () {
                        var Room = $injector.get('Room');
                        Room.join({
                            sessionID: session._id,
                            token: $window.sessionStorage.roomToken
                        }).success(function(data){
                            if(data.ok){
                                $window.sessionStorage.roomToken = data.token;
                                $state.go("home.room", {id:session._id});
                            } else {
                                if(data.tooSoon){
                                    $rootScope.rootAlerts.push({
                                         type:'info',
                                         msg: gettextCatalog.getString("You can join the session 15 minutes before it starts."),
                                         priority: 4
                                    });
                                } else if(data.duplicate){
                                    $rootScope.rootAlerts.push({
                                         type:'warning',
                                         msg: gettextCatalog.getString("You already have joined this session."),
                                         priority: 3
                                    });
                                } else if(data.tooLate){
                                    $rootScope.rootAlerts.push({
                                         type:'info',
                                         msg: gettextCatalog.getString("The session has already started."),
                                         priority: 3
                                    });
                                } else if(data.animatorFirst){
                                    $rootScope.rootAlerts.push({
                                         type:'info',
                                         msg: gettextCatalog.getString("Sorry, but you have to wait for the animator."),
                                         priority: 4
                                    });
                                } else if(data.full){
                                    $rootScope.rootAlerts.push({
                                         type:'info',
                                         msg: gettextCatalog.getString("Sorry, but this session is full."),
                                         priority: 4
                                    });
                                }
                            }        
                        }).error(function(status, data) {
                            $rootScope.rootAlerts.push({
                                type:'danger',
                                msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                                priority: 2
                            });
                        });
                    });
                }
                
                $scope.details = function(session){
                    ModalService.showModal({
                        templateUrl: "templates/modals/sessionDetails.html",
                        controller: function($scope, close){
                            $scope.session = session;
                            $scope.close = function(result) {
                                close(result, 500); // close, but give 500ms for bootstrap to animate
                            };
                        }
                    }).then(function(modal) {
                        modal.element.modal();
                        modal.close.then(function(result) {

                        });
                    });
                }
                
                $scope.follow = function(session){
                    if(!session.followed){
                        var reminders = $scope.remindersList;
                        ModalService.showModal({
                            templateUrl: "templates/modals/followSession.html",
                            controller: function($scope, $element, close){
                                $scope.session = session;
                                $scope.remindersList = reminders;
                                var boxes = [];
                                $scope.close = function() {
                                    for(i=0;i<$scope.remindersList.length;i++){
                                        if($scope.remindersList[i].state){
                                            boxes.push($scope.remindersList[i].value);
                                            delete $scope.remindersList[i].state;
                                        }
                                    }
                                    close({boxes: boxes}, 500); // close, but give 500ms for bootstrap to animate
                                };
                                $scope.cancel = function() {
                                    $element.modal('hide');
                                    close({cancel: true}, 500); // close, but give 500ms for bootstrap to animate
                                };
                            }
                        }).then(function(modal) {
                            modal.element.modal();
                            modal.close.then(function(result) {
                                if(!result.cancel){
                                    Session.follow({
                                        id: session._id,
                                        reminders: result.boxes
                                    }).success(function(data){
                                        getSessions();
                                        if(result.boxes.length != 0){
                                            $rootScope.rootAlerts.push({
                                                 type:'success',
                                                 msg: gettextCatalog.getString('A reminder was created.'),
                                                 priority: 4
                                            });
                                        }
                                    }).error(function(status, data) {
                                        $rootScope.rootAlerts.push({
                                            type:'danger',
                                            msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                                            priority: 2
                                        });
                                    });
                                }
                            });
                        });
                    } else {
                        Session.follow({
                            id: session._id,
                            unfollow : true
                        }).success(function(data){
                            getSessions();
                            $rootScope.rootAlerts.push({
                                 type:'success',
                                 msg: gettextCatalog.getString("I'm no longer interested in that session."),
                                 priority: 4
                            });
                        }).error(function(status, data) {
                            $rootScope.rootAlerts.push({
                                type:'danger',
                                msg: gettextCatalog.getString('An error occurred, please try again later. Sorry about that.'),
                                priority: 2
                            });
                        });
                    }
                }
            });
        }
    }
});