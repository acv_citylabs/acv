angular.module('routes', []).config(['$stateProvider', '$urlRouterProvider', '$httpProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider) {
    $.material.init();
    
    $urlRouterProvider.otherwise('/error/404');
	$stateProvider
    
        .state('signin', {
            url: '/signin',
            params: { isNew: null, isResetted: null, isLost: null, email: null},
            views: {
                'content': {
                    templateUrl: 'templates/signin.html',
                    controller: 'SigninController'
                }
            },
            access: { requiredAuthentication: false },
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/SigninCtrl.js');
                }]
            }
		})
    
        .state('signup', {
            url: '/signup/:token',
            views: {
                'content': {
                    templateUrl: 'templates/signup.html',
                    controller: 'SignupController'
                }
            },
            access: { requiredAuthentication: false},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/SignupCtrl.js');
                }]
            }
		})
    
        .state('activation', {
            url: '/signin/activation/:token',
            views: {
                'content': {
                    templateUrl: 'templates/signin.html',
                    controller: 'SigninController'
                }
            },
            access: { requiredAuthentication: false},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/SigninCtrl.js');
                }]
            }
		})
    
        .state('recovery', {
            abstract: true,
            url: '/recovery',
            views: {
                'content': {
                    templateUrl: 'templates/recovery/recovery.html'
                }
            },
            access: { requiredAuthentication: false}
		})
    
        .state('recovery.lost', {
            url: '/lost',
            templateUrl: 'templates/recovery/lost.html',
            controller: 'LostController',
            access: { requiredAuthentication: false},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/recovery/LostCtrl.js');
                }]
            }
		})
    
        .state('recovery.reset', {
            url: '/reset/:token',
            templateUrl: 'templates/recovery/reset.html',
            controller: 'ResetController',
            access: { requiredAuthentication: false},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/recovery/ResetCtrl.js');
                }]
            }
		})
        
        .state('conditions', {
            url: '/conditions/public',
            views: {
                'content': {
                    templateUrl: 'templates/conditions/public.html',
                    controller: 'ConditionsController'
                }
            },
            access: { requiredAuthentication: false},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/ConditionsCtrl.js');
                }]
            }
		})
    
        .state('home', {
            abstract: true,
            url: '/',
            views: {
                'header': {
                    templateUrl: 'templates/header.html',
                    controller: 'HeaderController'
                },
                'content': {
                    templateUrl: 'templates/home.html'
                }
            },
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/HeaderCtrl.js');
                }]
            }
		})
    
    
        .state('home.main', {
            url: '',
            templateUrl: 'templates/home/main.html',
            controller: 'HomeController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/HomeCtrl.js');
                }]
            }
		})
    
        .state('home.animators', {
            abstract: true,
            url: 'animators',
            templateUrl: 'templates/animators/animators.html',
            access: { requiredAuthentication: true},
		})
    
        .state('home.animators.main', {
            url: '',
            params: { goto: null},
            templateUrl: 'templates/animators/main.html',
            controller: 'AnimatorsController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/animators/AnimatorsCtrl.js');
                }]
            }
		})
    
        .state('home.animators.edit', {
            url: '/edit/:username',
            templateUrl: 'templates/animators/edit.html',
            controller: 'AnimatorController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/animators/AnimatorCtrl.js');
                }]
            }
		})
    
        .state('home.animators.profile', {
            url: '/profile/:username',
            templateUrl: 'templates/animators/profile.html',
            controller: 'AnimatorController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/animators/AnimatorCtrl.js');
                }]
            }
		})
    
        .state('home.animators.new', {
            url: '/new',
            templateUrl: 'templates/animators/edit.html',
            controller: 'AnimatorController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/animators/AnimatorCtrl.js');
                }]
            }
		})
    
        .state('home.participants', {
            abstract: true,
            url: 'participants',
            templateUrl: 'templates/participants/participants.html',
            access: { requiredAuthentication: true},
		})
    
        .state('home.participants.main', {
            url: '',
            params: { goto: null},
            templateUrl: 'templates/participants/main.html',
            controller: 'ParticipantsController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/participants/ParticipantsCtrl.js');
                }]
            }
		})
    
        .state('home.participants.edit', {
            url: '/edit/:username',
            templateUrl: 'templates/participants/edit.html',
            controller: 'ParticipantController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/participants/ParticipantCtrl.js');
                }]
            }
		})
    
        .state('home.participants.profile', {
            url: '/profile/:username',
            templateUrl: 'templates/participants/profile.html',
            controller: 'ParticipantController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/participants/ParticipantCtrl.js');
                }]
            }
		})
    
        .state('home.participants.new', {
            url: '/new',
            templateUrl: 'templates/participants/edit.html',
            controller: 'ParticipantController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/participants/ParticipantCtrl.js');
                }]
            }
		})
    
        .state('home.cafes', {
            abstract: true,
            url: 'cafes',
            templateUrl: 'templates/cafes/cafes.html',
            access: { requiredAuthentication: true},
		})
    
        .state('home.cafes.main', {
            url: '',
            templateUrl: 'templates/cafes/main.html',
            controller: 'CafesController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/cafes/CafesCtrl.js');
                }]
            }
		})
    
        .state('home.cafes.edit', {
            url: '/cafes/:id',
            templateUrl: 'templates/cafes/one.html',
            controller: 'CafeController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/cafes/CafeCtrl.js');
                }]
            }
		})
    
        .state('home.cafes.new', {
            url: '/cafe',
            templateUrl: 'templates/cafes/one.html',
            controller: 'CafeController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/cafes/CafeCtrl.js');
                }]
            }
		})
    
        .state('home.adminsessions', {
            url: 'adminsessions',
            templateUrl: 'templates/sessions/admin.html',
            controller: 'AdminSessionsController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/sessions/AdminSessionsCtrl.js');
                }]
            }
		})
    
        .state('home.adminconditions', {
            url: 'conditions/admin',
            templateUrl: 'templates/conditions/admin.html',
            controller: 'ConditionsController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/ConditionsCtrl.js');
                }]
            }
		})
    
        .state('home.conditions', {
            url: 'conditions/user',
            templateUrl: 'templates/conditions/user.html',
            controller: 'ConditionsController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/ConditionsCtrl.js');
                }]
            }
		})
    
        .state('home.sessions', {
            abstract: true,
            url: 'sessions',
            templateUrl: 'templates/sessions/sessions.html',
            access: { requiredAuthentication: true},
		})
    
        .state('home.sessions.main', {
            url: '',
            templateUrl: 'templates/sessions/main.html',
            controller: 'SessionsController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/sessions/SessionsCtrl.js');
                }]
            }
		})
    
        .state('home.sessions.edit', {
            url: '/sessions/:id',
            templateUrl: 'templates/sessions/one.html',
            controller: 'SessionController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/sessions/SessionCtrl.js');
                }]
            }
		})
    
        .state('home.sessions.new', {
            url: '/session',
            templateUrl: 'templates/sessions/one.html',
            controller: 'SessionController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/sessions/SessionCtrl.js');
                }]
            }
		})
    
        .state('home.chats', {
            abstract: true,
            url: 'chats',
            templateUrl: 'templates/chats/chats.html',
            access: { requiredAuthentication: true}
		})
    
        .state('home.chats.main', {
            url: '',
            templateUrl: 'templates/chats/main.html',
            controller: 'ChatsController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/chats/ChatsCtrl.js');
                }]
            }
		})
    
        .state('home.chats.new', {
            url: '/new',
            templateUrl: 'templates/chats/new.html',
            controller: 'ChatsController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/chats/ChatsCtrl.js');
                }]
            }
		})
    
        .state('home.chats.one', {
            url: '/:username',
            templateUrl: 'templates/chats/one.html',
            controller: 'ChatController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/chats/ChatCtrl.js');
                }]
            }
		})
    
        .state('home.room', {
            url: 'room/:id',
            templateUrl: 'templates/room.html',
            controller: 'RoomController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/RoomCtrl.js');
                }]
            }
		})
    
        .state('home.mycafe', {
            url: 'mycafe',
            templateUrl: 'templates/cafes/mycafe.html',
            controller: 'MycafeController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/cafes/MycafeCtrl.js');
                }]
            }
		})
    
        .state('home.reminders', {
            url: 'reminders',
            templateUrl: 'templates/reminders.html',
            controller: 'RemindersController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/RemindersCtrl.js');
                }]
            }
		})
		.state('home.video', {
            url: 'video',
            templateUrl: 'templates/video.html',
            controller: 'VideoController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/VideoCtrl.js');
                }]
            }
		})
        .state('home.audit', {
            url: 'audit',
            templateUrl: 'templates/audit.html',
            controller: 'AuditController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/AuditCtrl.js');
                }]
            }
		})
    
        .state('home.notifications', {
            url: 'notifications',
            templateUrl: 'templates/notifications.html',
            controller: 'NotificationsController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/NotificationsCtrl.js');
                }]
            }
		})
    
        .state('home.profile', {
            abstract: true,
            url: 'profile',
            templateUrl: 'templates/profile/profile.html',
            controller: 'ProfileController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/ProfileCtrl.js');
                }]
            }
		})
    
        .state('home.profile.main', {
            url: '',
            templateUrl: 'templates/profile/main.html',
            controller: 'ProfileController',
            access: { requiredAuthentication: true}
		})
    
        .state('home.profile.avatar', {
            url: '/avatar',
            templateUrl: 'templates/profile/avatar.html',
            controller: 'ProfileController',
            access: { requiredAuthentication: true}
		})
    
        .state('home.profile.basic', {
            url: '/basic',
            templateUrl: 'templates/profile/basic.html',
            controller: 'ProfileController',
            access: { requiredAuthentication: true}
		})
    
        .state('home.settings', {
            abstract: true,
            url: 'settings',
            templateUrl: 'templates/settings/settings.html',
            controller: 'SettingsController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/SettingsCtrl.js');
                }]
            }
		})
    
        .state('home.settings.main', {
            url: '',
            templateUrl: 'templates/settings/main.html',
            access: { requiredAuthentication: true}
		})
    
        .state('home.settings.password', {
            url: '/password',
            templateUrl: 'templates/settings/password.html',
            access: { requiredAuthentication: true}
		})
    
        .state('home.error', {
            url: 'error/:status',
            templateUrl: 'templates/error.html',
            controller: 'ErrorController',
            access: { requiredAuthentication: true},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('js/controllers/ErrorCtrl.js');
                }]
            }
		});
    
    $locationProvider.html5Mode(true);
    $httpProvider.interceptors.push('TokenInterceptor');
}]).run(function ($rootScope, $location, $state, $window, gettextCatalog, $ocLazyLoad, $injector) {
    $rootScope.$on("$stateChangeStart", function (event, nextState, currentRoute) {
        $rootScope.rootAlerts = [];
        if($window.localStorage.token){
            $ocLazyLoad.load('js/services/AccountService.js').then(function() {
                var Account = $injector.get('Account');
                Account.read().success(function(profile) {
                    $window.localStorage.language = profile.language;
                    $rootScope.user = {
                        username: profile.username,
                        firstname: profile.firstname,
                        lastname: profile.lastname,
                        email: profile.email
                    };
                    if (profile.preferences !== undefined && profile.preferences.avatar !== undefined) {
                        $rootScope.user.avatar = profile.preferences.avatar;
                    }
                });
            });
        }
        
        gettextCatalog.currentLanguage = ($window.localStorage.language !== undefined) ? $window.localStorage.language.toLowerCase() + '_BE' : (navigator.language.substr(0, 2) || navigator.userLanguage.substr(0, 2)) + '_BE';

        if (nextState !== null && nextState.access !== null && nextState.access.requiredAuthentication && !$window.localStorage.token) {
            $location.path("/signin");
        }
    });
});