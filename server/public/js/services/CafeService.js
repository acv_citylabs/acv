angular.module('CafeService', []).factory('Cafe', function($http) {
    return {
        list: function() {
            return $http.get('/api/cafes');
        },
        read: function(info) {
            return $http.get('/api/cafes/' + info.id);
        },
        readOwn: function() {
            return $http.get('/api/cafes/animator/own');
        },
        create: function(info) {
            return $http.post('/api/cafes', info);
        },
        update: function(info) {
            return $http.put('/api/cafes/' + info.id, info);
        },
        delete: function(info) {
            return $http.delete('/api/cafes/' + info.id);
        }
    }
});