angular.module('ConditionsService', []).factory('Conditions', function($http) {
    return {
        read: function() {
            return $http.get('/api/conditions/read');
        },
        update: function(info) {
            return $http.put('/api/conditions', info);
        }
    }
});