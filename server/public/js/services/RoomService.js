angular.module('RoomService', []).factory('Room', function($http, $q) {
    return {
        read: function(info) {
            return $http.put('/api/rooms/' + info.sessionID, info);
        },
        join: function(info) {
            return $http.put('/api/rooms/join/' + info.sessionID, info);        
        },
        getToken: function(info) {
            return $http.put('/api/rooms/getToken/' + info.sessionID, info); // get an authorization token for this socket ID
        }
    }
});