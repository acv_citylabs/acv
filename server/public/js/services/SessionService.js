angular.module('SessionService', []).factory('Session', function($http) {
    return {
        list: function(info) {
            return $http.get('/api/sessions/from/' + info.from + '/to/' + info.to);
        },
        read: function(info) {
            return $http.get('/api/sessions/' + info.id);
        },
        print: function(info) {
            return $http.get('/api/sessions/' + info.id + '/print');
        },
        create: function(info) {
            return $http.post('/api/sessions', info);
        },
        update: function(info) {
            return $http.put('/api/sessions/' + info._id, info);
        },
        interested: function(info) {
            return $http.put('/api/sessions/interested/' + info.id, info);
        },
        toggle: function(info) {
            return $http.put('/api/sessions/toggle/' + info.id, info);
        },
        delete: function(info) {
            return $http.delete('/api/sessions/' + info.id);
        },
        reminders: function() {
            return $http.get('/api/sessions/reminders/all');
        },
        follow: function(info) {
            return $http.put('/api/sessions/'+info.id+'/myreminder', info);
        }
    }
});