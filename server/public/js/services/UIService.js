angular.module('UIService', []).factory('UI', function($http) {
    return {
        nav: function(){
            return $http.get("/api/ui/nav");
        },
        
        home: function(){
            return $http.get("/api/ui/home");
        },
        
        toggle: function(info){
            return $http.put("/api/ui/app", info);
        },
        
        app: function(info){
            return $http.get("/api/ui/app/"+info.name);
        }
    }
});