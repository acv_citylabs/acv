angular.module('UserService', []).factory('User', function($http) {
    return {
        list: function() {
            return $http.get('/api/users');
        },
        readParticipant: function(info) {
            return $http.get('/api/users/read/' + info.username + '/participant');
        },
        readAnimator: function(info) {
            return $http.get('/api/users/read/' + info.username + '/animator');
        },
        create: function(info) {
            return $http.post('/api/users', info);
        },
        sendPassword: function(info) {
            return $http.post('/api/users/sendPassword', info);
        },
        update: function(info) {
            return $http.put('/api/users/' + info.username, info);
        },
        delete: function(info) {
            return $http.delete('/api/users/' + info.username);
        }
    }
});