angular.module('VideoService', []).factory('Video', function($http) {
    return {
        read: function() {
            return $http.get('/api/video/read');
        },
        update: function(video) {
            return $http.put('/api/video', video);
        }
    }
});