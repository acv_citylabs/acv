function now(){
    var date = new Date(Date.now());
    console.log(date);
    var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);
    var offset = date.getTimezoneOffset() / 60;
    var hours = date.getHours();
    newDate.setHours(hours - offset);
    console.log(newDate);
    return newDate;
};

function dateFormatFromISO(str){
    date = new Date(str);
    var d = addZero(date.getDate());
    var m = addZero(date.getMonth()+1);
    var y = date.getFullYear();
    return d + '/' + m + '/' + y;
};

function addZero(str){
    str = str.toString();
    if(str.length == 1){
        str = "0" + str;
    }
    return str;
};